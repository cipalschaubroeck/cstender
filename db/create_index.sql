# noinspection SqlNoDataSourceInspectionForFile

DROP INDEX query_fti1 ON Tender;
CREATE FULLTEXT INDEX query_fti1 ON Tender(title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpv,categoryAI,governmentType);

DROP INDEX queryAI_fti1 ON Tender;
CREATE FULLTEXT INDEX queryAI_fti1 ON Tender(title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType);

DROP INDEX queryAIExact_fti1 ON Tender;
CREATE FULLTEXT INDEX queryAIExact_fti1 ON Tender(title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvNoParent,categoryAI,governmentType);

CREATE INDEX noticeId on Tender(noticeId);

DROP INDEX attachment_fti1 ON Attachment;
CREATE FULLTEXT INDEX attachment_fti1 ON Attachment(reverseIndex,features,disciplinesAI,location,cpv,categoryAI,governmentType);

CREATE INDEX contractor_name_location ON Contractor(name,location);

DROP INDEX contractor_fti1 ON Contractor;
CREATE FULLTEXT INDEX contractor_fti1 ON Contractor(reverseIndex);

DROP INDEX government_fti1 ON Government;
CREATE FULLTEXT INDEX government_fti1 ON Government(reverseIndex);

CREATE INDEX government_name_location ON Government(name,location);

DROP INDEX user_fti1 ON User;
CREATE FULLTEXT INDEX user_fti1 ON User(searchIndex);

ALTER TABLE Tender MODIFY nuts text;
ALTER TABLE Tender MODIFY nutsNoParent text;
ALTER TABLE Tender MODIFY location text;
ALTER TABLE Tender MODIFY noticeIds text;
ALTER TABLE Tender MODIFY category text;
ALTER TABLE Tender MODIFY categoryAI text;
ALTER TABLE Tender MODIFY disciplines text;
ALTER TABLE Tender MODIFY disciplinesAI text;
ALTER TABLE Tender MODIFY disciplinesCpvAI text;

ALTER TABLE Tender MODIFY title1 text;
ALTER TABLE Tender MODIFY title2 text;
ALTER TABLE Tender MODIFY title3 text;
ALTER TABLE Tender MODIFY title4 text;
ALTER TABLE Tender MODIFY bda text;
ALTER TABLE Tender MODIFY ereference text;