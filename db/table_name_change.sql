ALTER TABLE FilterTender DROP FOREIGN KEY `FK_319v4i8oki33xk2l17rl6moha`;
ALTER TABLE FilterTender ADD FOREIGN KEY (`tender`) REFERENCES `Tender` (`id`);

ALTER TABLE Tender DROP FOREIGN KEY `FK_2q58oqyxnmifjge9eq5rhjnbd`;
ALTER TABLE Tender ADD FOREIGN KEY (`government`) REFERENCES `Government` (`id`);

ALTER TABLE Filter DROP FOREIGN KEY `FK_63gmcra58ds5a508fiyrminv9`;
ALTER TABLE Filter ADD  FOREIGN KEY (`user`) REFERENCES `User` (`id`);

ALTER TABLE Contact DROP FOREIGN KEY `FK_dexjrnk7xqlh353w7b3aq23ya`;
ALTER TABLE Contact ADD FOREIGN KEY (`government`) REFERENCES `Government` (`id`);

ALTER TABLE Attachment DROP FOREIGN KEY `FK_286rpuacsgo0p2jv00lwbrtrp`;
ALTER TABLE Attachment ADD FOREIGN KEY (`tender`) REFERENCES `Tender` (`id`);

ALTER TABLE AwardItem DROP FOREIGN KEY `FK_ro8l8llkj9dan635i666j8e3d`;
ALTER TABLE AwardItem ADD FOREIGN KEY (`contractor`) REFERENCES `Contractor` (`id`);

ALTER TABLE Rectification DROP FOREIGN KEY `FK_rfa2t7kbxxht677u35qpr2ci`;
ALTER TABLE Rectification ADD FOREIGN KEY (`tender`) REFERENCES `Tender` (`id`);
