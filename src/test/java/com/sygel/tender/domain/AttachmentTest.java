/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import org.junit.Test;

import com.sygel.tender.domain.util.Util;

import junit.framework.TestCase;

public class AttachmentTest extends TestCase {
	@Test
	public void testDBIndex() {
    Attachment attachment = new Attachment();
    attachment.setName("535545.doc");
    assertEquals(true, attachment.isExtraAttachment());
    attachment.setName("535545_Bestek.pdf");
    assertEquals(true, attachment.isExtraAttachment());
    attachment.setName("254278_0_0_fr-FR.pdf");
    assertEquals(false, attachment.isExtraAttachment());
	}
}
