package com.sygel.tender.domain.util;

import java.util.List;

import org.junit.Test;

import junit.framework.TestCase;

public class UtilTest extends TestCase {
	@Test
	public void testDBIndex() {
		String test = "het is;vier,uur.Jaja p_plop@pipo.com +32 14700350";
		System.out.println(Util.constructDBIndex(test));
		//assertEquals("zzteh zzsi zzreiv zzruu zzajaJ", Util.constructReverseWords(test));
	}
	
	@Test
	public void testEmailExtractor() {
		String test = "dit is mijn email p_plop@pipo.com, wat is uw nummer? mijn is y.p@test-hier.be";
		List<String> emails = Util.getEmails(test);
		String[] expected1 = {
				"p_plop@pipo.com",
				"y.p@test-hier.be"
		};
		int i = 0;
		for (String email : emails) {
		  assertEquals(expected1[i], email);
		  System.out.println(email);
		  i++;
		}
	}
	
	@Test
	public void testPhoneExtractor() {
		String test = "dit is mijn nummer +32 14700350, wat is uw nummer? mijn is +1 12312312";
		List<String> numbers = Util.getPhoneNumbers(test);
		String[] expected1 = {
				"+32 14700350",
				"+1 12312312"
		};
		int i = 0;
		for (String number : numbers) {
		  assertEquals(expected1[i], number);
		  System.out.println(number);
		  i++;
		}
	}

}
