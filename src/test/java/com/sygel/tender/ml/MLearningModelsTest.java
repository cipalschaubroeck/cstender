package com.sygel.tender.ml;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Util;

import junit.framework.TestCase;

public class MLearningModelsTest extends TestCase {
	@Test
	public void testUpdateDiscipline() throws IOException, InterruptedException, ClassNotFoundException {
		Tender tender = new Tender();
		tender.setCategoryAI(":"+Util.CATEGORY_PREFIX+"C1"+":");
		tender.setCpvNoParentAI("");
		KnowledgeBase knowledgeBase = new KnowledgeBase();
		HashMap<String, List<String>> cpvDisciplineMap = knowledgeBase.getCpvDisciplineMap();
		MLearningModels.updateDiscipline(tender, cpvDisciplineMap, knowledgeBase.getCategoryCpvMap(), true);
		System.out.println(tender.getDisciplinesCpvAI());
		System.out.println(tender.getCpvNoParentAI());
		assertEquals(":xxWR:", tender.getDisciplinesCpvAI());
		assertEquals("45232400", tender.getCpvNoParentAI());
				
		tender.setCategoryAI(":"+Util.CATEGORY_PREFIX+"P1"+":"+Util.CATEGORY_PREFIX+"C1"+":");
		tender.setCpvNoParentAI("45232460");
		MLearningModels.updateDiscipline(tender, cpvDisciplineMap, knowledgeBase.getCategoryCpvMap(), true);
		System.out.println(tender.getDisciplinesCpvAI());
		System.out.println(tender.getCpvNoParentAI());
		//assertEquals(":xxSA:xxEL:xxWR:", tender.getDisciplinesCpvAI());
		assertEquals("45232460 45310000 45232400", tender.getCpvNoParentAI());		
		
		tender.setCategoryAI(":"+Util.CATEGORY_PREFIX+"D8"+":");
		tender.setCpvNoParentAI("45000000");
		MLearningModels.updateDiscipline(tender, cpvDisciplineMap, knowledgeBase.getCategoryCpvMap(), true);
		System.out.println(tender.getDisciplinesCpvAI());
		System.out.println(tender.getCpvNoParentAI());
		assertEquals("45000000 45261214", tender.getCpvNoParentAI());		
		
		// test a CPV that is not in the exception list
		tender.setCategoryAI("");
		tender.setCpvNoParentAI("45000000");
		tender.setDisciplinesCpvAI("");
		MLearningModels.updateDiscipline(tender, cpvDisciplineMap, knowledgeBase.getCategoryCpvMap(), true);
		System.out.println(tender.getDisciplinesCpvAI());
		System.out.println(tender.getCpvNoParentAI());
		//assertEquals("45000000 45261214", tender.getCpvNoParentAI());
  }
	
	@Test
	public void testConstructParentCpv() throws IOException, InterruptedException, ClassNotFoundException {
		assertEquals("45261210", MLearningModels.constructParentCpv("45261214"));
		assertEquals("45260000", MLearningModels.constructParentCpv("45261000"));
		assertEquals(null, MLearningModels.constructParentCpv("45000000"));
	}
}
