package com.sygel.tender.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Test;

public class KnowledgeVerbPhraseTest extends TestCase {
	@Test
	public void testRelationExtraction() throws IOException, InterruptedException
	{
		KnowledgeBase nb = new KnowledgeBase();
		StringBuilder sb = new StringBuilder();
		Map<String,Integer> features = new HashMap<String, Integer>();
		KnowledgeVerb vps = new KnowledgeVerb("renovatie",nb);
		List<KnowledgeRelation> relalationList = vps.hasRelation(" Renovatie en uitbreiding bestaand kantoorgebouw Renovatie en uitbreiding bestaand kantoorgebouw."+
				" g van materiaal voor herstellingen electrische installaties. "+
				"Nieuwbouw en verbouwingswerken van bestaande schoolgebouw 45313100.", nb.namedEntityList, nb.namedAdjectiveList, nb.adjectiveList, nb.verbPhraseList);
		if (relalationList != null) {
			for (KnowledgeRelation rel : relalationList) {		    
				sb.append(rel.constructVerbs() + ";" + rel.constructEntities());
				rel.addFeatures(features, nb.namedEntityMap);
			}
		} 


		String[] expecteds = {"REL_renovatie_CAT_gebouw","REL_renovatie_kantoorgebouw","REL_uitbreiden_CAT_gebouw","REL_uitbreiden_kantoorgebouw"};
		int i = 0;
		List<String> sortedFeatures = new ArrayList<String>(features.keySet());
		Collections.sort(sortedFeatures);
		for (String feature : sortedFeatures) {
			//System.out.println(feature);
			assertEquals(expecteds[i++], feature);
		}	     
		assertEquals("renovatie/uitbreiden;kantoorgebouw", sb.toString());	


	}
	@Test
	public void testRelationsExtraction() throws IOException, InterruptedException
	{
		KnowledgeBase nb = new KnowledgeBase();
		List<String> features = new ArrayList<String>(nb.extractFeatures(
				" Renovatie en uitbreiding bestaand kantoorgebouw."+
						" g van materiaal voor herstellingen electrische installaties. "+
				"Nieuwbouw en verbouwingswerken van bestaande schoolgebouw 45313100."));
		Collections.sort(features);
		String[] expected1 = {
				"NE_electrische_installatie",
				"NE_herstelling",
				"NE_kantoorgebouw",
				"NE_materiaal",
				"NE_nieuwbouw",
				"NE_school",
				"NE_verbouwingswerk",
				"REL_herstellen_electrische_installatie",
				"REL_herstellen_installatie",
				"REL_nieuwbouw_CAT_gebouw",
				"REL_nieuwbouw_school",
				"REL_renoveren_CAT_gebouw",
				"REL_renoveren_kantoorgebouw",
				"REL_uitbreiden_CAT_gebouw",
				"REL_uitbreiden_kantoorgebouw",
				"REL_verbouwen_CAT_gebouw",
		        "REL_verbouwen_school"};
		int i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected1[i++], feature);
		}		
		assertEquals(expected1.length, features.size());	

		// TODO
		// Aanleg van stoepen en aanpassingen aan de riolering,
		// Afwerking en speciale technieken -
		// 

		features = new ArrayList<String>(nb.extractFeatures(
				" Bouwen van een DWA pompput, stuwputten, schotten en keermuren."
				));
		Collections.sort(features);
		String[] expected2 = {
				"NE_droogweerafvoer",
				"NE_dwa_pompput",
				"NE_keermuur",
				"NE_schot",
				"NE_stuwputten",
				"REL_bouwen_dwa_pompput",
				"REL_bouwen_keermuur",
				"REL_bouwen_pompput",
				"REL_bouwen_schot",
		    "REL_bouwen_stuwputten"};
		i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected2[i++], feature);
		}		
		assertEquals(expected2.length, features.size());
		
		// plaatsen van een nieuwe keuken, badkamer en weg
		features = new ArrayList<String>(nb.extractFeatures(
				" plaatsen van een nieuwe keuken, badkamer en weg."+
		        " herstellen van dak-en torenwerken."
				));
		Collections.sort(features);
		String[] expected3 = {
				"NE_badkamer",
				"NE_dakwerk",
				"NE_keuken",
				"NE_torenwerk",
				"NE_weg",
				"REL_herstellen_dakwerk",
				"REL_herstellen_torenwerk",
				"REL_plaatsen_badkamer",
        "REL_plaatsen_keuken",
        "REL_plaatsen_weg"};
		i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected3[i++], feature);
		}		
		assertEquals(expected3.length, features.size());
		
		
		
		//beperkte aanpassingswerken elektriciteit en sanitair
		features = new ArrayList<String>(nb.extractFeatures(
				"beperkte aanpassingswerken elektriciteit,hvac en sanitair jaja het is nog langer"
		       
				));
		Collections.sort(features);
		String[] expected4 = {
				"NE_aanpassingswerk",
				"NE_elektriciteit",
			  "NE_hvac",
				"NE_sanitair",				
				"REL_aanpassen_elektriciteit",
				"REL_aanpassen_hvac",
				"REL_aanpassen_sanitair"
				};
		i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected4[i++], feature);
		}		
		assertEquals(expected4.length, features.size());
		
   // TODO herstellen van dak-, torenwerken		
		//aanbrengen_profileerlaag aanbrengen_een_profileerlaag
	}	
}
