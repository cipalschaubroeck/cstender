package com.sygel.tender.ml;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class KnowledgeNormalizeTest extends TestCase{
	@Test
	public void testNormalizeNamedEntity() throws IOException, InterruptedException {
	  KnowledgeBase knowledgeBase = new KnowledgeBase();
	  KnowledgeNormalize kn = new KnowledgeNormalize(knowledgeBase);
	  assertEquals("de street  en de street  in Gent", kn.normalizeNamedEntity("de Patrijzenweg 34 en de Berenstraat, in Gent"));
  }
	
	@Test
	public void testReplaceHyphen() {
		KnowledgeBase knowledgeBase = new KnowledgeBase();
		KnowledgeNormalize kn = new KnowledgeNormalize(knowledgeBase);
		assertEquals("sloopwerken en ondersteuningswerken",kn.normalizeHyphenConcat("Sloop - en ondersteuningswerken",null));				
		assertEquals("afbraakwerken en RUWBOUWWERKEN",kn.normalizeHyphenConcat(" AFBRAAK- EN RUWBOUWWERKEN",null));
		assertEquals("dakwerken of ruwbouwwerken jaja",kn.normalizeHyphenConcat(" dak- of ruwbouwwerken jaja",null));
		assertEquals("dakwerken en/of ruwbouwwerken jaja",kn.normalizeHyphenConcat(" dak- en/of ruwbouwwerken jaja",null));
		assertEquals("dakwerken en ruwbouwwerken jaja",kn.normalizeHyphenConcat(" dak- en ruwbouwwerken jaja",null));		
		assertEquals("rotsbrokken, ijsbrokken, rotsbrokken, metselwerkbrokken en betonbrokken jaja",kn.normalizeHyphenConcat(" rots-   ijs-, rots-, metselwerk- en betonbrokken jaja",null));
		assertEquals("wegenwerken, rioleringswerken en omgevingswerken jaja",kn.normalizeHyphenConcat(" wegen-, riolerings- en omgevingswerken jaja",null));				
		assertEquals("verwarmingsinstallaties, warmwaterbereidingsinstallaties en verluchtingsinstallaties",kn.normalizeHyphenConcat("verwarmings-, warmwaterbereidings-en verluchtingsinstallaties",null));		
	  // TODO
		// De sanitaire-/cv- en ventilatie-uitrustingen			
	  // dak- en aanverwante werken
		// archeologische en installatiewerken		
		// Bouwen van 10 huur- en 10 koopwoningen		
	}
	
	@Test
	public void testNormalizeAdjectiveAnd() {
		KnowledgeBase knowledgeBase = new KnowledgeBase();
		KnowledgeNormalize kn = new KnowledgeNormalize(knowledgeBase);
		assertEquals("sanitaire installaties en HVAC installaties",kn.normalizeAdjectiveAndConcat("sanitaire en HVAC installaties"));
		assertEquals("hellende daken en platte daken"             ,kn.normalizeAdjectiveAndConcat("hellende en platte daken"));
		assertEquals("SANITAIRE INSTALLATIE EN CV INSTALLATIE"    ,kn.normalizeAdjectiveAndConcat("SANITAIRE EN CV INSTALLATIE"));
		assertEquals("hellende daken / platte daken"              ,kn.normalizeAdjectiveAndConcat("hellende / platte daken"));
		assertEquals("hellende daken + platte daken"              ,kn.normalizeAdjectiveAndConcat("hellende + platte daken"));		
	}
	
	@Test
	public void testNormalizeLine() {
		KnowledgeBase knowledgeBase = new KnowledgeBase();
		assertEquals(" sanitaire installaties en HVAC installaties",knowledgeBase.normalizeLine("sanitaire en HVAC installaties"));
		assertEquals(" Onderhoud van de sanitaire installaties, sprinklerinstallaties, elektrische installaties en HVAC,installaties van het C.C.N.-gebouw",knowledgeBase.normalizeLine("Onderhoud van de sanitaire, sprinkler-, elektrische en HVAC-installaties van het C.C.N.-gebouw"));
		assertEquals(" Kandidatuurstelling voor opname in de lijst van geselecteerden voor elektromechanische uitrustingen en waterbehandelingsuitrustingen ( kandidatuurstelling EMU )",knowledgeBase.normalizeLine("Kandidatuurstelling voor opname in de lijst van geselecteerden voor elektromechanische en waterbehandelingsuitrustingen (kandidatuurstelling EMU)"));
		
	}
}
