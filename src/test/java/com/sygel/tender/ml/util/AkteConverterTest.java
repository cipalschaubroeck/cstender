package com.sygel.tender.ml.util;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

import com.sygel.tender.domain.util.Util;
import com.sygel.tender.ml.KnowledgeBase;

public class AkteConverterTest extends TestCase {
	@Test
	public void testNormalizeCategory() throws IOException, InterruptedException
	{		  
		String normalized = AkteUtilities.normalizeCategory("o/cat. D1, kl. 2, kl. 1, kl. 1, kl. 1, kl. 1, kl. 1, kl. 2, kl. 2, kl. 2, kl. 2, kl. 2, kl. 1, kl. 1, kl. 1, kl. 1, kl. 1, kl. 2, kl. 2, kl. 2, kl. 2; o/cat. D1, kl. 2; o/cat. D5, kl. 1; o/cat. D14, kl. 1; o/cat. D20, kl. 1; o/cat. D16, kl. 1; o/cat. P1, kl. 1; o/cat. D4, kl. 2; o/cat. D5, kl. 2; o/cat. D10, kl. 2; o/cat. D11, kl. 2");
		System.out.println(normalized);
		assertEquals(":yyyD1:yyyD4:yyyD5:yyyD10:yyyD11:yyyD14:yyyD16:yyyD20:yyyP1:zzz1:zzz2:", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("dit is een test");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals("", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("dit is een cat. D12 jaja met klasse 3, ja");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD12:zzz3:", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("D12 jaja met klasse 3, ja");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD12:zzz3:", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("D12");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD12:", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("D 12");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD12:", normalized);

		normalized = AkteUtilities.eliminateSpaceBetweenCategoryNumber("D 12 plopD 12");
		System.out.println("eliminateSpaceBetweenCategoryNumber "+normalized);
		assertEquals(" D12 plopD 12 ", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass(" de categorieën D12 en D13 Les travaux faisant objet de la présente entreprise sont rangés dans la catégorie D8.  rentrent dans la classe 1.");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD8:yyyD12:yyyD13:zzz1:", normalized);

		normalized = AkteUtilities.getNormalizedCategoryAndClass("Vereiste categorie: D (Bouwwerken) D12 (huppeldepup), klasse 4 D1 (Ruwbouwwerken en onder kap brengen van gebouwen) , klasse 4");
		System.out.println("getNormalizedCategoryAndClass "+normalized);
		assertEquals(":yyyD:yyyD1:yyyD12:zzz4:", normalized);

	}

	@Test 
	public void testBda() {
		boolean isBda = AkteUtilities.isWordBDA("2015"+Util.HYPHEN_ESCAPE+"244444");
		System.out.println("isBda: "+isBda);
		assertEquals(true, isBda);
	}

	@Test
	public void testConstructLocation() throws IOException, InterruptedException
	{		  
		KnowledgeBase kn = new KnowledgeBase();	
		String normalized = AkteUtilities.constructLocation("ZNA Middelheim, Lindendreef 1, B-2020 Antwerpen., Lindendreef 1 te 2020 Antwerpen", "BE211", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l2020:lBE211:lBE21:", normalized);

		normalized = AkteUtilities.constructLocation("gemeente Heist o/d Berg", "BE212", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l2220:lBE212:lBE21:", normalized);

		normalized = AkteUtilities.constructLocation("Alle dialysecentra van de dienst nefrologie van het Jessa Ziekenhuis VZW en Centraal Magazijn Ekkelgaarden", "BE221", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":lBE221:lBE22:", normalized);

		normalized = AkteUtilities.constructLocation("Stad Aalst, Grote Markt 3 te Aalst", "BE231", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l9300:lBE231:lBE23:", normalized);	


		normalized = AkteUtilities.constructLocation("CHIREC Site DELTA, Boulevard du Triomphe 175 à 214 à Bruxelles",
				"BE1", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l1000:lBE100:lBE10:", normalized);	


		normalized = AkteUtilities.constructLocation("Diksmuide",
				"BEL", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l8600:lBE252:lBE25:", normalized);	

		normalized = AkteUtilities.constructLocation("Destelbergen, Lokeren",
				"BEL", kn.getCityList());
		System.out.println(normalized);
		assertEquals(":l9070:lBE234:lBE23:l9160:lBE236:lBE23:", normalized);

		// TODO
		// Woord alleen =  gemeente altijd goed
		// Sommige gemeenten die niet kunnen verward worden = altijd goed
		// cross check met aanbestedende overheid.
		// big data update gemeente namen, bv 9000 genti 2 maal gezien in de data dan is genti een juiste naam voor gent!
	}
}
