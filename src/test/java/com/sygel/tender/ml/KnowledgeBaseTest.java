package com.sygel.tender.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;

public class KnowledgeBaseTest extends TestCase {
	
	
	@Test 
	public void testUpgradeCpvMap() throws IOException, InterruptedException {
		KnowledgeBase nb = new KnowledgeBase();
		Map<String, String> cpvUpgradeMap = new HashMap<String, String>();
		nb.updateCpvUpgradeMap("!PA_excl_SA_EL,!PA_excl_SA,REL_(installeren|verwijderen)_regenwaterafvoerleiding", "45332300", cpvUpgradeMap);
	
		List<String> features = new ArrayList<String>(cpvUpgradeMap.keySet());
		Collections.sort(features);
		String[] expected1 = {
				"!PA_excl_SA_EL,!PA_excl_SA,REL_installeren_regenwaterafvoerleiding",
				"!PA_excl_SA_EL,!PA_excl_SA,REL_verwijderen_regenwaterafvoerleiding",
         };
		int i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected1[i++], feature);
		}		
		assertEquals(expected1.length, features.size());	
	}

	
	@Test
	public void testLotExtraction() throws IOException, InterruptedException
	{
		Date now = new Date();
		KnowledgeBase nb = new KnowledgeBase();
		System.out.println("testLotExtraction");
		List<String> features = new ArrayList<String>(nb.extractFeaturesAndConcepts("het is vier uur perceel ruwbouw-afbouw,centrale verwarming,sanitair,elektriciteit. Ja nu kunnen we verder"));
		Collections.sort(features);
		String[] expected1 = {
				//"CON_elektriciteit",
				"NE_centrale_verwarming",
				"NE_elektriciteit",
				"NE_ruwbouw-afbouw",
				"NE_sanitair",
				//"REL_LOT_afbouw",
				"REL_LOT_centrale_verwarming","REL_LOT_elektriciteit","REL_LOT_ruwbouw-afbouw","REL_LOT_sanitair",
				/*"REL_installeren_elektrische_installatie", "REL_installeren_hvac","REL_installeren_sa"*/};
		int i = 0;
		for (String feature : features) {
			System.out.println(feature);
			assertEquals(expected1[i++], feature);
		}		
		assertEquals(expected1.length, features.size());	

		features = new ArrayList<String>(nb.extractFeatures(" De huidige aanneming bestaat uit 2 delen :  • Deel A : Ruwbouw en afwerking  • Deel iii : Elektriciteit. • Ruwbouw en afwerking : Voorafgaande werken en sloopwerken, vloeren, wanden, plafond."));    
		Collections.sort(features);		
		String[] expected2 = {
				"NE_afwerking",
				"NE_deel",
				"NE_elektriciteit",
				"NE_plafond",
				"NE_ruwbouw",
				"NE_sloopwerk",
				"NE_vloer",
				"NE_voorafgaande_werken",
				"NE_wand",
				"REL_LOT_afwerking","REL_LOT_elektriciteit","REL_LOT_ruwbouw",
				"REL_afwerken_plafond",
				"REL_afwerken_sloopwerk",
				"REL_afwerken_vloer",
				"REL_afwerken_wand",
				"REL_afwerken_werken",
				"REL_werken_plafond",
				"REL_werken_sloopwerk",
				"REL_werken_vloer",
				"REL_werken_wand"
		};
		i = 0;
		for(String feature : features) {
			System.out.println(feature);
			assertEquals(expected2[i++], feature);
		}
		assertEquals(expected2.length, features.size());

		features = new ArrayList<String>(nb.extractFeatures(" NG TE ZUTENDAAL - DEEL III ELEKTRO - PERCEEL 3: ELEKTRONISCHE BEVEILIGING TEGEN BRAND fwerking : Voorafgaande werken en sloopwerken, vloeren, wanden, pla"));        
		Collections.sort(features);
		String[] expected3 = {"NE_brand"
				,"NE_elektriciteit"
				,"NE_elektronische_beveiliging"
				,"NE_sloopwerk"
				,"NE_vloer"
				,"NE_voorafgaande_werken"
				,"NE_wand"
				,"REL_LOT_elektriciteit","REL_LOT_elektronische_beveiliging","REL_werken_sloopwerk","REL_werken_vloer","REL_werken_wand"};
		i = 0;
		for(String feature : features) {
			System.out.println(feature);
			assertEquals(expected3[i++], feature);
		}
		assertEquals(expected3.length, features.size());

		features = new ArrayList<String>(nb.extractFeatures(" Nieuwbouw 18 klassen - perceel centrale verwarming/ventilatie/sanitair - OPEN OFFERTEVRAAG."));        
		Collections.sort(features);
		String[] expected4 = {
				"NE_centrale_verwarming"
				,"NE_klassen"
				,"NE_nieuwbouw"
				,"NE_sanitair"
				,"NE_ventilatie"
				,"REL_LOT_centrale_verwarming","REL_LOT_sanitair","REL_LOT_ventilatie","REL_nieuwbouw_klassen"};
		i = 0;
		for(String feature : features) {
			System.out.println(feature);
			assertEquals(expected4[i++], feature);
		}
		assertEquals(expected4.length, features.size());


		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" (elektriciteit,cv,... )"));        
		//		features = new ArrayList<String>(nb.extractFeatures("De aanneming omvat o.a.: sloopwerken, grondwerken, ruwbouwwerken, stabiliteit, hvac, elektriciteit en afwerking"));        
		Collections.sort(features);
		String[] expected5 = {
				"CON_elektriciteit",
				"NE_cv",
				"NE_elektriciteit",
				"REL_installeren_elektrische_installatie",
				"REL_installeren_hvac"
		};
		i = 0;
		for(String feature : features) {
			//System.out.println(feature);
			assertEquals(expected5[i++], feature);
		}
		assertEquals(expected5.length, features.size());

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" Visualisatie (her)aanleg van onderdoorgangen of bruggen "));        
		Collections.sort(features);
		String[] expected6 = {			
				"CON_aanleg",
				"NE_brug",
				"NE_onderdoorgang",
				"PA_allebruggen",
				"PA_bouwen_brug",
				"REL_aanleggen_brug",
				"REL_aanleggen_onderdoorgang"
		};
		i = 0;
		for(String feature : features) {
			//System.out.println(feature);
			assertEquals(expected6[i++], feature);
		}
		assertEquals(expected6.length, features.size());		

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" de reiniging en het onderhoud van de tunnels, De afbraak van de bestaande borstweringen uit metselwerk  "));        
		Collections.sort(features);
		String[] expected7 = {			
				"CON_afbraak",
				"CON_onderhoud",
				"NE_borstwering",
				"NE_metselwerk",
				"NE_onderhoud",
				"NE_tunnel",
				"PA_afbraakwerken",
				"PA_reinigen",
				"PA_tunnel",
				"REL_afbreken_borstwering",
				"REL_onderhouden_tunnel",
				"REL_reinigen_tunnel"
		};
		i = 0;
		for(String feature : features) {
			//System.out.println(feature);
			assertEquals(expected7[i++], feature);
		}
		assertEquals(expected7.length, features.size());		

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" bouwen Kemelbrug, Overbrugging Schmidtlaan Etterbeekpark Lijn 26 – km 10.940 Vernieuwingswerken van borstweringen  "));        
		Collections.sort(features);
		String[] expected8 = {			
				"CON_bouwen",
				"CON_overbrugging",
				"CON_vernieuwing",
				"NE_borstwering",
				"NE_bridge",
				"NE_lijn",
				"NE_park",
				"NE_street",
				"NE_vernieuwingswerk",
				"PA_allebruggen",
				"PA_bouwen_brug",
				"REL_bouwen_bridge",
				"REL_overbruggen_street",
				"REL_renoveren_borstwering"
		};
		i = 0;
		for(String feature : features) {
			//	System.out.println("\""+feature+"\",");
			assertEquals(expected8[i++], feature);
		}
		assertEquals(expected8.length, features.size());	

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" Vervangen mechanische uitrusting Visartbrug in Zeebrugge  "));        
		Collections.sort(features);
		String[] expected9 = {			
				"CON_uitrusting",
				"CON_vervang",
				"NE_bridge",
				"NE_mechanische_uitrusting",
				"PA_allebruggen",
				"REL_uitrusten_bridge",
				"REL_vervangen_mechanische_uitrusting",
				"REL_vervangen_uitrusting"
		};
		i = 0;
		for(String feature : features) {
			//	System.out.println("\""+feature+"\",");
			assertEquals(expected9[i++], feature);
		}
		assertEquals(expected9.length, features.size());	
		


		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" Renovatie lokalen-Sanitaire installatie-Loodgieterij  "));        
		Collections.sort(features);
		String[] expected10 = {			
				"CON_installatie",
				"CON_renovatie",
				"NE_lokaal",
				"NE_loodgieterij",
				"NE_sanitaire_installatie",
				"REL_installeren_loodgieterij",
				"REL_renoveren_CAT_gebouw",
				"REL_renoveren_installatie",
				"REL_renoveren_lokaal",
				"REL_renoveren_loodgieterij",
				"REL_renoveren_sanitaire_installatie",
				//"REL_renoveren_sanitaire_loodgieterij",
		};
		i = 0;
		for(String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected10[i++], feature);
		}
		assertEquals(expected10.length, features.size());	
		
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("LOT 2 : CV+SANITAIR+VENTILATIE"));        
		Collections.sort(features);
		String[] expected11 = {			
				"REL_LOT_cv",
				"REL_LOT_sanitair",
				"REL_LOT_ventilatie",
		};
		i = 0;
		for(String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected11[i++], feature);
		}
		assertEquals(expected11.length, features.size());	
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("sanitair  Exploitatiecontract voor de technische installaties "));        
		Collections.sort(features);
		String[] expected12 = {			
				"CON_installatie",
				"CON_technische installatie",
				"NE_exploitatiecontract",
				"NE_sanitair",
				"NE_technische_installatie",
				"PA_technieken_sa",
				"PA_technisch_beheer_sa",		};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected12[i++], feature);
		}
		assertEquals(expected12.length, features.size());	
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("  Exploitatiecontract voor de technische installaties "));        
		Collections.sort(features);
		String[] expected13 = {			
				"CON_installatie",
				"CON_technische installatie",
				"NE_exploitatiecontract",
				"NE_technische_installatie",
				"PA_technisch_beheer_cv",
				"PA_technisch_beheer_el",
				"PA_technisch_beheer_sa",
				"PA_technieken_sa",
				"PA_technieken_cv",
				"PA_technieken_el",
				};
		i = 0;
		for(String feature : features) {
			System.out.println("\""+feature+"\",");
			//assertEquals(expected13[i++], feature);
		}
		assertEquals(expected13.length, features.size());	
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("  Exploitatiecontract voor de technische installaties jaja elektro"));        
		Collections.sort(features);
		String[] expected14 = {		
				"CON_elektriciteit",
				"CON_installatie",
				"CON_technische installatie",
				"NE_elektriciteit",
				"NE_exploitatiecontract",
				"NE_technische_installatie",
				"PA_technieken_el",
				"PA_technisch_beheer_el",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected14[i++], feature);
		}
		assertEquals(expected14.length, features.size());	
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("ruwbouw, afwerking en technieken jaja het kan wel "));        
		Collections.sort(features);
		String[] expected15 = {		
				"CON_technieken",
				"NE_afwerking",
				"NE_ruwbouw",
				"NE_techniek",
				"PA_technieken_cv",
				"PA_technieken_el",
				"PA_technieken_sa",
				"REL_afwerken_techniek",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected15[i++], feature);
		}
		assertEquals(expected15.length, features.size());	
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("ruwbouw, afwerking en technieken  hvac"));        
		Collections.sort(features);
		String[] expected16 = {		
				"CON_technieken",
				"NE_afwerking",
				"NE_hvac",
				"NE_ruwbouw",
				"NE_techniek",
				"PA_technieken_cv",
				"REL_afwerken_techniek",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected16[i++], feature);
		}
		assertEquals(expected16.length, features.size());	
		
		
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("Verwarming - sanitair - ventilatie - elektriciteit"));        
		Collections.sort(features);
		String[] expected17 = {		
				"CON_elektriciteit",
				"NE_elektriciteit",
				"NE_sanitair",
				"NE_ventilatie",
				"NE_verwarming",
				"REL_installeren_elektrische_installatie",
				"REL_installeren_hvac",
				"REL_installeren_sa",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected17[i++], feature);
		}
		assertEquals(expected17.length, features.size());			
		
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("eerste uitrusting en technieken (elektra, sanitair, CV en ventilatie)"));        
		Collections.sort(features);
		String[] expected18 = {		
				"CON_elektriciteit",
				"CON_technieken",
				"CON_uitrusting",
				"NE_cv",
				"NE_elektro",
				"NE_sanitair",
				"NE_techniek",
				"NE_uitrusting",
				"NE_ventilatie",
				"PA_technieken_cv",
				"PA_technieken_el",
				"PA_technieken_sa",
				"REL_installeren_elektrische_installatie",
				"REL_installeren_hvac",
				"REL_installeren_sa",
				"REL_uitrusten_techniek",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected18[i++], feature);
		}
		assertEquals(expected18.length, features.size());			
		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("H. Hart Ziekenhuis Lier - Ombouw polikliniek - perceel 2 -VERWARMING/VENTILATIE/KOELING/SANITAIR"));        
		Collections.sort(features);
		String[] expected19 = {		
				//"CON_ziekenhuis",
				"NE_hart",
				"NE_koeling",
				"NE_lier",
				"NE_polikliniek",
				"NE_sanitair",
				"NE_ventilatie",
				"NE_verwarming",
				"NE_ziekenhuis",
				"PA_medischgebouw",
				"REL_LOT_koeling",
				"REL_LOT_sanitair",
				"REL_LOT_ventilatie",
				"REL_LOT_verwarming",
				/*"REL_ombouwen_CAT_ziekenhuis",
				"REL_ombouwen_polikliniek",*/
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected19[i++], feature);
		}
		assertEquals(expected19.length, features.size());			

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(" perceel H.V.A.C. java"));        
		Collections.sort(features);
		String[] expected20 = {		
				"NE_hvac",
				"NE_java",
				"REL_LOT_hvac",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected20[i++], feature);
		}
		assertEquals(expected20.length, features.size());		

		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("integreren van een ventilatiesysteem waarom geen relatie."));        
		Collections.sort(features);
		String[] expected21 = {		
				"NE_ventilatiesysteem",
				"REL_integreren_ventilatiesysteem",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected21[i++], feature);
		}
		assertEquals(expected21.length, features.size());		

		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("materiaal voor obo monoborstel"));        
		Collections.sort(features);
		String[] expected22 = {		
				"NE_materiaal",
				"NE_orïënterend_bodemonderzoek",
				"PA_bodemonderzoek",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected22[i++], feature);
		}
		assertEquals(expected22.length, features.size());		
	
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("leveren en plaatsen van een ondergrondse gaspijpleiding"));        
		Collections.sort(features);
		String[] expected23 = {		
				//"CON_leveren",
				"CON_plaatsing",
				"NE_ondergrondse_gaspijpleiding",
				"REL_leveren_gaspijpleiding",
				"REL_leveren_ondergrondse_gaspijpleiding",
				"REL_plaatsen_gaspijpleiding",
				"REL_plaatsen_ondergrondse_gaspijpleiding",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected23[i++], feature);
		}
		assertEquals(expected23.length, features.size());		

		
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("slopen bestaande polyvalente zaal en storend sanitair"));        
		Collections.sort(features);
		String[] expected24 = {		
				"CON_afbraak",
				"NE_polyvalente_zaal",
				"NE_sanitair",
				"REL_afbreken_CAT_gebouw",
				"REL_afbreken_CAT_polyvalente_gebouw",
				"REL_afbreken_polyvalente_zaal",
				"REL_afbreken_sanitair",
				"REL_afbreken_zaal",
				};
		i = 0;
		for(String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected24[i++], feature);
		}
		assertEquals(expected24.length, features.size());	
		System.out.println("last");
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("Leveren en plaatsen van 4 weegbruggen incl. aanleg van betonnen kuip voor weeg-brug"));        
		Collections.sort(features);
		String[] expected25 = {		
				"CON_aanleg",
				//"CON_leveren",
				"CON_plaatsing",
				"NE_betonnen_kuip",
				"NE_brug",
				"NE_weegbrug",
				"REL_aanleggen_betonnen_kuip",
				"REL_aanleggen_kuip",
				"REL_leveren_weegbrug",
				"REL_plaatsen_weegbrug",
				};
		i = 0;
		for(String feature : features) {
			//System.out.println("\""+feature+"\",");
			assertEquals(expected25[i++], feature);
		}
		assertEquals(expected25.length, features.size());	
		
		System.out.println("new");
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts("Het voorwerp van onderhavige opdracht omvat de levering van L.D.P.E. huisvuilzakken."));        
		Collections.sort(features);
		String[] expected26 = {		
				"CON_afbraak",
				"CON_bouwen",
				"NE_nieuwbouwproject",
				"NE_ontwerp",
				"REL_afbreken_nieuwbouwproject",
				"REL_bouwen_nieuwbouwproject",
				"REL_ontwerpen_nieuwbouwproject",
				};
		i = 0;
		for(String feature : features) {
			System.out.println("\""+feature+"\",");
			//assertEquals(expected25[i++], feature);
		}
		//assertEquals(expected25.length, features.size());	
		
		System.out.println("time:"+(new Date().getTime()-now.getTime()));

	  
		
		// 2015-526400 waarom geen SM, duidelijk SM !!!
		// 2015-528941 waarom geen SG, duidelijk schilderwerk
	}
	
	@Test
	public void testAugmentText() throws IOException, InterruptedException
	{
		Date now = new Date();
		KnowledgeBase nb = new KnowledgeBase();
		System.out.println("testAugmentText");
		String line = "WZC Ter Kimme St. Lievens-Houtem - uitbreiding + verbouwing - perceel KEUKEN - OPEN OFFERTEVRAAG";
		String augmentedLine = nb.augmentText(line);
		assertEquals("NE_woonzorgcentrum ter kimme sint lievens houtem VERB_uitbreiden + VERB_verbouwen LOT NE_keuken open offertevraag", augmentedLine);
		System.out.println(augmentedLine);
		line = "WZC Ter Kimme St. Lievens-Houtem - uitbreiding + verbouwing. KEUKEN - OPEN OFFERTEVRAAG";
		List<String> features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected1 = {
				"CON_verbouwing",
				"CON_woonzorgcentrum",
				"NE_keuken",
				"NE_woonzorgcentrum",
				"PA_medischgebouw",
				"REL_uitbreiden_woonzorgcentrum",
				"REL_verbouwen_woonzorgcentrum",
				};
		int i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected1[i++], feature);
		}		
		assertEquals(expected1.length, features.size());	
		
		line= "wordt een houten brugje geplaatst";
	  augmentedLine = nb.augmentText(line);
		assertEquals("wordt een NE_houten_brug VERB_plaatsen", augmentedLine);
	  System.out.println(augmentedLine);
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected2 = {
				"NE_houten_brug",
				"REL_plaatsen_houten_brug",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected2[i++], feature);
		}		
		assertEquals(expected2.length, features.size());	
	  
		line="wegenis voorzien";
	  augmentedLine = nb.augmentText(line);
		assertEquals("NE_wegenis VERB_voorzien", augmentedLine);
	  System.out.println(augmentedLine);
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected3 = {
				"CON_rijweg",
				"NE_wegenis",
				"PA_wegenwerken",
				"REL_voorzien_wegenis",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected3[i++], feature);
		}		
		assertEquals(expected3.length, features.size());	
	  
	  line = "slopen van een gedeelte van het bestaande politiekantoor";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  assertEquals("VERB_afbreken van een gedeelte van het bestaande NE_politiebureau", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected4 = {
				"CON_afbraak",
				"NE_politiebureau",
				"PA_afbraakwerken",
				"REL_afbreken_politiebureau",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected4[i++], feature);
		}		
		assertEquals(expected4.length, features.size());	
	  
	  line = "slopen van een gedeelte. van het bestaande politiekantoor";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  assertEquals("VERB_afbreken van een gedeelte . van het bestaande NE_politiebureau", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected5 = {
				"CON_afbraak",
				"NE_politiebureau",
				"PA_afbraakwerken",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected5[i++], feature);
		}		
		assertEquals(expected5.length, features.size());
		
	  line = "slopen van een gedeelte van het plop plop plop plop plop plop plop plop bestaande politiekantoor";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  assertEquals("VERB_afbreken van een gedeelte van het plop plop plop plop plop plop plop plop bestaande NE_politiebureau", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected6 = {
				"CON_afbraak",
				"NE_politiebureau",
				"PA_afbraakwerken",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected6[i++], feature);
		}		
		assertEquals(expected6.length, features.size());
		
	  line = "politiekantoor van een gedeelte van het bestaande slopen";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  assertEquals("NE_politiebureau van een gedeelte van het bestaande VERB_afbreken", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected7 = {
				"CON_afbraak",
				"NE_politiebureau",
				"REL_afbreken_politiebureau",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected7[i++], feature);
		}		
		assertEquals(expected7.length, features.size());
		
		
		//
    
	  line = "Basisschool DE KAMELEON Sint-Lambrechts-Herk, Verbouwing gebouwen C 2 en C 3, fase 3 (pleisterwerken, alu buitenschrijnwerk, CV, gedeeltelijke vernieuwing elektriciteit + data,...)";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  //assertEquals("NE_politiebureau van een gedeelte van het bestaande VERB_afbreken", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected8 = {
				"CON_bouwen",
				"CON_elektriciteit",
				"CON_schrijnwerk",
				"CON_verbouwing",
				"CON_vernieuwing",
				"NE_alu_buitenschrijnwerk",
				"NE_cv",
				"NE_data",
				"NE_elektriciteit",
				"NE_fase",
				"NE_gebouw",
				"NE_pleisterwerk",
				"NE_school",
				"REL_installeren_elektrische_installatie",
				"REL_installeren_hvac",
				"REL_renoveren_data",
				"REL_renoveren_elektriciteit",
				"REL_verbouwen_CAT_gebouw",
				"REL_verbouwen_gebouw",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected8[i++], feature);
		}		
		assertEquals(expected8.length, features.size());
	
		
		//
	  line = "Uitbreiding bestaande kantoren met nieuwbouwvleugel Perceel ruwbouw tot en met afwerking - zonder technieken";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  //assertEquals("NE_politiebureau van een gedeelte van het bestaande VERB_afbreken", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected9 = {
				"CON_technieken",
				"NE_afwerking",
				"NE_kantoren",
				"NE_ruwbouw",
				"NE_techniek",
				"PA_excl_techniek",
				"REL_LOT_ruwbouw",
				"REL_afwerken_techniek",
				"REL_uitbreiden_CAT_gebouw",
				"REL_uitbreiden_kantoren",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			assertEquals(expected9[i++], feature);
		}		
		assertEquals(expected9.length, features.size());
		
	  line = "Automatisatie van (sub)metering in gebouwen";
	  augmentedLine = nb.augmentText(line);
	  System.out.println(augmentedLine);
	  //assertEquals("NE_politiebureau van een gedeelte van het bestaande VERB_afbreken", augmentedLine); 
		features = new ArrayList<String>(nb.extractFeaturesAndConcepts(line));
		Collections.sort(features);
		String[] expected10 = {
				"CON_technieken",
				"NE_afwerking",
				"NE_kantoren",
				"NE_ruwbouw",
				"NE_techniek",
				"PA_excl_techniek",
				"REL_LOT_ruwbouw",
				"REL_afwerken_techniek",
				"REL_uitbreiden_CAT_gebouw",
				"REL_uitbreiden_kantoren",
				};
		i = 0;
		for (String feature : features) {
			System.out.println("\""+feature+"\",");
			//assertEquals(expected10[i++], feature);
		}		
		//assertEquals(expected10.length, features.size());
	}	
}
