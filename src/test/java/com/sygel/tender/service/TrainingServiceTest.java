package com.sygel.tender.service;

import junit.framework.TestCase;

import org.junit.Test;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.ml.KnowledgeBase;

public class TrainingServiceTest extends TestCase {
  @Test
  public void testUpgradeCpv() {
  	
  	KnowledgeBase knowledgeBase = new KnowledgeBase();
  	Tender tender = new Tender();
  	tender.setLanguage("NL");
  	tender.setTitle("NL","het plaatsen van beplating in fietstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels");
  	tender.setCpvNoParent("");
  	StringBuilder knowledgeCpv = new StringBuilder();
  	String cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(),  knowledgeBase,knowledgeCpv, null);
  	System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45221248", cpv);  	
  	  	
  	tender.setTitle("NL","excl lot sanitair");
  	tender.setEssence("NL","excl lot sanitair");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 	
  	assertEquals("", cpv);
  	
  	tender.setTitle("NL","lot sanitair");
  	tender.setEssence("NL","lot sanitair");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45332400", cpv);	


    
  	tender.setTitle("NL","lot elektro");
  	tender.setEssence("NL","excl lot elektro");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("", cpv);	
  	
  	tender.setTitle("NL","");
  	tender.setEssence("NL","");
  	tender.setCpvNoParent("18832100");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("18832100 18830000", cpv);	

  	tender.setTitle("NL","het plaatsen van beplating in fietstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels");
  	tender.setCpvNoParent("45221248");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45221248", cpv);	
  	assertEquals("45221248 ", knowledgeCpv.toString());
  	
  	tender.setTitle("NL","het plaatsen van beplating in fietstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels");
  	tender.setCpvNoParent("66221248");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("66221248 45221248", cpv);	
  	assertEquals("45221248 ", knowledgeCpv.toString());
  	
  	tender.setTitle("NL","(afbraak, herstel pleisterwerken, gipskartonwanden, akoestische plafonds, schilderwerken,elektriciteit en verlichting, inbraakbeveiliging)");
  	tender.setEssence("NL","(afbraak, herstel pleisterwerken, gipskartonwanden, akoestische plafonds, schilderwerken,elektriciteit en verlichting, inbraakbeveiliging)");
  	System.out.println(tender.getTitle());
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45310000 45410000 45111100 31527260 45442100 45421146 45421141", cpv);

  	tender.setTitle("NL","lot elektro");
  	tender.setEssence("NL","lot elektro");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45310000", cpv);	
  	
  	tender.setTitle("NL","installeren hvac");
  	tender.setEssence("NL","excl lot elektro");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("42500000 45331000", cpv);	

  	tender.setTitle("NL","Technisch beheerscontract");
  	tender.setEssence("NL"," . De dienstverlening vindt plaats in verschillende gebouwen: - recreatiedomein en zwembaden het Netepark - sporthal De Vossenberg - dorpshuis Noorderwijk - dorpshuis Morkhoven - Lakenhal - kasteel Le Paige");
  	tender.setCpvNoParent("");
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45332400 50711000 50720000", cpv);	

    
  	tender.setTitle("NL","het plaatsen van beplating in fietstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels");
  	System.out.println(tender.getTitle());
  	tender.setCpvNoParent("45332400"); //Installeren van sanitair
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(),tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45221248", cpv);	// 45332400 removed because it has nothing to do with fietstunnels.
  	assertEquals("45221248 ", knowledgeCpv.toString());

  	tender.setTitle("NL","het plaatsen van beplating in fietstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels en installeren van sanitair");
  	System.out.println(tender.getTitle());
  	tender.setCpvNoParent("45332400"); //Installeren van sanitair
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(), tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45332400 45221248", cpv);	// 45332400 NOT removed because "installeren van sanitair" is present in the text.
  	assertEquals("45221248 45332400 ", knowledgeCpv.toString());    
  	
  	tender.setTitle("NL","het bouwen van voetgangerstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels");
  	System.out.println(tender.getTitle());
  	tender.setCpvNoParent("45310000"); //aanleg van elektriciteit
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(), tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45221248 45221240", cpv);	// 45310000  removed because "aanleg van elektriciteit" is not present in the text.
  	assertEquals("45221248 45221240 ", knowledgeCpv.toString());    

  	tender.setTitle("NL","het bouwen van voetgangerstunnels");
  	tender.setEssence("NL","het plaatsen van beplating in fietstunnels en installeren van elektriciteit");
  	System.out.println(tender.getTitle());
  	tender.setCpvNoParent("45310000"); //aanleg van elektriciteit
  	knowledgeCpv = new StringBuilder();
    cpv = TrainingService.upgradeCpv(tender.getBda(), tender.getCpvNoParent(), tender.getTitle(), tender.getEssence(), knowledgeBase, knowledgeCpv, null);
    System.out.println("upgrade cpv "+cpv + " knowledgeCpv "+knowledgeCpv.toString()); 
  	assertEquals("45310000 45221248 45221240", cpv);	// 45310000  removed because "aanleg van elektriciteit" is not present in the text.
  	assertEquals("45221248 45221240 45310000 ", knowledgeCpv.toString());    
  
  }
}
