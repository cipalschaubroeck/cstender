#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:79930000 Gespecialiseerd ontwerpen Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79300000 Markt- en economieonderzoek; enquêtes en statistieken
TYPE_SERVICES:73100000 Onderzoek en experimentele ontwikkeling
TYPE_SERVICES:71540000 Coördinatie van bouwwerkzaamheden Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:71210000 Architectonische adviezen Percent : 100 Info : 5/2/null/null
TYPE_SERVICES:71317200 Gezondheids- en veiligheidsdiensten Percent : 100 Info : 3/2/null/null
TYPE_SERVICES:71421000 Landschaps- en tuinaanlegdiensten Percent : 100 Info : 2/2/null/null
TYPE_SERVICES:72242000 Maken van ontwerp modellen Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79415200 Ontwerpadviezen Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71541000 Bouwprojectenbeheer Percent : 100 Info : 3/2/null/null
TYPE_SERVICES:71351500 Bodemonderzoeksdiensten Percent : 100 Info : 11/3/null/null
TYPE_SERVICES:71323000 Technisch ontwerpen voor bedrijfsprocessen en -productie Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90712400 Planningsdiensten voor beheers- of beschermingsstrategie van natuurlijke rijkdommen Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:71311210 Adviezen inzake wegenbouw Percent : 100 Info : 17/1/null/null
TYPE_SERVICES:71315210 Advies inzake bouwdiensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71311300 Adviezen inzake infrastructuur Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:71317210 Advies inzake gezondheid en veiligheid Percent : 100 Info : 11/4/null/null
TYPE_SERVICES:90731000 Diensten in verband met luchtvervuling Percent : 100 Info : 4/4/null/null
TYPE_SERVICES:77231500 Diensten voor bosmonitoring en -beoordeling Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:85312300 Diensten voor begeleiding en adviesverlening Percent : 100 Info : 3/2/null/null
TYPE_SERVICES:79342000 Marketingdiensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71243000 Ontwerpplannen (systemen en integratie) Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:71315200 Bouwadvies Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:63721200 Exploitatie van haven Percent : 100 Info : 2/2/null/null
TYPE_SERVICES:79415000 Advies inzake productiebeheer Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:71621000 Diensten voor technische analyse of adviezen Percent : 100 Info : 5/4/null/null
TYPE_SERVICES:71244000 Kostenberekening en -bewaking Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:90715000 Diensten voor onderzoek naar vervuiling Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79410000 Advies inzake bedrijfsvoering en management Percent : 100 Info : 4/null/null/null
TYPE_SERVICES:71420000 Landschapsarchitectuurdiensten Percent : 100 Info : 9/2/null/null
TYPE_SERVICES:90711300 Analyse van milieu-indicatoren voor andere dan bouwwerkzaamheden Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:71321000 Technische ontwerpdiensten voor mechanische en elektrische installaties voor gebouwen Percent : 100 Info : 6/2/null/null
TYPE_SERVICES:71230000 Organisatie van een architectenprijsvraag Percent : 100 Info : 3/1/null/null
TYPE_SERVICES:90733700 Diensten voor toezicht op of beheersing van grondwatervervuiling Percent : 100 Info : 8/8/null/null
TYPE_SERVICES:90720000 Milieubescherming Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79998000 Begeleidingsdiensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71351612 Hydrometereologische diensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90711400 Uitvoeren van milieueffectstudie voor andere dan bouwwerkzaamheden Percent : 100 Info : 5/2/null/null
TYPE_SERVICES:79900000 Diverse zakelijke en andere diensten Percent : 100 Info : 3/null/null/null
TYPE_SERVICES:71314310 Diensten in verband met warmtetechniek voor gebouwen Percent : 100 Info : 1/null/null/null
TYPE_WORKS:71230000 Organisatie van een architectenprijsvraag Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:73111000 Diensten van researchlaboratorium Percent : 100 Info : 2/2/null/null
TYPE_SERVICES:98900000 Diensten verleend door extraterritoriale organisaties en instanties Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:73420000 Voorbereidende haalbaarheidsstudie en technologische demonstratie Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90714000 Uitvoeren van milieuaudits Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:90722000 Milieuherstel Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79822500 Maken van grafische ontwerpen Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:90711000 Milieueffectstudie voor andere dan bouwwerkzaamheden Percent : 100 Info : 3/2/null/null
TYPE_SERVICES:79311200 Diensten voor het uitvoeren van enquêtes Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79413000 Advies inzake marketing management Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90712000 Milieuplanning Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:79110000 Juridisch advies en juridische vertegenwoordiging Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71311230 Dienstverlening voor spoorwegenbouwtechniek Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:71251000 Architecten- en bouwinspectiediensten Percent : 100 Info : 3/1/null/null
TYPE_SERVICES:79411000 Advies inzake algemeen management Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:71322500 Technische ontwerpen van verkeersinstallaties Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:22314000 Ontwerpen Percent : 100 Info : 7/4/null/null
TYPE_SERVICES:71311000 Adviezen inzake bouwkunde Percent : 100 Info : 4/1/null/null
TYPE_SERVICES:79200000 Boekhoudkundige, audit- en fiscale diensten Percent : 100 Info : 3/3/null/null
TYPE_SERVICES:71313420 Milieunormen voor de bouw Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79210000 Boekhoudkundige en auditdiensten Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:79416200 Advies inzake public relations Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71322000 Technische ontwerpen voor constructie van civieltechnische werken Percent : 100 Info : 9/3/null/null
TYPE_SERVICES:79330000 Statistische dienstverlening Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:71356200 Technische bijstandsdiensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71311200 Adviezen inzake transportsystemen Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:71620000 Analysediensten Percent : 100 Info : 2/2/null/null
TYPE_SERVICES:79314000 Haalbaarheidsonderzoek Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:71320000 Technische ontwerpdiensten Percent : 97 Info : 28/10/1/null
TYPE_SERVICES:71400000 Dienstverlening op het gebied van stedenbouw en landschapsarchitectuur Percent : 97 Info : 30/11/1/null
TYPE_SERVICES:71241000 Haalbaarheidsstudie, adviesverlening, analyse Percent : 95 Info : 19/8/1/null
TYPE_SERVICES:71242000 Project- en ontwerpvoorbereiding, kostenraming Percent : 95 Info : 20/3/1/null
TYPE_SERVICES:71220000 Maken van bouwkundige ontwerpen Percent : 95 Info : 20/8/1/null
TYPE_SERVICES:71221000 Diensten door architectenbureaus voor gebouwen Percent : 94 Info : 32/18/2/null
TYPE_SERVICES:71250000 Dienstverlening van architecten en bouwkundigen, en controles Percent : 94 Info : 16/5/1/null
TYPE_SERVICES:71200000 Dienstverlening op het gebied van architectuur en dergelijke Percent : 94 Info : 91/65/6/null
TYPE_SERVICES:73000000 Onderzoek en ontwikkeling, en aanverwante adviezen Percent : 93 Info : 25/13/2/null
TYPE_SERVICES:71000000 Dienstverlening op het gebied van architectuur, bouwkunde, civiele techniek en inspectie Percent : 93 Info : 226/186/16/null
TYPE_SERVICES:71310000 Adviezen inzake techniek en bouw Percent : 93 Info : 14/7/1/null
TYPE_SERVICES:71240000 Dienstverlening op het gebied van architectuur, bouwkunde en planning Percent : 92 Info : 44/24/4/null
TYPE_SERVICES:71313400 Milieueffectbeoordeling voor bouwwerkzaamheden Percent : 91 Info : 10/5/1/null
TYPE_SERVICES:79311000 Uitvoeren van studies Percent : 91 Info : 43/31/4/null
TYPE_SERVICES:71314300 Advies inzake efficiënt energiegebruik Percent : 89 Info : 8/3/1/null
TYPE_SERVICES:71500000 Diensten in verband met de bouw Percent : 89 Info : 8/6/1/null
TYPE_SERVICES:71351914 Archeologische diensten Percent : 88 Info : 14/8/2/null
TYPE_SERVICES:71222000 Diensten door architectenbureaus voor buitenvoorzieningen Percent : 87 Info : 13/1/2/null
TYPE_SERVICES:71313000 Advies inzake milieutechniek Percent : 86 Info : 6/null/1/null
TYPE_SERVICES:71351920 Oceanografische en hydrologische diensten Percent : 86 Info : 6/6/1/null
TYPE_SERVICES:71223000 Diensten door architectenbureaus voor uitbreidingsverbouwingen Percent : 86 Info : 12/6/2/null
TYPE_SERVICES:71355200 Topografische opmetingen Percent : 83 Info : 5/2/1/null
TYPE_SERVICES:79400000 Adviezen inzake bedrijfsvoering en management, en aanverwante diensten Percent : 83 Info : 10/5/2/null
TYPE_SERVICES:71300000 Dienstverlening door ingenieurs Percent : 82 Info : 41/11/9/null
TYPE_SERVICES:71318000 Technische advies- en raadgevingsdiensten Percent : 82 Info : 9/3/2/null
TYPE_SERVICES:71330000 Diverse technische dienstverlening Percent : 80 Info : 4/2/1/null
TYPE_SERVICES:90710000 Milieubeheer Percent : 80 Info : 4/3/1/null
TYPE_SERVICES:71312000 Bouwkundig advies Percent : 80 Info : 4/1/1/null
TYPE_SERVICES:79420000 Diensten op het gebied van management Percent : 80 Info : 4/2/1/null
TYPE_SERVICES:79100000 Juridische dienstverlening Percent : 75 Info : 3/2/1/null
TYPE_SERVICES:71317000 Advies inzake beveiliging en risicobeheersing Percent : 75 Info : 6/3/2/null
TYPE_WORKS:71250000 Dienstverlening van architecten en bouwkundigen, en controles Percent : 75 Info : 3/2/1/null
TYPE_WORKS:71200000 Dienstverlening op het gebied van architectuur en dergelijke Percent : 75 Info : 9/null/3/null
TYPE_WORKS:71400000 Dienstverlening op het gebied van stedenbouw en landschapsarchitectuur Percent : 75 Info : 6/1/2/null
TYPE_SERVICES:79310000 Uitvoeren van marktonderzoek Percent : 75 Info : 3/null/1/null
TYPE_SERVICES:73110000 Uitvoeren van onderzoek Percent : 75 Info : 6/5/2/null
TYPE_SERVICES:71335000 Technische onderzoeken Percent : 75 Info : 3/null/1/null
TYPE_SERVICES:92330000 Diensten inzake recreatiegebieden Percent : 75 Info : 3/3/1/null
TYPE_SERVICES:71350000 Dienstverlening inzake wetenschappen en techniek Percent : 71 Info : 5/2/2/null
TYPE_SERVICES:71332000 Geotechnische diensten Percent : 67 Info : 2/1/1/null
TYPE_SERVICES:79419000 Evaluatie-adviesdiensten Percent : 67 Info : 2/null/1/null
TYPE_SERVICES:71245000 Plannen ter goedkeuring, werktekeningen en specificaties Percent : 67 Info : 2/null/1/null
TYPE_SERVICES:71317100 Advies inzake brand- en explosiebeveiliging en -controle Percent : 67 Info : 2/null/1/null
TYPE_SERVICES:79212000 Auditdiensten Percent : 67 Info : 2/2/1/null
TYPE_WORKS:71300000 Dienstverlening door ingenieurs Percent : 63 Info : 5/null/3/null
TYPE_SERVICES:79340000 Reclame- en marketingdiensten Percent : 60 Info : 3/1/2/null
TYPE_WORKS:71221000 Diensten door architectenbureaus voor gebouwen Percent : 56 Info : 5/2/4/null
TYPE_WORKS:71251000 Architecten- en bouwinspectiediensten Percent : 50 Info : 1/1/1/null
TYPE_SERVICES:71600000 Diensten voor technische testen, analyse en adviezen Percent : 50 Info : 4/3/4/null
TYPE_WORKS:70112000 Projectontwikkeling voor onroerend goed anders dan woningen Percent : 50 Info : 2/1/2/null
TYPE_SERVICES:73200000 Advies inzake onderzoek en ontwikkeling Percent : 50 Info : 2/1/2/null
TYPE_WORKS:45112712 Landschappelijk ontwerp voor tuinen Percent : 50 Info : 1/null/1/null
TYPE_SERVICES:73300000 Planning en uitvoering van onderzoek en ontwikkeling Percent : 50 Info : 1/1/1/null
TYPE_SERVICES:73400000 O&O-diensten voor beveiligings- en defensiematerialen
TYPE_SERVICES:71325000 Ontwerpen van fundering Percent : 50 Info : 1/null/1/null
TYPE_SERVICES:71311100 Ondersteunende diensten voor bouwkunde Percent : 50 Info : 1/null/1/null
TYPE_SERVICES:90713000 Adviesdiensten inzake milieuproblematiek Percent : 50 Info : 3/2/3/null
TYPE_WORKS:71240000 Dienstverlening op het gebied van architectuur, bouwkunde en planning Percent : 50 Info : 1/null/1/null
TYPE_SERVICES:79411100 Advisering omtrent bedrijfsontwikkeling Percent : 50 Info : 1/null/1/null
TYPE_WORKS:71000000 Dienstverlening op het gebied van architectuur, bouwkunde, civiele techniek en inspectie Percent : 48 Info : 15/7/16/null
TYPE_WORKS:71223000 Diensten door architectenbureaus voor uitbreidingsverbouwingen Percent : 33 Info : 1/1/2/null
TYPE_WORKS:71421000 Landschaps- en tuinaanlegdiensten Percent : 8 Info : 1/null/11/null
TYPE_SERVICES:79417000 Veiligheidsadviezen Percent : 100 Info : 2/null/null/null
TYPE_WORKS:71315100 Advies inzake ruwbouw Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71247000 Toezicht op bouwwerkzaamheden Percent : 13 Info : 1/null/7/null
TYPE_SERVICES:70110000 Projectontwikkelaarsdiensten Percent : 100 Info : 1/1/null/null
TYPE_WORKS:71248000 Toezicht op plannen en documentatie Percent : 100 Info : 1/null/null/null
TYPE_WORKS:79620000 Diensten voor de terbeschikkingstelling van personeel, met inbegrip van tijdelijk personeel
TYPE_SERVICES:71700000 Meet- en controlediensten
TYPE_SERVICES:71354100 Digitale kartering
TYPE_SERVICES:71800000 Adviesdiensten voor watervoorziening en afval
TYPE_SERVICES:71311220 Dienstverlening voor wegenbouwtechniek
TYPE_SERVICES:79723000 Diensten voor afvalanalyse
TYPE_SERVICES:79000000 Zakelijke dienstverlening: juridisch, marketing, consulting, drukkerij en beveiliging