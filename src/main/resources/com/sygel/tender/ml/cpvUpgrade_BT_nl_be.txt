#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 44212100 Brug
# 44212120 Metaalconstructies van bruggen
# 45221000 Bouwen van bruggen en tunnels, schachten en ondergrondse doorgangen
# 45221100 Bruggenbouwwerkzaamheden
NE_brugdek,PA_allebruggen 45221100
NE_beweegbare_brug 45221100
NE_brugcomplex 45221100
REL_(afbreken|vervangen|plaatsen)_brugranden 45221100 4
REL_(uitvoeren|realiseren)_landhoofden 45221100 2
REL_uitvoeren_betonnen_landhoofden 45221100 1
REL_(uitrusten|automatiseren|onderhouden|ontwerpen)_bridge 45221100 2
REL_(bouwen|uitvoeren|realiseren|ontwerpen)_betonnen_landhoofden 45221100 1
REL_(bouwen|uitvoeren|realiseren|ontwerpen)_landhoofden 45221100 1
REL_(uitrusten|automatiseren|bouwen|ontwerpen)_klapbrug 45221100 3
REL_(bouwen|uitvoeren|realiseren|vervangen|ontwerpen)_ophaalbrug 45221100 1
REL_(bouwen|uitvoeren|realiseren|vervangen|ontwerpen)_landhoofden 45221100 1
REL_(bouwen|uitvoeren|realiseren|vervangen|ontwerpen)_betonnen_landhoofden 45221100 1
REL_(uitrusten|automatiseren|vervangen|bouwen|ontwerpen)_hefbrug 45221100 2
REL_afbreken_scheenmuur 45221100 1
REL_(uitrusten|automatiseren|bouwen|uitvoeren|realiseren|vervangen|ontwerpen)_klapbrug 45221100 3
REL_aanbrengen_leuning,PA_allebruggen 45221100 3
REL_herstellen_brugstructuur 45221100 3
REL_(nazien|onderhouden|vervangen|bouwen|ontwerpen)_brug 45221100 3
# 45221110 Bruggenbouw
PA_bouwen_brug 45221110 1
PA_bouwen_brug_TITLE 45221110 1
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_brugconstructie 45221110 9
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|verbreden|ontwerpen|maken)_brug 45221110 9
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_bridge 45221110 9
REL_overbruggen_street 45221110 1
REL_bouwen_brugdek 45221110 1
# 45221111 Bouwen van verkeersbrug
# 45221112 Bouwen van spoorbrug
NE_spoorbrugdek 45221112
NE_spoorbrug 45221112
# 45221113 Bouwen van voetgangersbrug
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_loopbrug 45221113 2
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_voetgangersbrug 45221113 2
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_fietsbrug 45221113 1
# 45221114 Bouwwerkzaamheden voor ijzeren bruggen
REL_schilderen_brug 45221114 1
REL_schilderen_brug_TITLE 45221114 1
# 45221115 Bouwwerkzaamheden voor stalen bruggen
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_stalen_brug 45221115 1
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_metalen_brug 45221115 1
# 45221119 Brugrenovatiebouw
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|schouwen|onderhouden|saneren|herbouwen)_kunstwerk 45221119 2
REL_(uitbreken|verwijderen)_brugdekvoeg 45221119 2
REL_afbreken_brug_TITLE 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_rok 45221119 4
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_waterdichte_rok 45221119 4
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_onderbrugging 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_brugvoeg 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_waterdichte_rokken_TITLE 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_brugdek 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_brug 45221119 9
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen)_boogbrug 45221119 9
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_rokken 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_waterdichte_rokken 45221119 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_overkapping 45221200 1
REL_(restaureren|renoveren|afbreken|verwijderen|saneren|herstellen|herbouwen|vervangen|vernieuwen|aanbrengen)_bridge 45221200 2 null
# 45221121 Bouwen van verkeersviaduct
# 45221122 Bouwen van spoorwegviaduct
# 45221200 Bouwen van tunnels, schachten en ondergrondse doorgangen
REL_realiseren_inkokering 45221200 1
REL_(leveren|plaatsen)_amfibietunnels 45221200 2
# 45221211 Onderdoorgang
REL_(uitvoeren|afwerken|bouwen|aanleggen|vernieuwen|renoveren|graven|realiseren)_onderdoorgang 45221211 3
REL_(uitvoeren|afwerken|bouwen|aanleggen|vernieuwen|renoveren|graven|realiseren)_onderbrugging 45221211 1
# 45221220 Duikers
REL_(renoveren|herinrichten|herstellen|ruimen)_duiker 45221220 4
# 45221240 Bouwen van tunnels
PA_bouwen_tunnel 45221240 1
PA_bouwen_tunnel_TITLE 45221240 1
REL_(bouwen|herinrichten|herstellen|onderhouden)_tunnel 45221240 1
REL_(bouwen|herinrichten|herstellen|onderhouden)_tunnels 45221240 2
# 45221241 Bouwen van verkeerstunnel
# 45221242 Bouwen van spoorwegtunnel
# 45221243 Bouwen van voetgangerstunnel
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_fietstunnel 45221243 1
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_fietserstunnel 45221243 1
REL_(uitrusten|installeren|bouwen|vervangen|vernieuwen|realiseren|aanleggen|leveren|plaatsen|ontwerpen|maken)_voetgangerstunnel 45221243 1
# 45221248 Aanbrengen van tunnelbekleding
NE_tunnelbekleding 1
REL_plaatsen_brandwerende_beplating_TITLE,PA_tunnel 45221248 1
REL_plaatsen_brandwerende_beplating,PA_tunnel 45221248 1
REL_plaatsen_beplating_TITLE,PA_tunnel 45221248 1
REL_plaatsen_beplating,PA_tunnel 45221248 1
# 45233126 Bouwen van ongelijkvloerse kruising
# 45247130 Bouwen van aquaduct
