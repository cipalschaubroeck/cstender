#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SUPPLIES:45248200 bouwen van droogdokken
TYPE_SUPPLIES:38420000 Instrumenten voor het meten van debiet, peil en druk van vloeistoffen en gassen Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45244200 Havensteigers Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45247120 Waterwegen, met uitzondering van kanalen Percent : 100 Info : 12/9/null/null
TYPE_WORKS:45246410 Onderhoud van hoogwaterkeringen Percent : 100 Info : 4/null/null/null
TYPE_SERVICES:90733200 Opruimdiensten voor vervuild oppervlaktewater Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45244000 Maritieme bouwwerkzaamheden Percent : 100 Info : 2/2/null/null
TYPE_WORKS:45243510 Aanleg van dijken en kades Percent : 100 Info : 3/null/null/null
TYPE_WORKS:90513600 Diensten voor slibverwijdering Percent : 100 Info : 3/1/null/null
TYPE_WORKS:45242210 Bouwen van jachthaven Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45246400 Werkzaamheden voor hoogwaterpreventie Percent : 100 Info : 4/2/null/null
TYPE_SUPPLIES:38421000 Debietmeetuitrusting Percent : 100 Info : 1/null/null/null
TYPE_WORKS:90513800 Diensten voor slibbehandeling Percent : 100 Info : 3/1/null/null
TYPE_WORKS:45248400 Bouwen van landingssteigers Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:45246200 Oeverbeschermingswerken Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:34516000 Fenders voor de marine Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:98363000 Duikdiensten Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:45248300 Bouwwerkzaamheden voor drijvende dokken Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45262211 Heien Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90733300 Diensten voor bescherming van oppervlaktewater tegen vervuiling Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:63721000 Exploitatie van havens en waterwegen en aanverwante diensten Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45252124 Bagger- en pompwerkzaamheden Percent : 100 Info : 14/10/null/null
TYPE_SUPPLIES:34931000 Uitrusting voor havens Percent : 100 Info : 1/1/null/null
TYPE_WORKS:71354400 Hydrografische diensten Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45241000 Havenbouwwerkzaamheden Percent : 100 Info : 3/1/null/null
TYPE_SERVICES:45240000 Waterbouwwerkzaamheden Percent : 100 Info : 1/null/null/null
TYPE_WORKS:71351923 Diensten voor bathymetrische metingen Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45246000 Werkzaamheden voor rivierdebietregeling en hoogwaterbeheersing Percent : 100 Info : 45/45/null/null
TYPE_SERVICES:45232451 Afwaterings- en oppervlaktewerkzaamheden Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:45244200 Havensteigers Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45247100 Bouwwerkzaamheden voor waterwegen Percent : 91 Info : 10/6/1/null
TYPE_WORKS:45246200 Oeverbeschermingswerken Percent : 90 Info : 9/5/1/null
TYPE_WORKS:45240000 Waterbouwwerkzaamheden Percent : 87 Info : 67/45/10/null
TYPE_WORKS:45241100 Bouwen van kades Percent : 80 Info : 4/1/1/null
TYPE_WORKS:45243600 Bouwen van kademuren Percent : 78 Info : 7/null/2/null
TYPE_SERVICES:63721300 Exploitatie van waterwegen Percent : 75 Info : 3/3/1/null
TYPE_WORKS:34514200 Opvijzelbare platforms Percent : 67 Info : 2/null/1/null
TYPE_SERVICES:50240000 Reparatie, onderhoud en aanverwante diensten in verband met scheepvaart en andere uitrusting Percent : 67 Info : 2/null/1/null
TYPE_WORKS:45247230 Bouwen van dijk Percent : 60 Info : 3/null/2/null
TYPE_SERVICES:50241000 Reparatie en onderhoud van schepen Percent : 60 Info : 3/1/2/null
TYPE_WORKS:45232451 Afwaterings- en oppervlaktewerkzaamheden Percent : 50 Info : 2/null/2/null
TYPE_WORKS:45233200 Diverse oppervlaktewerkzaamheden Percent : 33 Info : 6/1/12/null
TYPE_WORKS:45243000 Kustverdedigingswerken
TYPE_WORKS:45243400 Werkzaamheden ter voorkoming van strandafslag
TYPE_WORKS:45243500 Bouwen van zeewering
TYPE_WORKS:45247270 Bouw van waterbekken Percent : 67 Info : 2/1/1/null
TYPE_WORKS:50246000 Onderhoud van havenuitrusting
TYPE_WORKS:50246200 onderhoud van boeien
TYPE_WORKS:45247110 aanleg van kanaal
TYPE_WORKS:45248100 bouwen van kanaalsluizen
TYPE_WORKS:90733400 Diensten voor behandeling van oppervlaktewater