#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_WORKS:44114250 Betonplaten Percent : 100 Info : 1/null/null/null
TYPE_WORKS:39530000 Tapijten, matten en vloerkleden Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45431200 Zetten van wandtegels Percent : 100 Info : 4/1/null/null
TYPE_WORKS:45432111 Leggen van flexibele vloerbekleding Percent : 100 Info : 9/2/null/null
TYPE_WORKS:45432113 Leggen van parketvloeren Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45432210 Aanbrengen van wandbekleding Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45432120 Aanbrengen van dubbele vloer Percent : 100 Info : 1/null/null/null
TYPE_WORKS:44112230 Linoleum Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45431000 Tegelwerk Percent : 92 Info : 11/6/1/null
TYPE_WORKS:45432000 Leggen van vloeren en aanbrengen van vloerbedekking, wandbekleding en behang Percent : 80 Info : 8/6/2/null
TYPE_WORKS:45432100 Aanbrengen en afwerken van vloeren Percent : 73 Info : 8/3/3/null
TYPE_WORKS:45432110 Leggen van vloeren Percent : 67 Info : 2/null/1/null
TYPE_WORKS:45431100 Leggen van vloertegels Percent : 64 Info : 7/2/4/null
TYPE_WORKS:45432130 Vloerbekledingswerk Percent : 60 Info : 6/1/4/null
TYPE_WORKS:45430000 Aanbrengen van vloer- en wandbekleding Percent : 57 Info : 13/8/10/null
TYPE_WORKS:44112200 Vloerbedekking Percent : 33 Info : 1/null/2/null
TYPE_WORKS:45410000 Pleisterwerk Percent : 10 Info : 3/null/28/null
TYPE_WORKS:39190000 behangselpapier en andere bekled
TYPE_WORKS:39191100 behangselpapier
TYPE_WORKS:45432200 aanbrengen van wandbekleding en
TYPE_WORKS:45432220 behangen
TYPE_WORKS:45432114 leggen van houten vloerbedekkinge
TYPE_WORKS:44112220 verhoogde vloeren
TYPE_WORKS:44112210 hard vloermateriaal
TYPE_WORKS:39193000 vloerbedekking op papier- of kart
TYPE_WORKS:39191000 wandbekleding op papier- of karto
TYPE_WORKS:39192000 wandbekleding van textiel
TYPE_WORKS:45262212 Aanbrengen van wandbekleding, beschotting, grondkeringen Percent : 50 Info : 1/null/1/null