#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 32354110 Röntgenfilm
NE_röntgenfilm 32354110
# 32354120 Blauwediazofilm
NE_blauwediazofilm 32354120
# 32354100 Film voor radiologie
NE_film,NE_radiologie 32354100
# 31532910 Fluorescentiebuizen 
NE_fluorescentiebuis 31532910
# 70310000 verhuur of verkoop van gebouwen
REL_(verkopen|aankopen|verhuren|huren)_gebouw 70310000
REL_(verkopen|aankopen|verhuren|huren)_school 70310000
REL_(verkopen|aankopen|verhuren|huren)_CAT_gebouw 70310000
# 37535200 speeltuinuitrusting
NE_speeltuinuitrusting 37535200
NE_speelinfrastructuur 37535200
REL_(plaatsen|leveren|ontwerpen|realiseren)_speeltuin 37535200
REL_(plaatsen|leveren|ontwerpen|realiseren)_speeltuig 37535200
REL_(plaatsen|leveren|ontwerpen|realiseren)_speeltoestel 37535200
REL_(plaatsen|leveren|ontwerpen|realiseren)_speelinfrastructuur 37535200
REL_(plaatsen|leveren|ontwerpen|realiseren)_speeltuinuitrusting 37535200
# 34942000 signalisatie-uitrusting
NE_signalisatiedrager 34942000
NE_signalisatie 34942000
NE_informatiepaneel 34942000
NE_informatiescherm 34942000
NE_havensignalisatie 34942000
REL_LOT_signalisatie 34942000
# 39100000 Meubilair
NE_veiligheidsmeubilair 39100000
NE_auditoriumstoel 39100000
NE_labomeubelen 39100000
NE_labomeubilair 39100000
NE_meubilair 39100000
NE_schoolmeubilair 39100000
NE_reftermeubilair 39100000
NE_stoel,NE_tafel 39100000
NE_bureaustoel 39100000
REL_(plaatsen|leveren|aankopen)_bureau 39100000
REL_(plaatsen|leveren|aankopen)_stoel 39100000
REL_(plaatsen|leveren|aankopen)_kleedkast 39100000
REL_LOT_meubilair 39100000
REL_(plaatsen|leveren|aankopen)_meubilair 39100000
REL_(plaatsen|leveren|aankopen)_CAT_meubilair 39100000
NE_binnenmeubilair 39100000
REL_LOT_binnenmeubilair 39100000
NE_kantoormeubilair 39100000
NE_bemeubeling 39100000
NE_bibliotheekinrichting 39100000
# 33192300 Medisch meubilair, met uitzondering van bedden en tafels
REL_leveren_geriatrische_stoel 33192300
NE_medisch_meubilair 33192300
NE_behandelingszetel 33192300
NE_geriatrische_stoel 33192300
NE_meubilair,NE_ziekenhuis 33192300
NE_meubilair,PA_medischgebouw 33192300
NE_patiënt,NE_matras 33192300
NE_patiënt,NE_matras 33192300
NE_alternatingsmatras 33192300
# 33192120 Ziekenhuisbedden
NE_bedhoofdtoestel 33192120
CON_verzorgingsbed 33192120
NE_ziekenhuisbed 33192120
PA_medischgebouw,NE_bed 33192120
# 79512000 telefonisch informatiecentrum
NE_call_center 79992000
NE_contact_center 79992000
NE_callcenterdienst 79992000
# 79992000 - Receptiediensten
NE_receptiedienst 79992000
# 45343200 - Installeren van brandblusinstallaties
REL_installeren_brandblusinstallatie 45343200
REL_installeren_brandblustoestel 45343200
NE_sprinklersysteem 45343200
NE_brandhaspel 45343200
# 35111000 - Brandblusuitrusting
NE_brandslang 35111000
NE_brandblusuitrusting 35111000
NE_brandbestrijdingsmiddelen 35111000
# 44482200 44482000 Brandkranen  /  
NE_brandkraan 44482200
# 44482100 44482000 Brandslangen  /  
NE_brandslang 44482100
NE_brandleiding 44482100
NE_brandhaspel 44482100
# 45343100 45343000 Aanbrengen van brandwering  /  
REL_aanbrengen_brandwering 45343100
NE_brandwering 45343100
NE_brandwerende_klep 45343100
# 35111500 Brandbestrijdingssysteem
# 50413200 - Reparatie en onderhoud van brandblusinstallaties
REL_onderhouden_brandblusinstallatie 50413200
REL_repareren_brandblusinstallatie 50413200
NE_blussysteem 50413200
# 79710000 - Beveiligingsdiensten
NE_oproepdienst 79710000
NE_patrouilledienst 79710000
NE_beveiligingsdienst 79710000
REL_beheren_controlecentrum 79710000
REL_garanderen_veiligheid 79710000
NE_waardentransportdienst 79710000
REL_beheren_elektronisch_toezicht_TITLE 79710000 2
REL_bewaken_interventies_TITLE 79710000 2
REL_beheren_elektronisch_toezicht 79710000 2
# 35120000 bewakings- en beveiligingssystemen en ?appa
# 35121000 beveiligingsuitrusting
NE_veiligheidsmat 35121000
NE_beveiligingsuitrusting 35121000
CON_veiligheidszakken 35121000
NE_perimeteromheining 35121000
NE_perimeterbeveiliging 35121000
NE_automatische_identificatie 35121000
# 80410000 diverse schooldiensten
NE_schooldienst 80410000
NE_infosessie 80410000