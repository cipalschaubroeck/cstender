<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.sygel.tender.service.PushService"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="java.net.URLEncoder"%>
<% 
String filterName = request.getParameter("filter");
try {
	filterName = URLEncoder.encode(filterName, "UTF-8");
} catch (UnsupportedEncodingException e) {
}
%>
<% response.sendRedirect(PushService.BASEURL+filterName+"/go"); %>
<!-- Landing page for EMAIL, so Google analytics pics it up. 

<html>
<head>
<meta charset="utf-8" /> 
</head>
<body>
<a href="<%=PushService.BASEURL%><%=filterName%>/go">MY TENDER</a>
<script type="text/javascript" language="JavaScript"><!--
//setTimeout('Redirect()',500);
function Redirect()
{
 location.href = '<%=PushService.BASEURL%><%=filterName%>/go';
}
// </script>
</body>
</html>
-->