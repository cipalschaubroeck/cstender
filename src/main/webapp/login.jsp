<!DOCTYPE form PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- /*
 * Copyright � Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */ -->
<%@page import="javax.inject.Inject"%>
<%@page import="com.sygel.tender.service.I18Service"%>
<%@page import="com.sygel.tender.util.CdiHelper"%>
<%!@Inject I18Service i18Service = null;%>
<%try {if (i18Service == null) i18Service = CdiHelper.lookupInject(I18Service.class);} catch (Exception e) {e.printStackTrace();}%> 
<html>
<head>
<title>Sygel Aanbestedingen</title>
<style>
.valo-title {
    font-family: 'Open Sans', sans-serif;
    line-height: 1.2;
    background-color: #197de1;
    color: white;
    padding: 100px 50px;
    font-size: 40px;
    text-align: center;
}
.valo-content {
    font-family: 'Open Sans', sans-serif;
    line-height: 1.2;
    background-color: #ffffff;
    color: black;
    padding: 12px 19px;
    font-size: 14px;
    text-align: center;
}
</style>
</head>
<body>

<div class="valo-title">
<%= i18Service.getMessage("PRODUCT_NAME", request.getLocale()) %>
</div>

<div class="valo-content">
<form action="j_security_check" method=post>
    <p>
        <strong><%= i18Service.getMessage("USER", request.getLocale()) %>:&nbsp;&nbsp;&nbsp;&nbsp;</strong>
        <input type="text" name="j_username" size="25">
    <p>

    <p>
        <strong><%= i18Service.getMessage("USER_PASSWORD", request.getLocale()) %>: </strong><input type="password" size="25" name="j_password">
        
    <p>

    <p>
    <input type="submit" value="<%= i18Service.getMessage("LOGIN", request.getLocale()) %>">
    <input type="reset" value="<%= i18Service.getMessage("CANCEL", request.getLocale()) %>">
</form>
<!-- 
<p>
  Demo versie, mag enkel gebruikt worden binnen Cipal
  voor evaluatie doeleinden zoals bepaald in het contract &quot;Overeenkomst voorstudie en optie tot aankoop sygel aanbestedingen&quot; van 7 maart 2016 tussen Sygel en Cipal.  Deze software mag buiten Cipal niet aan derden getoond worden.</p>
   -->
  <%= i18Service.getMessage("COPYRIGHT", request.getLocale()) %>

</div>
</body>
</html>