/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.Award;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contact;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.Information;
import com.sygel.tender.domain.NoticeText;
import com.sygel.tender.domain.PreInformation;
import com.sygel.tender.domain.Rectification;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Lot;
import com.sygel.tender.domain.util.Loten;
import com.sygel.tender.domain.util.Util;
import com.sygel.tender.ml.BuildModels;
import com.sygel.tender.ml.KnowledgeBase;
import com.sygel.tender.ml.MLearningModels;
import com.sygel.tender.ml.classify.ClassifierInterface;
import com.sygel.tender.ml.classify.FeatureList;
import com.sygel.tender.ml.util.AkteUtilities;
import com.sygel.tender.ml.util.XmlUtilities;

/**
 * Import service for Tenders
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Stateless
@Local
public class ImportService {

	@PersistenceContext(unitName = "tender")
	private EntityManager entityManager;

	@Inject
	KnowledgeService knowledgeService;

	@Inject
	SearchService searchService;
	
	@Inject
	TrainingService trainingService;

	private final Logger logger = Logger.getLogger(ImportService.class);
	
	public static final String DATA_FOLDER = BuildModels.rootDataFolder + "data/tender/be";

	public static void writeInputStream(InputStream in, File dst) throws IOException {
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static String getDoneFileName(File file) {
		String dateString = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
		return file.getName().substring(0, file.getName().length() - 4) + "_" + dateString + ".zip";
	}


	
	/**
	 * The BES xml and document XML is extracted from the ZIP.  The notice is not downloaded from enot.
	 * This function can be used for old ZIP files which are no longer downloadable from ENOT.
	 * @param folder
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void importTenderDirectFromZip(File file) {
		if (file.getName().endsWith(".zip")) {
			logger.trace("importTenderFolderDirectFromZip "+file.getName());
			List<String> noticeIds;
			try {
				noticeIds = extractNoticeIds(file);
				Collections.sort(noticeIds);
				ZipFile zipFile = new ZipFile(file);
				for (String noticeId : noticeIds) {
					//logger.trace("importTenderFolderDirectFromZip noticeId "+noticeId);		
					noticeId = new Long(noticeId)+"";
					List<Entry> entries = convertToEntries(zipFile, noticeId);
					updateTender(noticeId, entries);									  
				}
				zipFile.close();		
			} catch (IOException | ParserConfigurationException | SAXException | ParseException e) {
				e.printStackTrace();
			}
		}
	}
  /**
   * Import the enotification notice zip files in the database.
   * @param startNoticeId
   * @param endNoticeId
   */
	public void importHistory(int startNoticeId, int endNoticeId) {
		try {
			logger.trace("importHistory "+startNoticeId + " to " +endNoticeId);
			List<String> noticeIds = new ArrayList<>();

			noticeIds = new ArrayList<>();
			for (int i = startNoticeId; i < endNoticeId; i++) {
				if (checkAttachment(i + "")) {
					noticeIds.add(i + "");
				}
			}
			updateTenders(noticeIds, false);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (ZipException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {

		}
	}
  /**
   * Import the the attachments of the existing tenders.
   * @param startNoticeId
   * @param endNoticeId
   */
	public void importAttachmentHistory(int startNoticeId, int endNoticeId) {
		try {
			logger.trace("importAttachmentHistory "+startNoticeId + " to " +endNoticeId);
			List<String> noticeIds = new ArrayList<>();
			noticeIds = new ArrayList<>();
			for (int i = startNoticeId; i < endNoticeId; i++) {
				if (checkAttachment(i + "")) {
					noticeIds.add(i + "");
				}
			}
			updateTenders(noticeIds, true);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (ZipException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {

		}
	}
	
  /**
   * Update the AI part of the tender.
   * @param startNoticeId
   * @param endNoticeId
   */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateTendersAI(int startNoticeId, int endNoticeId, HashMap<String, List<String>> cpvDisciplineMap, Map<String,String> categoryCpvMap, KnowledgeBase knowledgeBase, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers) {
		try {
			logger.info("updateTenderAI "+startNoticeId + " to " +endNoticeId);
			List<String> noticeIds = new ArrayList<>();
			noticeIds = new ArrayList<>();
			for (int i = startNoticeId; i < endNoticeId; i++) {
				if (checkAttachment(i + "")) {
					noticeIds.add(i + "");
				}
			}
			updateTendersAI(noticeIds, cpvDisciplineMap, categoryCpvMap, knowledgeBase, dataStructure, classifiers);
		} finally {

		}
	}
	
	/**
	 * 1° Extract the notice ids from the zip
	 * 2° Download the notice from Enot if it is not already downloaded
	 * 
	 * @param file
	 * @param fileDate
	 * @param failedNoticeIds 
	 * @return the successfully downloaded noticeIds
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<String> importBijlagen(File file, Date fileDate, List<String> failedNoticeIds) {
		List<String> noticeIds = null;
		try {
			noticeIds = extractNoticeIds(file);
			noticeIds = importBijlagen(noticeIds,fileDate, failedNoticeIds);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return noticeIds;
	}
	
	/**
	 * Import or update the tender from the corresponding ZIP file.
	 * @param noticeIds
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void importUpdateTender(List<String> noticeIds) {
		try {
			updateTenders(noticeIds, false);			
		} catch (ClassNotFoundException | IOException | ParserConfigurationException | SAXException | TransformerException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Index the attachments of a Tender
	 * @param noticeIds
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void importUpdateTenderAttachments(List<String> noticeIds) {
		try {
			updateTenders(noticeIds, true);			
		} catch (ClassNotFoundException | IOException | ParserConfigurationException | SAXException | TransformerException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the CPV AI and Discipline AI of a tender.
	 * 
	 * @param noticeIds
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateTendersAI(List<String> noticeIds)  {
		KnowledgeBase knowledgeBase = knowledgeService.getKnowledgeBase();		
		HashMap<String, List<String>> cpvDisciplineMap;
		try {
			cpvDisciplineMap = knowledgeBase.getCpvDisciplineMap();
			FeatureList dataStructure = MLearningModels.initCpvClassifierFeatureStructure();			
			List<String> cpvList = knowledgeBase.getModelCpvList();
			Map<String,String> categoryCpvMap = knowledgeBase.getCategoryCpvMap();
			HashMap<String, ClassifierInterface> classifiers = MLearningModels.initCpvClassifierLibSvmMap(cpvList);	

			for (String noticeId : noticeIds) {
				trainingService.updateCpvDisciplineAI(new Integer(noticeId), cpvDisciplineMap, categoryCpvMap, knowledgeBase, dataStructure, classifiers);
			}
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Update the CPV AI and Discipline AI of a tender.
	 * 
	 * @param noticeIds
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateTendersAI(List<String> noticeIds, HashMap<String, List<String>> cpvDisciplineMap, Map<String,String> categoryCpvMap, KnowledgeBase knowledgeBase, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers)  {
			for (String noticeId : noticeIds) {
				trainingService.updateCpvDisciplineAI(new Integer(noticeId), cpvDisciplineMap, categoryCpvMap, knowledgeBase, dataStructure, classifiers);
			}
	}

	private Entry findFirstBESDocument(List<Entry> entries) {

		for (Entry entry : entries) {
			// System.out.println("PUB "+entry.publicationDate + " FORM "+entry.form +
			// " LG "+entry.language);
			if ("bes".equals(entry.form))
				return entry;
		}
		return null;
	}

	private class Entry implements Comparable<Entry> {
		Document document = null;
		String content = null;
		String form = null;
		String language = null;
		Date publicationDate = null;
		String name = null;
		String title = null;
		String government = null;

		@Override
		public int compareTo(Entry o) {
			if (publicationDate == null)
				return -1;
			if (o.publicationDate == null)
				return 1;
			return publicationDate.compareTo(o.publicationDate);
		}
	}
	
	private List<Entry> convertToEntries(ZipFile zipFile, String noticeId) throws ParserConfigurationException, SAXException, IOException, ParseException {
		List<Entry> entries = new ArrayList<>();
		AkteUtilities akteConverter = new AkteUtilities();
		for (Enumeration e = zipFile.entries(); e.hasMoreElements();) {
			ZipEntry zipEntry = (ZipEntry) e.nextElement();
			String name = zipEntry.getName();
			if (name.startsWith("notice_"+noticeId+"_") && name.endsWith(".xml")) {
				try {
					Document doc = XmlUtilities.getDocument(zipFile.getInputStream(zipEntry));
					Entry entry = new Entry();
					entry.name = name;
					if (entry.name.startsWith("notice_"))  entry.name =entry.name.substring(7);
					if (name.endsWith("-BES.xml") || name.endsWith("_BES.xml")) {
						String pubDateString = XmlUtilities.getNodeValue("/BSENDER/APPS/APP/PARAMS/PARAM[NAME=\"publication_date_enot\"]/VALUE/text()", doc);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						Date pubDate = sdf.parse(pubDateString);
						entry.document = doc;
						entry.publicationDate = pubDate;
						entry.form = "bes";
						entries.add(entry);
					} else {
						entry.document = doc;
						akteConverter.setDocument(doc);
						entry.publicationDate = akteConverter.getPublicationDate();

						entry.form = akteConverter.getForm(); // F2
						entry.language = akteConverter.getLanguage();
						entry.title = akteConverter.getOmschrijving(entry.language);
						
						if (entry.form == null) {
							logger.info("OOOOOOOOOOOOOOOOOOOOOOOOO");
							logger.info("FORM is null for " + name);
							logger.info("OOOOOOOOOOOOOOOOOOOOOOOOO");
						}
						if (entry.publicationDate != null)
							entries.add(entry);
					}
				} catch (org.xml.sax.SAXParseException se) {
					logger.error("DOCUMENT ERROR",se);
					logNoticeError(name + ";xml error");

				}
			} 
		}
		Collections.sort(entries);
		return entries;
	}

	private List<Entry> convertToEntries(ZipFile zipFile, boolean addAttachmentContent) throws ParserConfigurationException, SAXException, IOException, ParseException {
		List<Entry> entries = new ArrayList<>();
		AkteUtilities akteConverter = new AkteUtilities();
		for (Enumeration e = zipFile.entries(); e.hasMoreElements();) {
			ZipEntry zipEntry = (ZipEntry) e.nextElement();
			String name = zipEntry.getName();
			if (name.endsWith(".xml")) {
				try {
					Document doc = XmlUtilities.getDocument(zipFile.getInputStream(zipEntry));
					Entry entry = new Entry();
					entry.name = name;
					if (name.endsWith("-BES.xml") || name.endsWith("_BES.xml")) {
						String pubDateString = XmlUtilities.getNodeValue("/BSENDER/APPS/APP/PARAMS/PARAM[NAME=\"publication_date_enot\"]/VALUE/text()", doc);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						Date pubDate = sdf.parse(pubDateString);
						entry.document = doc;
						entry.publicationDate = pubDate;
						entry.form = "bes";
						entries.add(entry);
					} else {
						entry.document = doc;
						akteConverter.setDocument(doc);
						entry.publicationDate = akteConverter.getPublicationDate();

						entry.form = akteConverter.getForm(); // F2
						entry.language = akteConverter.getLanguage();

						if (entry.form == null) {
							logger.info("OOOOOOOOOOOOOOOOOOOOOOOOO");
							logger.info("FORM is null for " + name);
							logger.info("OOOOOOOOOOOOOOOOOOOOOOOOO");
						}
						if (entry.publicationDate != null)
							entries.add(entry);
					}
				} catch (org.xml.sax.SAXParseException se) {
					logger.error("DOCUMENT ERROR",se);
					logNoticeError(name + ";xml error");

				}
			} else {
				Entry entry = new Entry();
				try {
					String n = name.toLowerCase();
					if (addAttachmentContent && !n.endsWith("_en-en.pdf") && !n.endsWith("_de-de.pdf") && !n.endsWith("_fr-fr.pdf") && !n.endsWith("_nl-nl.pdf")
							&& (n.endsWith(".rtf") || n.endsWith(".doc") || n.endsWith(".pdf") || n.endsWith(".docx") || n.endsWith(".xls") || n.endsWith(".xlsx"))) {
						//System.out.println("before tikaParseToString");
						entry.content = tikaParseToString(zipFile.getInputStream(zipEntry));
						//System.out.println("after tikaParseToString");
					}
				} catch (Exception e1) {
					logger.info("set content to null");
					entry.content = null;
				}
				entry.name = name;
				entry.form = "att";
				entries.add(entry);
			}
		}
		Collections.sort(entries);
		return entries;
	}

	public String tikaParseToString(InputStream stream) throws IOException, SAXException, TikaException {
		Tika tika = new Tika();
		String content = "";
		
		try {
			content= tika.parseToString(stream);
		} catch (java.io.EOFException e) {
			content = "";
		} finally {
			stream.close();
		}
		return content;
	}

	private HashMap<String, Entry> findTenderEntries(List<Entry> entries) {
		HashMap<String, Entry> tenderEntries = new HashMap<>();
		for (Entry entry : entries) {
			if (!"14".equals(entry.form) && !"1".equals(entry.form) && !"3".equals(entry.form) && !"18".equals(entry.form) && !"4".equals(entry.form) && !"6".equals(entry.form) && !"bes".equals(entry.form)
					&& !"att".equals(entry.form)) {
				tenderEntries.put(entry.language, entry);
			}
		}
		return tenderEntries;
	}

	private HashMap<String, Entry> findAwardEntries(List<Entry> entries) {
		HashMap<String, Entry> tenderEntries = new HashMap<>();
		for (Entry entry : entries) {
			if ("3".equals(entry.form) || "18".equals(entry.form) || "6".equals(entry.form)) {
				tenderEntries.put(entry.language, entry);
			}
		}
		return tenderEntries;
	}

	private List<Entry> findAttachmentEntries(List<Entry> entries) {
		List<Entry> attachmentEntries = new ArrayList<>();
		for (Entry entry : entries) {
			if ("att".equals(entry.form)) {
				attachmentEntries.add(entry);
			}
		}
		return attachmentEntries;
	}

	private HashMap<String, Entry> findPriorInformationEntries(List<Entry> entries) {
		HashMap<String, Entry> tenderEntries = new HashMap<>();
		for (Entry entry : entries) {
			if ("1".equals(entry.form) || "4".equals(entry.form)) {
				tenderEntries.put(entry.language, entry);
			}
		}
		return tenderEntries;
	}

	private List<Entry> findRectificationEntries(List<Entry> entries) {
		List<Entry> rectificationEntries = new ArrayList<>();
		for (Entry entry : entries) {
			if ("14".equals(entry.form)) {
				rectificationEntries.add(entry);
			}
		}
		Collections.sort(rectificationEntries);
		return rectificationEntries;
	}

	private void logNoticeError(String noticeError) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(DATA_FOLDER + File.separator + "noticeError.txt", true)))) {
			out.println(noticeError);
		} catch (IOException e) {
			logger.error("logNoticeError problems",e);
		}
	}

	public void updateTenders(List<String> noticeIds, boolean attachmentOnly) throws ZipException, IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, ClassNotFoundException {
		boolean didone = false;
		try {
			for (String noticeId : noticeIds) {
				if (noticeId != null && !didone) {
					File folder = getDataFolder(DATA_FOLDER, noticeId);
					try {
						ZipFile zipFile = new ZipFile(new File(folder, "attachment.zip"));
						if (attachmentOnly)
							updateTenderAttachmentOnly(noticeId, zipFile);
						else
						  updateTender(noticeId, zipFile);
						zipFile.close();
					} catch (java.util.zip.ZipException | java.io.FileNotFoundException fe) {
						logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$");
						logger.info("$$$$$$$ ZIP EMPTY !!!!!!!!");
						logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$");
					}
				}

			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}

	public HashMap<String, String> findProperties(List<Entry> entries) throws ParserConfigurationException, SAXException, IOException, ParseException {
		HashMap<String, String> properties = new HashMap<String, String>();

		//List<Entry> entries = convertToEntries(zipFile);
		Entry firstBesEntry = findFirstBESDocument(entries);
		String noticeId = null;
		int endNotice = firstBesEntry.name.indexOf("_");
		if (endNotice == -1)
			firstBesEntry.name.indexOf("-");
		if (endNotice != -1)
			noticeId = firstBesEntry.name.substring(0, endNotice);
		String ereference = XmlUtilities.getNodeValue("/BSENDER/APPS/APP/PARAMS/PARAM[NAME=\"enot_reference\"]/VALUE/text()", firstBesEntry.document);
		if (ereference == null)
			ereference = "";
		else {
			int formPosition = ereference.lastIndexOf("F");
			if (formPosition > ereference.length()-7 && ereference.length() > 15)  ereference = ereference.substring(0,formPosition);
		}
		int year = new Integer(XmlUtilities.getNodeValue("/BSENDER/DATA_FILENAME/YEAR/text()", firstBesEntry.document)).intValue();
		String publicationDateString = XmlUtilities.getNodeValue("/BSENDER/APPS/APP/PARAMS/PARAM[NAME=\"publication_date_enot\"]/VALUE/text()", firstBesEntry.document);
		if (publicationDateString != null) {
		  Date publicationDate = new SimpleDateFormat("yyyyMMdd").parse(publicationDateString);
		  properties.put("publicationDate", publicationDate.getTime()+"");
		}
		String noticeIds = "";
		for (Entry entry : entries) {
			// System.out.println("PUB "+entry.publicationDate + " FORM "+entry.form +
			// " LG "+entry.language);
			if ("bes".equals(entry.form)) {
				endNotice = entry.name.indexOf("_");
				if (endNotice == -1)
					entry.name.indexOf("-");
				if (endNotice != -1 && noticeIds.indexOf(entry.name.substring(0, endNotice)) == -1)
					noticeIds = noticeIds + ":" + entry.name.substring(0, endNotice);
			}
		}
		noticeIds = noticeIds + ":";
		properties.put("year", year + "");
		properties.put("ereference", ereference);
		properties.put("noticeId", noticeId);
		properties.put("noticeIds", noticeIds);

		return properties;
	}

	protected void updateAwardEntries(HashMap<String, Entry> awardEntries, Tender tender, boolean updateTenderDetails) throws ParserConfigurationException {
		AkteUtilities akteConverter = new AkteUtilities();
		StringBuilder texts = new StringBuilder();
		for (String language : awardEntries.keySet()) {
			Entry entry = awardEntries.get(language);
			akteConverter.setDocument(entry.document);
			Award award = tender.getAward();
			if (award == null) {
				award = new Award();
				entityManager.persist(award);
				tender.setAward(award);
			}
			award.setForm(entry.form);
			award.setPublication(entry.publicationDate);
			award.setAttachmentName(entry.language, entry.name);

			List<AwardItem> awardItems = null;
			awardItems = award.getAwardItems();
			if (awardItems != null && awardItems.size() > 0)
				for (AwardItem item : awardItems) {
					entityManager.remove(item);
				}
			awardItems = akteConverter.getAwardItems(knowledgeService.getKnowledgeBase());

			if (awardItems != null) {
				updateContractors(awardItems);
				for (AwardItem item : awardItems) {
					entityManager.persist(item);
					item.setAward(award);
				}
			}
			award.setAwardItems(awardItems);

			NoticeText noticeText = award.getNoticeText(language);
			if (noticeText == null) {
				noticeText = new NoticeText();
				noticeText.setLanguage(language);
				entityManager.persist(noticeText);
				noticeText.setAward(award);
				award.getAwardTexts().add(noticeText);
			}
			noticeText.setForm(entry.form);
			noticeText.setPublication(entry.publicationDate);
			if (entry.publicationDate != null && (tender.getChangeDate() == null || tender.getChangeDate().before(entry.publicationDate)))
				tender.setChangeDate(entry.publicationDate);

			try {
				noticeText.setText(XmlUtilities.xmlToString(entry.document));
				texts.append(noticeText.getText());
			} catch (TransformerException e) {
				e.printStackTrace();
			}
			tender.addBda(akteConverter.getBDA(entry.language));
			if (updateTenderDetails) {				 
				updateTenderDetails(akteConverter, tender, entry.language);
				tender.setPublication(entry.publicationDate);
				tender.setForm(entry.form);
			}
		}
		if (updateTenderDetails && texts.length() > 1) {	
		  String tekst = texts.toString();
		  updateTenderDetail(tender, tekst);	
		}	
	}

	private void updateContractors(List<AwardItem> awardItems) {
		for (AwardItem awardItem : awardItems) {
			Contractor contractor = awardItem.getContractor();
			Contractor dbContractor = searchService.findContractorByNameLocation(contractor.getName(), contractor.getLocation());
			if (dbContractor != null) {
				awardItem.setContractor(dbContractor);
				if (contractor.getType() != null)
					dbContractor.setType(contractor.getType());
				if (contractor.getPhones() != null && contractor.getPhones().length() > 0)
					dbContractor.setPhones(contractor.getPhones());
				if (contractor.getFaxes() != null && contractor.getFaxes().length() > 0)
					dbContractor.setFaxes(contractor.getFaxes());
				if (contractor.getEmails() != null && contractor.getEmails().length() > 0) {
					String dbEmails = dbContractor.getEmails();
					if (dbEmails != null && dbEmails.length() > 0) {
						String[] emails = contractor.getEmails().split(";");
						for (String e : emails) {
							if (dbEmails.indexOf(e) == -1)
								dbEmails = dbEmails + ";" + e;
						}
						dbContractor.setEmails(dbEmails);
					} else {
						dbContractor.setEmails(contractor.getEmails());
					}
				}

			} else {
				entityManager.persist(contractor);
			}
		}
	}

	protected void updatePreInformationEntries(HashMap<String, Entry> preInformationEntries, Tender tender, boolean updateTenderDetails) throws ParserConfigurationException {
		AkteUtilities akteConverter = new AkteUtilities();
		StringBuilder texts = new StringBuilder();
		for (String language : preInformationEntries.keySet()) {
			Entry entry = preInformationEntries.get(language);
			PreInformation preInformation = tender.getPreInformation();
			if (preInformation == null) {
				preInformation = new PreInformation();
				entityManager.persist(preInformation);
				tender.setPreInformation(preInformation);
			}
			preInformation.setForm(entry.form);
			preInformation.setPublication(entry.publicationDate);
			preInformation.setAttachmentName(entry.language, entry.name);
			NoticeText noticeText = preInformation.getNoticeText(language);
			if (noticeText == null) {
				noticeText = new NoticeText();
				noticeText.setLanguage(language);
				entityManager.persist(noticeText);
				noticeText.setPreInformation(preInformation);
				preInformation.getPreInformationTexts().add(noticeText);
			}
			
			noticeText.setForm(entry.form);
			noticeText.setPublication(entry.publicationDate);
			if (entry.publicationDate != null && (tender.getChangeDate() == null || tender.getChangeDate().before(entry.publicationDate)))
				tender.setChangeDate(entry.publicationDate);

			try {
				noticeText.setText(XmlUtilities.xmlToString(entry.document));
				texts.append(noticeText.getText());
			} catch (TransformerException e) {
				e.printStackTrace();
			}
			akteConverter.setDocument(entry.document);
			tender.addBda(akteConverter.getBDA(entry.language));
			if (updateTenderDetails) {				 				
				updateTenderDetails(akteConverter, tender, entry.language);
				tender.setPublication(entry.publicationDate);
				tender.setForm(entry.form);
			}
		}
		if (updateTenderDetails && texts.length() > 1) {	
		  String tekst = texts.toString();
		  updateTenderDetail(tender, tekst);	
		}		
	}

	protected void updateRectificationEntries(List<Entry> rectificationEntries, Tender tender, boolean updateTenderDetails) throws ParserConfigurationException {
		Date publicationDate = null;
		Rectification currentRectification = null;
		AkteUtilities akteConverter = new AkteUtilities();
		Set<Rectification> rectifications = null;
		rectifications = tender.getRectifications();
		if (rectifications != null && rectifications.size() > 0)
			for (Rectification rectification : rectifications) {
				entityManager.remove(rectification);
			}
		rectifications = new HashSet<Rectification>();
		StringBuilder texts = new StringBuilder();
		for (Entry entry : rectificationEntries) {
			if (publicationDate == null || !publicationDate.equals(entry.publicationDate)) {
				currentRectification = new Rectification();
				entityManager.persist(currentRectification);
				currentRectification.setTender(tender);				
				rectifications.add(currentRectification);
			}
			currentRectification.setAttachmentName(entry.language, entry.name);
			publicationDate = entry.publicationDate;
			NoticeText noticeText = new NoticeText();
			noticeText.setLanguage(entry.language);
			entityManager.persist(noticeText);
			noticeText.setRectification(currentRectification);
			currentRectification.getRectificationTexts().add(noticeText);
			noticeText.setForm(entry.form);
			noticeText.setPublication(entry.publicationDate);
			if (entry.publicationDate != null && (tender.getChangeDate() == null || tender.getChangeDate().before(entry.publicationDate)))
				tender.setChangeDate(entry.publicationDate);
			
			try {
				noticeText.setText(XmlUtilities.xmlToString(entry.document));
				texts.append(noticeText.getText());
			} catch (TransformerException e) {
				e.printStackTrace();
			}
			akteConverter.setDocument(entry.document);
			currentRectification.setType(akteConverter.getF14Type());
			currentRectification.setPublicationDate(entry.publicationDate);
      tender.addBda(akteConverter.getBDA(entry.language));
			if (updateTenderDetails) {				 
				updateTenderDetails(akteConverter, tender, entry.language);
				tender.setPublication(entry.publicationDate);
				tender.setForm(entry.form);
			}
			
			//logger.trace("updateRectificationEntries " + entry.form + " " + entry.publicationDate + " ");
		}
		if (updateTenderDetails && texts.length() > 1) {	
		  String tekst = texts.toString();
		  updateTenderDetail(tender, tekst);	
		}
		tender.setRectifications(rectifications);
	}

	private void updateTenderDetail(Tender tender, String tekst) {
		tender.setCpv(AkteUtilities.constructCpvIndex(tekst, true));
		tender.setCpvNoParent(AkteUtilities.constructCpvIndex(tekst, false));
		tender.setNuts(AkteUtilities.constructNutsIndex(tekst, true));
		tender.setNutsNoParent(AkteUtilities.constructNutsIndex(tekst, false));
	}

	protected void updateAttachmentEntries(List<Entry> attachmentEntries, Tender tender, boolean updateText) throws ParserConfigurationException {
		Attachment currentAttachment = null;
		Set<Attachment> attachments = null;
		attachments = tender.getAttachments();
		if (attachments != null && attachments.size() > 0)
			for (Attachment attachment : attachments) {
				entityManager.remove(attachment);
			}
		attachments = new HashSet<Attachment>();
		for (Entry entry : attachmentEntries) {
			currentAttachment = new Attachment();
			entityManager.persist(currentAttachment);
			currentAttachment.setTender(tender);
			attachments.add(currentAttachment);
			currentAttachment.setName(entry.name);
			if (entry.content != null && updateText) {
				String nameUpgrade = entry.name.replace("_", " ");
				nameUpgrade = nameUpgrade.replace(".", " ");
				String text = nameUpgrade + " " + entry.content;
				Set<String> features = knowledgeService.getKnowledgeBase().extractFeatures(text);
				StringBuilder fString = new StringBuilder();
				for (String f : features)
					fString.append(":" + f);
				currentAttachment.setFeatures(nameUpgrade + ":" + fString.toString());
				currentAttachment.setText(text);
			}
			currentAttachment.setCategoryAI(tender.getCategoryAI());
			currentAttachment.setDisciplinesAI(tender.getDisciplinesCpvAI());
			currentAttachment.setLocation(tender.getLocation());
			currentAttachment.setGovernmentType(tender.getGovernmentType());
			currentAttachment.setCpv(tender.getCpv());

			if (entry.name.lastIndexOf(".") != -1)
				currentAttachment.setFileType(entry.name.substring(entry.name.lastIndexOf(".") + 1));
		}
		tender.setAttachments(attachments);
	}
	
	public void updateTenderAttachmentOnly(String noticeId, ZipFile zipFile) throws ParserConfigurationException, SAXException, IOException, ParseException {
		Tender tender = findTenderByNoticeId(noticeId);
		if (tender != null) {
			List<Entry> entries = convertToEntries(zipFile, true);
			List<Entry> attachmentEntries = findAttachmentEntries(entries);
			updateAttachmentEntries(attachmentEntries, tender, true);				
		}
	}
	
	public void updateTender(String noticeId, ZipFile zipFile) throws ParserConfigurationException, SAXException, IOException, ParseException {
		boolean updateAttachmentText = false;
		List<Entry> entries = convertToEntries(zipFile, updateAttachmentText);
		HashMap<String, String> properties = findProperties(entries);
		if (!noticeId.equals(properties.get("noticeId")))
			logger.error("*************************** ERROR " + properties.get("noticeId"));
		else 
		  updateTender(entries, properties, updateAttachmentText);
	}
	
	private boolean onlyDependantEntries(List<Entry> entries) {
		for (Entry entry : entries)
			if (!entry.form.equals("18") && !entry.form.equals("6") && !entry.form.equals("3") && !entry.form.equals("14") && !entry.form.equals("bes")) return false;
		return true;
	}
	
	private String extractFormTitle(List<Entry> entries) {
		String form = null;
		for (Entry entry : entries)
			if (!entry.form.equals("bes")) form = entry.form + " " +entry.title;
		return form;
	}
	
	public void updateTender(String noticeId, List<Entry> entries) throws ParserConfigurationException, SAXException, IOException, ParseException {
		boolean updateAttachmentText = false;
		boolean validEntry = true;
		logger.info("updateTender " + noticeId);
		HashMap<String, String> properties = findProperties(entries);
		String noticeIds = properties.get("noticeIds");
		if (!checkAttachment(noticeId)) {
			logger.info("updateTender " + noticeId + " "+ properties.get("ereference"));
			String form = extractFormTitle(entries);
			logger.info("updateTender onlyDependantEntries " + noticeId);
			Tender rootTender = findTenderByEreference(properties.get("ereference"));			
			if (rootTender != null) {
				if (noticeIds.indexOf(rootTender.getNoticeId()) == -1) {
					if (checkAttachment(rootTender.getNoticeId())) {
						logger.info(rootTender.getNoticeId() + " " +noticeId + " already in DB via ZIP attachment");
						validEntry = false;
					}
					noticeIds = noticeIds+rootTender.getNoticeId()+":";
					properties.put("noticeIds", noticeIds);
				}
			}
			if (onlyDependantEntries(entries)) {
				if (rootTender != null) {
					if (form.startsWith("3 "))
						logger.info(noticeId + " companion tender found for orphan "+properties.get("ereference")+" "+rootTender.getBda()+ " "+form+" !!!!!");
				} else {
					validEntry = false;
					if (form.startsWith("14 "))
						logger.info(noticeId + "companion tender not found for orphan "+properties.get("ereference")+ " "+form);
				}
			}

			if (!noticeId.equals(properties.get("noticeId")))
				logger.error("*************************** ERROR " + properties.get("noticeId"));
			if (validEntry) 
				updateTender(entries, properties, updateAttachmentText);
		} else {
			logger.info(noticeId + " already in DB via ZIP attachment");
		}
	}	

	private void updateTender(List<Entry> entries, HashMap<String, String> properties, boolean updateAttachmentText)
			throws ParserConfigurationException, SAXException, IOException, ParseException {		
		KnowledgeBase knowledgeBase = knowledgeService.getKnowledgeBase();
		AkteUtilities akteConverter = new AkteUtilities();
		Entry firstBesEntry = findFirstBESDocument(entries);
		//String ereference = AkteTekstParser.getNodeValue("/BSENDER/APPS/APP/PARAMS/PARAM[NAME=\"enot_reference\"]/VALUE/text()", firstBesEntry.document);
		String ereference = properties.get("ereference");
		if (ereference == null)
			ereference = "";
    
    String noticeId = properties.get("noticeId");
		Tender tender = findTenderByNoticeIds(properties.get("noticeIds"));
		if (tender == null)
			tender = createTender(noticeId, properties.get("noticeIds"));
		else
			tender.setNoticeIds(properties.get("noticeIds"));

		tender.setEreference(ereference);

		HashMap<String, Entry> tenderEntries = findTenderEntries(entries);
		StringBuilder texts = new StringBuilder();
		Date publication = null;
		Date openingDate = null;

		for (String language : tenderEntries.keySet()) {
			Entry entry = tenderEntries.get(language);
			Information information = tender.getInformation();
			if (information == null) {
				information = new Information();
				entityManager.persist(information);
				tender.setInformation(information);
			}
			information.setForm(entry.form);
			information.setPublication(entry.publicationDate);
			information.setAttachmentName(language, entry.name);
			NoticeText noticeText = information.getNoticeText(language);
			if (noticeText == null) {
				noticeText = new NoticeText();
				noticeText.setLanguage(language);
				entityManager.persist(noticeText);
				noticeText.setInformation(information);
				information.getInformationTexts().add(noticeText);
			}
			tender.setForm(entry.form);
			noticeText.setForm(entry.form);
			noticeText.setPublication(entry.publicationDate);
			try {
				String tekst = XmlUtilities.xmlToString(entry.document);
				noticeText.setText(tekst);
				akteConverter.setDocument(entry.document);
				updateTenderDetails(akteConverter, tender, language);
				texts.append(tekst);
				String cat = akteConverter.getNormalizedCategoryAndClass();
				tender.setCategoryAI(cat);
				Date p = akteConverter.getPublicationDate(); // we want the first F2 date, not the 2nd or 3rd.
				if (publication == null || publication.after(p)) {
					publication = p;					
				}				
				openingDate = akteConverter.getOpeningDate();
			} catch (TransformerException e) {
				e.printStackTrace();
			}
		}
		if (tenderEntries.size() > 0) {
		  String tekst = texts.toString();
		  updateTenderDetail(tender, tekst);
		  tender.setPublication(publication);
		  if (tenderEntries.size() > 0) {
			  tender.setLocation(akteConverter.constructLocation(tender.getNutsNoParent(), knowledgeBase.getCityList()));			
		  }
		  tender.setChangeDate(publication);
		  tender.setOpeningDate(openingDate);
		}

		List<Entry> rectificationEntries = findRectificationEntries(entries);
		updateRectificationEntries(rectificationEntries, tender, tenderEntries.size() == 0 && tender.getInformation() == null) ;

		HashMap<String, Entry> preInformationEntries = findPriorInformationEntries(entries);
		updatePreInformationEntries(preInformationEntries, tender, tenderEntries.size() == 0 && tender.getInformation() == null);
		
		HashMap<String, Entry> awardEntries = findAwardEntries(entries);
		updateAwardEntries(awardEntries, tender, tenderEntries.size() == 0 && tender.getInformation() == null);

		List<Entry> attachmentEntries = findAttachmentEntries(entries);
		updateAttachmentEntries(attachmentEntries, tender, updateAttachmentText);	
		
		//updateTenderAI(tender, filter, dataStructure, classifiers, cpvExceptions);
	}

	private void updateTenderDetails(AkteUtilities akteConverter, Tender tender, String language) {
		updateGovernment(tender, akteConverter);
		tender.setTitle(language, akteConverter.getOmschrijving(language));
		tender.setEssence(language, akteConverter.getEssentie(language));
		tender.setLot(language, akteConverter.getLot(language));
		tender.addBda(akteConverter.getBDA(language));
		tender.setType(akteConverter.getSoortNormalized(""));
	}

	private void updateGovernment(Tender tender, AkteUtilities akteConverter) {
		Government government = akteConverter.getGovernment(knowledgeService.getKnowledgeBase());
		Government dbGovernment = searchService.findGovernmentByNameLocation(government.getName(), government.getLocation());
		if (dbGovernment != null) {
			tender.setGovernment(dbGovernment);
			if (government.getActivity() != null && government.getActivity().length() > 0) {
				if (dbGovernment.getActivity() != null && dbGovernment.getActivity().indexOf(government.getActivity()) == -1) {
					dbGovernment.setActivity(dbGovernment.getActivity() + " " +government.getActivity());
				} else
					dbGovernment.setActivity(government.getActivity());
			}
			if (government.getType() != null) {
				dbGovernment.setType(government.getType());
				tender.setGovernmentType(government.getType());
			}
			Collection<Contact> dbContacts = dbGovernment.getContacts();
			Contact dbContact = null;
			Contact contact = government.getContacts().iterator().next();
			for (Contact c : dbContacts) {
				if (c.getName().equals(contact.getName()) && c.getAttention().equals(contact.getAttention()))
					dbContact = c;
			}
			if (dbContact == null) {
				contact.setGovernment(dbGovernment);
				dbContact = contact;
				entityManager.persist(contact);
				dbGovernment.getContacts().add(contact);
			} else {
				if (contact.getPhones() != null && contact.getPhones().length() > 0)
					dbContact.setPhones(contact.getPhones());
				if (contact.getFaxes() != null && contact.getFaxes().length() > 0)
					dbContact.setFaxes(contact.getFaxes());
				if (contact.getEmails() != null && contact.getEmails().length() > 0) {
					String dbEmails = dbContact.getEmails();
					if (dbEmails != null && dbEmails.length() > 0) {
						String[] emails = contact.getEmails().split(";");
						for (String e : emails) {
							if (dbEmails.indexOf(e) == -1)
								dbEmails = dbEmails + ";" + e;
						}
						dbContact.setEmails(dbEmails);
					} else {
						dbContact.setEmails(contact.getEmails());
					}
				}
			}

		} else {
			entityManager.persist(government);
			tender.setGovernment(government);
		}
	}

	/**
	 * Extract noticeIds from an Enot Zip file.
	 * 
	 * @param bestand
	 * @return
	 * @throws ZipException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public List<String> extractNoticeIds(File bestand) throws ZipException, IOException, ParserConfigurationException, SAXException {
		ZipFile zipFile = new ZipFile(bestand);
		List<String> noticeIds = new ArrayList<>();
		for (Enumeration e = zipFile.entries(); e.hasMoreElements();) {
			ZipEntry zipEntry = (ZipEntry) e.nextElement();
			String name = zipEntry.getName();
			if (name.endsWith("-BES.xml") || name.endsWith("_BES.xml")) {
				String id = name.substring(7, 13).trim();
				if (id.indexOf("_") != -1) id = id.substring(0,id.indexOf("_"));
				while (id.length() < 6) id = "0"+id;
				noticeIds.add(id);
			}
		}
		zipFile.close();
		return noticeIds;
	}

	private String getNoticeFromNoticeId(String noticeId) {
		if (noticeId.indexOf("_") == -1)
			return null;
		String[] v = noticeId.split("_");
		return v[1];
	}

	private String getYearFromNoticeId(String noticeId) {
		if (noticeId.indexOf("_") == -1)
			return null;
		String[] v = noticeId.split("_");
		return v[0];
	}

	private boolean checkAttachment(String notice) {

		File tmpFolder = getDataFolder(DATA_FOLDER, notice);
		File file = new File(tmpFolder, "attachment.zip");
		if (file.exists()) {
			try {
				ZipFile zipFile = new ZipFile(file);
				zipFile.close();
				return true;
			} catch (ZipException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return false;
	}

	/**
	 * Download notices from Enot
	 * 
	 * @param noticeIds
	 * @param fileDate
	 * @param failedNoticeIds
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws ParseException
	 */
	public List<String> importBijlagen(List<String> noticeIds, Date fileDate, List<String> failedNoticeIds) throws ParserConfigurationException, SAXException, ParseException {
		boolean didone = false;
		List<String> noticeIdsUpgrade = new ArrayList<String>();
		try {
			for (String noticeId : noticeIds) {
				if (noticeId != null && !didone) {
					File checkFolder = getDataFolder(DATA_FOLDER, noticeId);
					File checkFile = new File(checkFolder, "attachment.zip");
					File file = new File(DATA_FOLDER, "attachment.zip");
					Date downloadedFile = null;
					if (checkFile.exists()) 
						downloadedFile = new Date(checkFile.lastModified());
					else {
					  Tender tender = findTenderByNoticeId(noticeId);
					  if (tender != null) {
							checkFolder = getDataFolder(DATA_FOLDER, tender.getNoticeId());
							checkFile = new File(checkFolder, "attachment.zip");
							if (checkFile.exists()) 
								downloadedFile = new Date(checkFile.lastModified());
					  }
					}
					if (downloadedFile != null && downloadedFile.after(fileDate)) {
						logger.info("already downloaded " + noticeId + " on " + downloadedFile);
						try {
							ZipFile zipFile = new ZipFile(checkFile);
							List<Entry> entries = convertToEntries(zipFile, false);
							noticeId = findProperties(entries).get("noticeId");
							zipFile.close();
							noticeIdsUpgrade.add(noticeId);
						} catch (ZipException ze) {
							logger.info("Zip not valid" + noticeId);
							logNoticeError(noticeId + ";Zip not valid");
							failedNoticeIds.add(noticeId + ";Zip not valid");
						}
					} else {
						logger.info("download " + noticeId + " " + downloadedFile);
						URL website = new URL("https://enot.publicprocurement.be/enot-war/downloadAllNoticeDocuments.do?noticeId=" + noticeId + "&packXml=true");
						try {
							try {
							  FileUtils.copyURLToFile(website, file, 60000, 1000 * 60 * 5);  //java.io.InterruptedIOException
							} catch (java.io.InterruptedIOException interruptedIOException) {
								try {
									logger.info("Retry " + noticeId);
								  FileUtils.copyURLToFile(website, file, 60000, 1000 * 60 * 60);  //java.io.InterruptedIOException
								} catch (java.io.InterruptedIOException interruptedIOException2) {
									logger.error("Could not download " + noticeId + " "+interruptedIOException,interruptedIOException2);
								  return null;
								}
							}
							//ReadableByteChannel rbc = Channels.newChannel(website.openStream());
							//FileOutputStream fos = new FileOutputStream(file);
							//fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
							//fos.close();
							try {
								ZipFile zipFile = new ZipFile(file);
								List<Entry> entries = convertToEntries(zipFile, false);
								noticeId = findProperties(entries).get("noticeId");
								zipFile.close();
								File tmpFolder = getDataFolder(DATA_FOLDER, noticeId);
								if (!tmpFolder.exists())
									tmpFolder.mkdirs();
								file.renameTo(new File(tmpFolder, "attachment.zip"));
								noticeIdsUpgrade.add(noticeId);
							} catch (ZipException ze) {
								logger.info("Zip not valid" + noticeId);
								logNoticeError(noticeId + ";Zip not valid");
								failedNoticeIds.add(noticeId + ";Zip not valid");
								file.delete();
							}
						} catch (IOException ioe) {
							logger.error("Could not download " + noticeId);
							logNoticeError(noticeId + ";Could not download");
							failedNoticeIds.add(noticeId + ";Could not download");
						}
					}
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return noticeIdsUpgrade;
	}

	public static File getDataFolder(String rootFolder, String id) {
		String sub1 = "00";
		String sub2 = "00";
		String sub3 = "00";
		String sub4 = "00";

		if (id.length() > 1)
			sub1 = id.substring(0, 2);
		if (id.length() > 3)
			sub2 = id.substring(2, 4);
		if (id.length() > 5)
			sub3 = id.substring(4, 6);
		if (id.length() > 7)
			sub4 = id.substring(6, 8);
		return new File(rootFolder + File.separator + sub1 + File.separator + sub2 + File.separator + sub3 + File.separator + sub4 + File.separator + "id" + id);
	}
	
	public Tender findTenderByEreference(String ereference) {
		Tender tender = null;
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findByEreference", Tender.class);
		q.setParameter("ereference", ereference );
		try {
			tender = q.getSingleResult();
			return tender;
		} catch (NoResultException e) {
			return null;
		} catch (javax.persistence.NonUniqueResultException e) {
			System.out.println("findTenderByEreference NOT UNIQUE " +ereference);
			throw e;
		}
	}

	public Tender findTenderByNoticeIds(String noticeIds) {
		String[] noticeIdArray = noticeIds.split(":");
		Tender tender = null;
		for (String noticeId : noticeIdArray) {
			tender = findTenderByNoticeId(noticeId);
			if (tender != null) break;
		}
		return tender;
	}
	
	public Tender findTenderByNoticeId(String noticeId) {
		Tender tender = null;
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findByNoticeIds", Tender.class);
		q.setParameter("noticeId", "%:" + noticeId + ":%");
		try {
			tender = q.getSingleResult();
			return tender;
		} catch (NoResultException e) {
			return null;
		} catch (javax.persistence.NonUniqueResultException e) {
			logger.error("findTenderByNoticeId NOT UNIQUE " +noticeId);
			throw e;
		}	
	}

	public List<Tender> findTenders() {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findAll", Tender.class);
		q.setMaxResults(100);
		try {
			return q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Tender createTender(String noticeId, String noticeIds) {
		Tender tender = new Tender();
		tender.setNoticeId(noticeId);
		tender.setNoticeIds(noticeIds);
		entityManager.persist(tender);
		return tender;
	}

	public Tender findTenderById(Long id) {
		return entityManager.find(Tender.class, id);
	}
	
	public void updateContractorIndex() {
		TypedQuery<Contractor> q = entityManager.createNamedQuery("Contractor.findAll", Contractor.class);
		try {
			for (Contractor contractor : q.getResultList()) {
				contractor.updateReverseIndex();
			}
		} catch (NoResultException e) {
			
		}
	}
	public void updateGovernmentIndex() {
		TypedQuery<Government> q = entityManager.createNamedQuery("Government.findAll", Government.class);
		try {
			for (Government government : q.getResultList()) {
				government.updateReverseIndex();
			}
		} catch (NoResultException e) {
			
		}
	}
	public void updateTenderIndex() {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findAll", Tender.class);
		try {
			for (Tender tender : q.getResultList()) {
				tender.updateReverseIndex();
			}
		} catch (NoResultException e) {
			
		}
	}

}