/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.sygel.tender.domain.Filter;
import com.sygel.tender.domain.FilterTender;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.MyTenderFilter;
import com.sygel.tender.domain.PushTenderFilter;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.TenderFilter;
import com.sygel.tender.domain.User;
import com.sygel.tender.domain.util.Util;
import com.sygel.tender.ml.util.AkteUtilities;
import com.sygel.tender.ui.util.SessionUtil;

/**
 * User service
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Stateless
@Local
@DeclareRoles({User.ROLE_ADMIN, User.ROLE_USER})
public class UserService {
	@PersistenceContext(unitName = "tender")
	private EntityManager entityManager;
	
	private final Logger logger = Logger.getLogger(UserService.class);
	
	@Inject
	KnowledgeService knowledgeService;

	//@RolesAllowed({User.ROLE_ADMIN})
	public User createUser(String email, String password, String name, String firstname) {
		User user = new User();
		
		user.setEmail(email);
		user.storePassword(password);
		user.setName(name);
		user.setFirstname(firstname);
		entityManager.persist(user);
		
		return user;
	}
	@RolesAllowed({User.ROLE_ADMIN})
	public List<User> findAllUsers() {
		TypedQuery<User> q = entityManager.createNamedQuery("User.findAll", User.class);
		//q.setMaxResults(100);
		try {
			return q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	@RolesAllowed({User.ROLE_ADMIN})
	public List<User> findUsers(String searchString) {
		List<User> users = null;
		TypedQuery<User> q = entityManager.createNamedQuery("User.findByUserFilter", User.class);
		q.setParameter("searchString", upgradeUserSearchString(searchString));
		q.setMaxResults(1000);
		try {
			users = q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return users;
	}
	
	protected String upgradeUserSearchString(String searchString) {	
		StringBuilder searchBuild = new StringBuilder();
		if (searchString != null && searchString.length()  > 0) {
		  searchString = searchString.replace("\u00a0"," ");
		  searchString = searchString.replace("@",Util.AT_ESCAPE); // eg. in emails
		  searchString = searchString.replaceAll("(?<!\\s)\\.(?!\\s)",Util.DOT_ESCAPE); // . not connected to a space, eg. in emails
		  searchString = searchString.replaceAll("(?<!\\s)-(?!\\s)",Util.HYPHEN_ESCAPE);// - not connected to a space, eg. in emails
		  String[] words = Util.constructWords(searchString);		  
		  for (String word : words) {
		  	logger.trace("upgradeContractorSearchString word "+word);
		  	if (word.length() < 4 && word.length() > 0 && '*' != word.charAt(0)) {
		  		searchBuild.append(Util.REVERSE_PREFIX+Util.reverseString(word)+" ");
		  	} else if (word.matches("[0-9]{4}")) {
			  	searchBuild.append(Util.LOCATION_PREFIX + word + " ");
			  	searchBuild.append(word + " ");
		  	} else if (word.length()>1 && '*' == word.charAt(0))  {
			  	searchBuild.append(Util.REVERSE_PREFIX + Util.reverseString(word.substring(1)) + "* ");
			  } else if (!"*".equals(word) && !"+".equals(word)){
			  	searchBuild.append(word + " ");
			  }
		  }
	  }
		logger.trace("upgradeUserSearchString "+searchBuild.toString());
		return searchBuild.toString();
	}

	@RolesAllowed({User.ROLE_USER})
	public PushTenderFilter findPushTenderFilter(Long userId, String filterName) {
		PushTenderFilter filter = null;
		TypedQuery<PushTenderFilter> q = entityManager
				.createNamedQuery("PushTenderFilter.findByUserAndFiltername", PushTenderFilter.class);	 	
		EntityGraph<?> eg = entityManager.getEntityGraph("PushTenderFilter.filterTenders");
	  q.setHint("javax.persistence.fetchgraph", eg);	 	
		q.setParameter("userId", userId );
		q.setParameter("filtername", filterName );
		try {
			filter = q.getSingleResult();
			for(FilterTender filterTender : filter.getFilterTenders()) {
				SearchService.upgradeTender(knowledgeService, filterTender.getTender());
			}
		} catch (NoResultException e) {
		}
		return filter;
	}

	@RolesAllowed({User.ROLE_USER})
	public MyTenderFilter findMyTenderFilter(Long userId, String filterName) {
		MyTenderFilter filter = null;
		TypedQuery<MyTenderFilter> q = entityManager
				.createNamedQuery("MyTenderFilter.findByUserAndFiltername", MyTenderFilter.class);	 	
		EntityGraph<?> eg = entityManager.getEntityGraph("MyTenderFilter.filterTenders");
	  q.setHint("javax.persistence.fetchgraph", eg);	 	
		q.setParameter("userId", userId );
		q.setParameter("filtername", filterName );
		try {
			filter = q.getSingleResult();
			for(FilterTender filterTender : filter.getFilterTenders()) {
				SearchService.upgradeTender(knowledgeService, filterTender.getTender());
			}
		} catch (NoResultException e) {
		}
		return filter;
	}

	@RolesAllowed({User.ROLE_USER})
	public User findUser(Long userId) {
		return entityManager.find(User.class, userId);
	}
	
	@RolesAllowed({User.ROLE_ADMIN})
	public boolean removeUser(Long userId) {
		User user = findUser(userId);
		if (user != null && !SessionUtil.getUser().getId().equals(user.getId())) {
		  entityManager.remove(user);
		  return true;
		} else
			return false;
	}
	
	@RolesAllowed({User.ROLE_USER})
	public User findUser(String email, boolean addFilters) {
		User user = null;
		
		TypedQuery<User> q = entityManager.createNamedQuery("User.findByEmail", User.class);
	 	if (addFilters) {
		  EntityGraph<?> eg = entityManager.getEntityGraph("User.filters");
			q.setHint("javax.persistence.fetchgraph", eg);
	 	}
		q.setParameter("email", email );
		try {
			user = q.getSingleResult();
		} catch (NoResultException e) {
		}
		return user;
	}
	@PermitAll
	public User findUser(String email) {
		User user = null;
		System.out.println("UserService::findUser "+email);
		TypedQuery<User> q = entityManager.createNamedQuery("User.findByEmail", User.class);
		q.setParameter("email", email );
		try {
			user = q.getSingleResult();
		} catch (NoResultException e) {
		}
		return user;
	}
	
	public boolean checkUser(String email, String password) {
		User user = findUser(email, false);
		if (user != null)
		  return user.checkPassword(password);
		return false;
	}
	
	@RolesAllowed({User.ROLE_USER})
	public void addUserFilter(String userEmail, Filter filter) {
		entityManager.persist(filter);
		User user = findUser(userEmail, true);
		filter.setUser(user);
		user.getFilters().add(filter);
	}
	
	@RolesAllowed({User.ROLE_USER})
	public void saveFilter(Filter filter) {
		entityManager.merge(filter);
	}
	
	@RolesAllowed({User.ROLE_USER})
	public void deleteFilter(Filter filter) {
		filter = entityManager.merge(filter);
		User user = filter.getUser();
		user.getFilters().remove(filter);
		filter.setUser(null);
		entityManager.remove(filter);
	}
	
	@RolesAllowed({User.ROLE_USER})
	public void saveUser(User user) {
		if (user.getNewPassword() != null && user.getNewPassword().length() > 5) {
			user.storePassword(user.getNewPassword());;
		}
		if (user.getId() == null) {
			entityManager.persist(user);
			Filter filter = new MyTenderFilter(MyTenderFilter.NAME, "");
			entityManager.persist(filter);
			filter.setUser(user);
			user.getFilters().add(filter);
		} else {
		  entityManager.merge(user);
		}
	}
	
	@RolesAllowed({User.ROLE_USER})
	public boolean isMyTenderFilter(Long userId, Tender tender) {
		MyTenderFilter myTenderFilter = findMyTenderFilter(userId, MyTenderFilter.NAME);
		return isMyTenderFilter(myTenderFilter, tender);
	}
	
	@RolesAllowed({User.ROLE_USER})
	protected boolean isMyTenderFilter(MyTenderFilter myTenderFilter, Tender tender) {
		boolean isMyTenderFilter = false;
		for (FilterTender filterTender : myTenderFilter.getFilterTenders())
			if (tender.getId().equals(filterTender.getTender().getId())) {
				isMyTenderFilter = true;
				break;
			}
		return isMyTenderFilter;
	}

	@RolesAllowed({User.ROLE_USER})
	public void addTenderToMyTenderFilter(Long userId, Tender tender) {
		MyTenderFilter myTenderFilter = findMyTenderFilter(userId, MyTenderFilter.NAME);
		if (!isMyTenderFilter(myTenderFilter, tender)) {
			FilterTender filterTender = new FilterTender();
			filterTender.setTender(tender);
			filterTender.setTenderFilter(myTenderFilter);
			entityManager.persist(filterTender);
			myTenderFilter.getFilterTenders().add(filterTender);
		}
	}
	
	@RolesAllowed({User.ROLE_USER})
	public void removeTenderFromMyTenderFilter(Long userId, Tender tender) {
		MyTenderFilter myTenderFilter = findMyTenderFilter(userId, MyTenderFilter.NAME);
		FilterTender removefilterTender = null;
		for (FilterTender filterTender : myTenderFilter.getFilterTenders())
			if (tender.getId().equals(filterTender.getTender().getId())) {
				removefilterTender = filterTender;
				break;
			}
		if (removefilterTender != null) {
		  myTenderFilter.getFilterTenders().remove(removefilterTender);
		  removefilterTender.setTender(null);
		  removefilterTender.setTenderFilter(null);
		  entityManager.remove(removefilterTender);
		}
	}
}
