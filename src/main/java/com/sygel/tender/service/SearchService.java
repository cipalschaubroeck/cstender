/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Category;
import com.sygel.tender.domain.util.Cpv;
import com.sygel.tender.domain.util.Util;
import com.sygel.tender.ml.MLearningModels;
import com.sygel.tender.ml.util.AkteUtilities;
import com.sygel.tender.ml.util.XmlUtilities;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Stateless
@Local
public class SearchService {
	@PersistenceContext(unitName = "tender")
	private EntityManager entityManager;

	@Inject
	KnowledgeService knowledgeService;
	
	private final Logger logger = Logger.getLogger(SearchService.class);

	public Contractor findContractorByNameLocation(String name, String location) {
		Contractor contractor = null;
		TypedQuery<Contractor> q = entityManager.createNamedQuery("Contractor.findByNameLocation", Contractor.class);
		q.setParameter("name", name.toUpperCase());
		q.setParameter("location", location);
		q.setMaxResults(2);
		try {
			contractor = q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return contractor;
	}

	public Government findGovernmentByNameLocation(String name, String location) {
		Government government = null;
		TypedQuery<Government> q = entityManager.createNamedQuery("Government.findByNameLocation", Government.class);
		q.setParameter("name", name.toUpperCase());
		q.setParameter("location", location);
		q.setMaxResults(2);
		try {
			government = q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return government;
	}

	public Tender findTenderById(Long id) {
		EntityGraph eg = entityManager.getEntityGraph("Tender.attachments");
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("javax.persistence.fetchgraph", eg);
		Tender tender = entityManager.find(Tender.class, id, props);
		return tender;
	}
	
	public Government findGovernmentById(Long id) {
		EntityGraph eg = entityManager.getEntityGraph("Government.tenders");
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("javax.persistence.fetchgraph", eg);
		Government government = entityManager.find(Government.class, id, props);
		return government;
	}
	
	public Contractor findContractorById(Long id) {
		EntityGraph eg = entityManager.getEntityGraph("Contractor.awardItems");
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("javax.persistence.fetchgraph", eg);
		Contractor contractor = entityManager.find(Contractor.class, id, props);
		return contractor;
	}

	public Tender findTenderByAttachmentId(Long id) {
		// TODO fix this, this is not good, make a graph and get everything in one
		// go. Or retrieve the tenderid from the attachment.
		Attachment attachment = entityManager.find(Attachment.class, id);
		return attachment.getTender();
	}

	public List<Tender> findTenders(String searchString, boolean addAwards) {
		return findTenders( searchString,  addAwards, null);
	}
	
	public List<Tender> findTenders() {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findAll", Tender.class);
		q.setMaxResults(300);
		try {
			List<Tender> tenders = q.getResultList();
			upgradeTenderList(knowledgeService, tenders);
			return  tenders;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Tender> findTendersOrderByPublication() {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findAllOrderPublication1", Tender.class);
		q.setMaxResults(300);
		try {
			List<Tender> tenders = q.getResultList();
			upgradeTenderList(knowledgeService, tenders);
			return  tenders;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Tender> findTendersWithoutAttachment(Date fromPublication, Date toPublication) {
		List<Tender> tenders = findTenders(fromPublication, toPublication);
		
		System.out.println("findTendersWithoutAttachment findTenders size "+tenders.size());
		List<Tender> tendersWithoutAttachment  = new ArrayList<Tender>();
		List<Tender> tendersWithAttachment  = new ArrayList<Tender>();
		for (Tender tender : tenders) {
			//System.out.println("findTendersWithoutAttachment "+tender.getBdaMaster()+ " "+tender.getForm());
			boolean extraAttachment = false;
			for (Attachment attachment : tender.getAttachments()) {
				extraAttachment = extraAttachment || attachment.isExtraAttachment();
			}
			if (!extraAttachment) 
				tendersWithoutAttachment.add(tender);
			else {
				tendersWithAttachment.add(tender);
				System.out.println("findTendersWithoutAttachment WITH "+tender.getBdaMaster()+ " "+tender.getForm());
			}
				
		}
		System.out.println("findTendersWithAttachment size "+tendersWithAttachment.size());
		System.out.println("findTendersWithoutAttachment size "+tendersWithoutAttachment.size());
		return tendersWithoutAttachment;
	}
	
	public List<Tender> findTenders(Date fromPublication, Date toPublication) {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findByPublicationDate", Tender.class);
		q.setParameter("fromPublication", fromPublication);
		q.setParameter("toPublication", toPublication);
		
		q.setMaxResults(5000);
		try {
			List<Tender> tenders = q.getResultList();
			return  tenders;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Tender> findTenders(String searchString, boolean addAwards, Date lastModified) {
		List<Tender> tenders = null;
		
		String type = "Tender.findByQuery1";
		if (addAwards) type = "Tender.findByQueryWithAward";
		if (lastModified != null) type = "Tender.findByQueryChangeDate1";
		String exactCpv = null;
		if (searchString.startsWith("exact:")) {
			// 45000000
			searchString = searchString.substring(6);
			if (searchString.length() > 7) exactCpv = searchString.substring(0,8);
			//type = "Tender.findByQueryExactCategory1";
		}
		TypedQuery<Tender> q = entityManager.createNamedQuery(type, Tender.class);
		
		if (lastModified != null) q.setParameter("changeDate", lastModified);
		// if (addAwards)
		// q.setHint("javax.persistence.fetchgraph",
		// entityManager.getEntityGraph("Tender.award"));
		q.setParameter("searchString", upgradeSearchString(searchString, false, null));
		if (addAwards)
			q.setMaxResults(1000);
		else
			q.setMaxResults(1000);
		try {
			tenders = q.getResultList();
			if (exactCpv != null) {
				List<Tender> filteredList = new ArrayList<>();
				for (Tender tender:tenders) if (tender.getCpvNoParent() != null && exactCpv.equals(tender.getCpvNoParent().trim())) filteredList.add(tender);
				tenders = filteredList;
			}
			if (addAwards) {
				for (Tender t : tenders) {
					t.getAward();
				}
			}
		} catch (NoResultException e) {
			return null;
		}
		upgradeTenderList(knowledgeService, tenders);
		return tenders;
	}
	
	public static void upgradeTenderList(KnowledgeService knowledgeService, List<Tender> tenders) {
		for (Tender tender:tenders) {
			upgradeTender(knowledgeService, tender);
		}
	}
	
	public static void upgradeTender(KnowledgeService knowledgeService, Tender tender) {
		tender.setDisciplineTranslation(knowledgeService.getKnowledgeBase().translateDiscipline(tender.getDisciplinesCpvAI()));
		tender.setLocationTranslation(knowledgeService.getKnowledgeBase().translateLocation(tender.getLocation(), tender.getNutsNoParent()));		
    tender.setCategoryTranslation(knowledgeService.getKnowledgeBase().translateCategory(tender.getCategoryAI(), true).trim());
		tender.setDescription("<b>"+tender.getTitle() + "</b><br/>"+ tender.getEssence()+"<br/>"+ tender.getCategoryTranslation()+"<br/>");
	}

	protected void updateAttachmentSnippets(Attachment attachment, List<String> snippetList, String text, boolean fullSnippet) {
		if (text != null) {
			String textLowerCase = text.toLowerCase();
			for (String snippet : snippetList) {
				int sp = 0;
				if (!fullSnippet && (sp = snippet.indexOf(" ")) != -1) {
					snippet = snippet.substring(0, sp);
				}
				if (snippet.length() > 0) {
					int snippetPosition = textLowerCase.indexOf(snippet.toLowerCase());
					if (snippetPosition != -1) {
						if (snippetPosition > 10)
							snippetPosition -= 10;
						int endPosition = snippet.length() + snippetPosition + 100;
						if (endPosition > text.length())
							endPosition = text.length();
						String aSnippet = text.substring(snippetPosition, endPosition);
						snippetPosition = aSnippet.indexOf(" ");
						if (snippetPosition != -1)
							aSnippet = aSnippet.substring(snippetPosition + 1);
						endPosition = aSnippet.lastIndexOf(" ");
						if (endPosition != -1)
							aSnippet = aSnippet.substring(0, endPosition);
						attachment.setTextSnippet(aSnippet);
						break;
					} 
				}
			}
		} 
	}

	protected List<Attachment> updateAttachmentsSnippets(List<Attachment> attachments, List<String> snippetList) {
		List<Attachment> results = new ArrayList<Attachment>();
		for (Attachment attachment : attachments) {
			if (attachment.getText() != null) {
				String text = attachment.getText().replace("\r", " ").replace("\n", " ");
				updateAttachmentSnippets(attachment, snippetList, text, true);
				if (attachment.getTextSnippet() == null || attachment.getTextSnippet().length() == 0)
					updateAttachmentSnippets(attachment, snippetList, text, false);
				if (attachment.getTextSnippet() != null && attachment.getTextSnippet().length() > 0)
					results.add(attachment);
			}
		}
		return results;
	}
	
	public List<Government> findGovernments(String searchString) {
		List<Government> contractors = null;
		TypedQuery<Government> q = entityManager.createNamedQuery("Government.findByQuery1", Government.class);
		q.setParameter("searchString", upgradeContractorSearchString(searchString));
		q.setMaxResults(1000);
		try {
			contractors = q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return contractors;
	}
	
	public List<Contractor> findContractors(String searchString) {
		List<Contractor> contractors = null;
		TypedQuery<Contractor> q = entityManager.createNamedQuery("Contractor.findByQuery1", Contractor.class);
		q.setParameter("searchString", upgradeContractorSearchString(searchString));
		q.setMaxResults(1000);
		try {
			contractors = q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return contractors;
	}
	
	protected String upgradeContractorSearchString(String searchString) {	
		StringBuilder searchBuild = new StringBuilder();
		if (searchString != null && searchString.length()  > 0) {
		  searchString = searchString.replace("\u00a0"," ");
		  searchString = searchString.replace("@",Util.AT_ESCAPE); // eg. in emails
		  searchString = searchString.replaceAll("(?<!\\s)\\.(?!\\s)",Util.DOT_ESCAPE); // . not connected to a space, eg. in emails
		  searchString = searchString.replaceAll("(?<!\\s)-(?!\\s)",Util.HYPHEN_ESCAPE);// - not connected to a space, eg. in emails
		  String[] words = Util.constructWords(searchString);		  
		  for (String word : words) {
		  	if (word.length() < 4 && word.length() > 0 && '*' != word.charAt(0)) {
		  		searchBuild.append(Util.REVERSE_PREFIX+Util.reverseString(word)+" ");
		  	} else if (AkteUtilities.isWordType(word)) {
			  	searchBuild.append(Util.TYPE_PREFIX+Util.TYPE_PREFIX + word + " ");
			  	searchBuild.append(word + " ");
		  	} else if (word.matches("[0-9]{4}")) {
			  	searchBuild.append(Util.LOCATION_PREFIX + word + " ");
			  	searchBuild.append(word + " ");
		  	} else if (word.length()>1 && '*' == word.charAt(0))  {
			  	searchBuild.append(Util.REVERSE_PREFIX + Util.reverseString(word.substring(1)) + "* ");
			  } else if (!"*".equals(word) && !"+".equals(word)){
			  	searchBuild.append(word + " ");
			  }
		  }
	  }
		logger.trace("upgradeContractorSearchString "+searchBuild.toString());
		return searchBuild.toString();
	}

	public List<Attachment> findAttachments(String searchString) {
		List<Attachment> attachments = null;
		TypedQuery<Attachment> q = entityManager.createNamedQuery("Attachment.findByQuery1", Attachment.class);
		List<String> snippetList = new ArrayList<>();
		q.setParameter("searchString", upgradeSearchString(searchString, false, snippetList));
		q.setMaxResults(1000);
		try {
			attachments = q.getResultList();
			updateAttachmentsSnippets(attachments, snippetList);
		} catch (NoResultException e) {
			return null;
		}
		return attachments;
	}

	/**
	 * Remove the _CAT_ relation if the search is for a specific item on the
	 * category Remove the specific Item is the Item = Category
	 * 
	 * @param features
	 */
	protected void updateCatFeatures(HashMap<String, Integer> features) {
		List<String> deletedCatFeatures = new ArrayList<>();
		for (String feature : features.keySet()) {
			if (feature.contains("_CAT_")) {
				String nc = feature.replaceAll("_CAT_", "_");
				if (features.get(nc) == null)
					deletedCatFeatures.add(feature);
				else
					deletedCatFeatures.add(nc);
			}
		}
		for (String df : deletedCatFeatures)
			features.remove(df);
	}
	
	protected String convertWordToSnippet(String word) {
		String s = word;
		if (s.endsWith("*") && s.length() > 1) s = s.substring(0,s.length()-1);
		if (s.startsWith("+") && s.length() > 1) s = s.substring(1);	
		if (s.startsWith("*") && s.length() > 1) s = s.substring(1);

		return s;
	}

	protected String upgradeSearchString(String searchString, boolean addNamedEntities, List<String> snippetList) {
		if (searchString == null || searchString.length() == 0)
			return "";
		
	  searchString = searchString.replace("\u00a0"," ");
	  searchString = searchString.replace("@",Util.AT_ESCAPE); // eg. in emails
	  searchString = searchString.replaceAll("(?<!\\s)\\.(?!\\s)",Util.DOT_ESCAPE); // . not connected to a space, eg. in emails
	  searchString = searchString.replaceAll("(?<!\\s)-(?!\\s)",Util.HYPHEN_ESCAPE);// - not connected to a space, eg. in emails
		
		StringBuilder search = new StringBuilder();
		StringBuilder featuresearch = new StringBuilder();
		StringBuilder categorySearch = new StringBuilder();
		HashMap<String, Integer> features = new HashMap<String, Integer>();
		knowledgeService.getKnowledgeBase().extractFeatures(searchString, features);
		updateCatFeatures(features);
		for (String feature : features.keySet()) {
			if (feature.startsWith("REL_")) {
				if (feature.startsWith("REL_LOT_"))
					featuresearch.append(">" + feature + " ");
				else
					featuresearch.append(feature + " ");
				List<String> synList = knowledgeService.getKnowledgeBase().getSynonymsFeature(feature);
				for (String s : synList) {
					if (snippetList != null)
						snippetList.add(s);
					searchString = removeSynonyms(searchString, s);
				}
			}
			if (feature.startsWith("NE_") && addNamedEntities) {
				featuresearch.append(feature + " ");
			}
		}
		String[] words = searchString.split(" ");
		for (String word : words) {
      if (word.matches("[0-9]{4}")) {
				categorySearch.append("+" + Util.LOCATION_PREFIX + word + " ");
			} else if (AkteUtilities.isWordNuts(word)) {
				categorySearch.append("+" + Util.LOCATION_PREFIX + word.toUpperCase() + " ");
			} else if (AkteUtilities.isWordDiscipline(word)) {
				categorySearch.append("+" + Util.DISCIPLINE_PREFIX + word.toUpperCase() + " ");
			} else if (AkteUtilities.isWordCategory(word)) {
				categorySearch.append("+" + Util.CATEGORY_PREFIX + word.toUpperCase() + " ");
			} else if (AkteUtilities.isWordClass(word)) {
				categorySearch.append("+" + Util.CLASS_PREFIX + word.toUpperCase() + " ");
			} else if (AkteUtilities.isWordBDA(word)) {
				categorySearch.append("+"+ Util.BDA_PREFIX+word.replace("-", Util.HYPHEN_ESCAPE) + " ");
			} else if (word.length() < 4 && word.length() > 0 && '*' != word.charAt(0) && !"*".equals(word) && !"+".equals(word)) {
	  		search.append(Util.REVERSE_PREFIX+Util.reverseString(word)+" ");
	  		if (snippetList != null) snippetList.add(convertWordToSnippet(word));
	  	} else if (word.length()>1 && '*' == word.charAt(0))  {
	  		search.append(Util.REVERSE_PREFIX + Util.reverseString(word.substring(1)) + "* ");
	  		if (snippetList != null) snippetList.add(convertWordToSnippet(word));
			} else if (!"*".equals(word) && !"+".equals(word)) {
				search.append(word + " ");
				if (snippetList != null) snippetList.add(convertWordToSnippet(word));
			}
		}
		String s = (search.toString() + " " + featuresearch.toString()).trim();
		s = s.replaceAll("  ", " ");
		if (s.endsWith("+")) s = s.substring(0,s.length()-1);

		if (categorySearch.length() > 0)
			s = "+(" + s + ") +(" + categorySearch + ")";
		logger.trace("SearchService::upgradeSearchString " + s);
		return s;
	}

	private String removeSynonyms(String searchString, String s) {

		return searchString.replaceAll(s, "");
	}

	public File createAttachmentFile(String noticeId, String attachmentName, String extension) {
		File tempFile = null;
		try {
			tempFile = File.createTempFile("tmp", "." + extension);
			File folder = ImportService.getDataFolder(ImportService.DATA_FOLDER, noticeId);
			System.out.println(folder.getAbsolutePath());

			ZipFile zipFile = new ZipFile(new File(folder, "attachment.zip"));

			for (Enumeration e = zipFile.entries(); e.hasMoreElements();) {
				ZipEntry zipEntry = (ZipEntry) e.nextElement();
				String name = zipEntry.getName();
				if (name.equals(attachmentName)) {
					ImportService.writeInputStream(zipFile.getInputStream(zipEntry), tempFile);
				}
			}

		} catch (IOException e) {
			logger.error("createAttachmentFile" + e);
			e.printStackTrace();
		}

		return tempFile;

	}
	
	public Collection<Category> constructCategoryList(List<Tender> tenders) {
		HashMap<String, Category> categoryMap = new HashMap<>();
		int l = Util.CATEGORY_PREFIX.length();
		for (Tender tender : tenders) {
			String c = tender.getCategoryAI();
			if (c != null && c.length() > 0) {
				String[] codes = c.split(":");
				for (String code : codes) {
					if (code.startsWith(Util.CATEGORY_PREFIX)) {
						code = code.substring(l);
						Category category = categoryMap.get(code);
						if (category == null) {
							category = new Category(code, knowledgeService.getKnowledgeBase().translateCategory(code, false), 1);
							categoryMap.put(code, category);
						} else {
							category.addOccurance();
						}
					}
				}
			}
		}
		return categoryMap.values();		
	}

	public Collection<Cpv> constructCpvList(List<Tender> tenders) {
		HashMap<String, Cpv> cpvMap = new HashMap<>();
		for (Tender tender : tenders) {
			String c = tender.getCpvNoParentAI();
			if (c != null && c.length() > 0) {
				String[] codes = c.trim().split(" ");
				for (String code : codes) {
					
					Cpv cpv = cpvMap.get(code);
					if (cpv == null) {
						cpv = new Cpv(code, knowledgeService.getKnowledgeBase().translateCPV(code), 1);
						cpvMap.put(code, cpv);
					} else {
						cpv.addOccurance();
					}
				}
			}
		}
		return cpvMap.values();
	}
	
	public Collection<AwardItem> findAwardItems(String searchString) {
		return constructAwardItemList(findTenders(searchString, true));
	}

	public Collection<AwardItem> constructAwardItemList(List<Tender> tenders) {
		List<AwardItem> awardItems = new ArrayList<>();

		for (Tender tender : tenders) {
			if (tender.getAward() != null && tender.getAward().getAwardItems() != null) {
				for (AwardItem item : tender.getAward().getAwardItems()) {
					if (item.getContractorName() != null && item.getContractorName().length() > 0) {
						if (item.getTitle() == null || item.getTitle().length() == 0 ) {
							item.setDisplayTitle(tender.getTitle());
						} else {
							item.setDisplayTitle(item.getTitle());
						}
					  awardItems.add(item);
					}
				}
			}
		}

		return awardItems;
	}
	public Tender findTenderByNoticeId(String noticeId) {
		Tender tender = null;
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findByNoticeId", Tender.class);
		q.setParameter("noticeId", noticeId);
		try {
			tender = q.getSingleResult();
			return tender;
		} catch (NoResultException e) {
			return null;
		}
	}
	public List<Tender> findTenderAll() {
		TypedQuery<Tender> q = entityManager.createNamedQuery("Tender.findAll", Tender.class);
		try {
			return q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}
