/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.ml.KnowledgeBase;
import com.sygel.tender.ml.MLearningModels;
import com.sygel.tender.ml.classify.ClassifierInterface;
import com.sygel.tender.ml.classify.FeatureList;
import com.sygel.tender.ml.util.FileUtils;
import com.sygel.tender.ml.util.Utils;

/**
 * Import tender service
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Stateless
@Local
public class ImportPreService {
	@PersistenceContext(unitName = "tender")
	private EntityManager entityManager;
	
	private final Logger logger = Logger.getLogger(ImportPreService.class);
	
	@Inject
	KnowledgeService knowledgeService;
	
	@Inject
	ImportService importService;
	
	@Inject
	SearchService searchService;
	/**
	 * The BES xml and document XML is extracted from the ZIP.  The notice is not downloaded from enot.
	 * This function can be used for old ZIP files which are no longer downloadable from ENOT.
	 * @param folder
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void importTenderFolderDirectFromZip(File folder) {
		File[] files = folder.listFiles();
		Arrays.sort(files, new Comparator<File>(){
	    public int compare(File f1, File f2)
	    {
	        return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
	    }
	  });
		for (File file : files) {
			importService.importTenderDirectFromZip(file);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void importHistory() {
		for (int i = TrainingService.IMPORT_BEGIN; i < TrainingService.IMPORT_END; i=i+TrainingService.IMPORT_STEP_SMALL) {
			importService.importHistory(i, i+ TrainingService.IMPORT_STEP_SMALL);
		}	
	}	
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void importAttachmentHistory() {
		for (int i = TrainingService.IMPORT_BEGIN; i < TrainingService.IMPORT_END; i=i+TrainingService.IMPORT_STEP_SUPER_SMALL) {
			importService.importAttachmentHistory(i, i+ TrainingService.IMPORT_STEP_SUPER_SMALL);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error(e);
			}
		}	
	}	
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateTenderAI() {
		KnowledgeBase knowledgeBase = knowledgeService.getKnowledgeBase();		
		HashMap<String, List<String>> cpvDisciplineMap;
		try {
			cpvDisciplineMap = knowledgeBase.getCpvDisciplineMap();
			FeatureList dataStructure = MLearningModels.initCpvClassifierFeatureStructure();			
			List<String> cpvList = knowledgeService.getKnowledgeBase().getModelCpvList();
			Map<String,String> categoryCpvMap = knowledgeBase.getCategoryCpvMap();
			HashMap<String, ClassifierInterface> classifiers = MLearningModels.initCpvClassifierLibSvmMap(cpvList);
		
		
		  for (int i = TrainingService.IMPORT_BEGIN; i < TrainingService.IMPORT_END; i=i+TrainingService.IMPORT_STEP) {
			  importService.updateTendersAI(i, i+ TrainingService.IMPORT_STEP, cpvDisciplineMap, categoryCpvMap, knowledgeBase, dataStructure, classifiers);
		  }	
		}catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}		
	}	
	
	/**
	 * The noticeIds are extracted from the zips in the folder.
	 * All noticeIds are downloaded from enot website.
	 * 
	 * @param folder with the daily enot zips
	 * @param progress object that can be used in the UI
	 * @return message with the number of processed tenders and the tenders who failed to download.
	 */
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Future<String> importTenderFolder(File folder, Progress progress) {
		File[] files = folder.listFiles();
		List<String> failedNoticeIds = new ArrayList<String>();
		int totalFiles = 0;
		for (File file : files) {
			if (file.getName().endsWith(".zip")) {
				totalFiles++;
			}
	  }
		totalFiles = totalFiles *3;
		int counter  = 0;
		long importedFiles = 0;
		for (File file : files) {
			if (file.getName().endsWith(".zip")) {
				Date fileDate = new Date(file.lastModified());
				logger.info("importService.importUpdatTender from "+fileDate);
				
				List<String> noticeIds = importService.importBijlagen(file, fileDate,failedNoticeIds);
				if (noticeIds == null) {
					return new AsyncResult<String>(new String(importedFiles + " - ERROR DOWNLOADING FILES PLEASE TRY AGAIN"));
				}
				
				importedFiles += noticeIds.size();
				logger.info("importService.importUpdatTender(noticsIds);");
				importService.importUpdateTender(noticeIds);
				if (progress != null) progress.setProgress(((float) ++counter) / totalFiles);
				logger.info("importService.updateTendersAI(noticsIds)");
				importService.updateTendersAI(noticeIds);
				if (progress != null) progress.setProgress(((float) ++counter) / totalFiles);
				logger.info("importService.importUpdateTenderAttachments(noticsIds)");				
				int size = noticeIds.size() / 5;
				if (size > 2) {
					logger.info("importService.importUpdateTenderAttachments(noticsIds) split in " + size + " total "+ noticeIds.size());	
				  List<List<String>> partitions = Utils.splitList(noticeIds, size);
				  int t  = 0;
				  for (List<String> noticeIdsPartition : partitions) {
				    importService.importUpdateTenderAttachments(noticeIdsPartition);
				    t += noticeIdsPartition.size();
						logger.info("importService.importUpdateTenderAttachments(noticsIds) batch size " + noticeIdsPartition.size() + " total "+ t);	
				  }
				} else 
					importService.importUpdateTenderAttachments(noticeIds);
				logger.info("importService.renameTo");
				file.renameTo(new File(ImportService.DATA_FOLDER + "/enot/done/" + ImportService.getDoneFileName(file)));
				if (progress != null) progress.setProgress(((float) ++counter) / totalFiles);
			}
		}
		StringBuilder messages = null;
		if (failedNoticeIds.size() == 0) messages = new StringBuilder(" 0");
		else {
			messages = new StringBuilder();
		  for (String m : failedNoticeIds) messages.append(" - "+m);
		}
		return new AsyncResult<String>(new String(importedFiles + " - failed"+messages.toString()));
	}
	
	/**
	 * Import attachments form EBP
	 * @param fromPublication
	 * @param toPublication
	 */
	public void importEBPAttachments(Date fromPublication, Date toPublication) {
		System.out.println("importEBPAttachments S");
		List<Tender> tendersWithoutAttachment = searchService.findTendersWithoutAttachment(fromPublication, toPublication);
		System.out.println("importEBPAttachments size "+tendersWithoutAttachment.size());
		/*
		ScraperInterface scraper = new JavaScraper();
		List<String> sb = new ArrayList<String>();
		int counter = 0;
		sb.add("importEBPAttachments size "+tendersWithoutAttachment.size());
		for (Tender tender : tendersWithoutAttachment) {
			String ebpAttachmentResult = scraper.getPageSource("http://enotwebservice.ebp.be/api/Attachment/PX0lXMQPCC9MrooYqKwOB55g0qxMuPo670EzFvSSSoc/"+tender.getBdaMaster());
			sb.add("tenderwithout "+ tender.getBda()+" "+tender.getForm()+"\n");
			sb.add("ebpAttachmentResult "+ ebpAttachmentResult+"\n");
			if (ebpAttachmentResult.indexOf(".zip") != -1) {
				counter++;
				sb.add("ebpAttachmentResult "+tender.getBdaMaster()+" FOUND ZIP "+tender.getForm()+"\n");
			}
		}
		sb.add("importEBPAttachments EBP attachment size "+ counter);
		FileUtils.writeList(sb, "/Users/jurrien/Documents/projects/ml/tender/ebp.txt");
		System.out.println("importEBPAttachments EBP attachment size "+ counter);
		*/
	}
}
