/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import com.sygel.tender.ml.KnowledgeBase;

/**
 * KnowledgeBase entry point.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Singleton
public class KnowledgeService {
	KnowledgeBase knowledgeBase = null;	
	
	@PostConstruct
    public void init() {
		knowledgeBase = new KnowledgeBase();
    }

	public KnowledgeBase getKnowledgeBase() {
		return knowledgeBase;
	}

	public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}	
}
