/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.apache.log4j.Logger;

import com.vaadin.server.VaadinSession;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Singleton
public class I18Service {
	private final Logger logger = Logger.getLogger(I18Service.class);
	
	HashMap<Locale, ResourceBundle> resourceBundleMap;	
	
	@PostConstruct
  public void init() {
		resourceBundleMap = new HashMap<>();	
  }
	
	public String getMessage(String key) {
		return getMessage(key, VaadinSession.getCurrent().getLocale());
	}
	
	public String getMessage(String key, Locale locale) {
		//logger.trace("I18Service "+locale.toString());
		try {
		  ResourceBundle resourceBundle = resourceBundleMap.get(locale);
		  if (resourceBundle == null) {
			  resourceBundle = ResourceBundle.getBundle("com/sygel/tender/resources/language",locale);
			  if (resourceBundle != null)
			    resourceBundleMap.put(locale, resourceBundle);
		  }		
		
		  String translation = resourceBundle.getString(key);
		  return translation;
		} catch (Exception e) {
			return key+" "+locale.toString();
		}		
	}
}
