/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

/**
 * Defines the progress of a report.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Progress {
  // Volatile because read in another thread
  private volatile float progress = 0;
  /**
  * @return progress value between 0 and 1
  */
  public float getProgress() {
	return progress;
  }
  /**
   * 
   * @param progress value between 0 and 1
   */
  public void setProgress(float progress) {
	this.progress = progress;
  }
}
