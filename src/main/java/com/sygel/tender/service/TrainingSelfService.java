/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.namespace.QName;
import javax.xml.ws.Response;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Util;
import com.sygel.tender.ml.KnowledgeBase;
import com.sygel.tender.ml.MLearningModels;
import com.sygel.tender.ml.classify.ClassifierInterface;
import com.sygel.tender.ml.classify.FeatureList;
import com.sygel.tender.ml.util.AkteUtilities;
import com.sygel.tender.ml.util.XmlUtilities;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Stateless
@Local
public class TrainingSelfService {
	@PersistenceContext(unitName = "tender")
	private EntityManager entityManager;
	
	private final Logger logger = Logger.getLogger(TrainingSelfService.class);

	@Inject
	KnowledgeService knowledgeService;

	@Inject
	SearchService searchService;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateCpvAI(int start, int end,KnowledgeBase filter, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers, HashMap<String, List<String>> cpvDisciplineMap, Map<String, String> categoryCpvMap) {
		try {
			for (int i = start; i < end; i = i + 1) {
				logger.info("findTenderByNoticeId "+i);
				Tender tender = searchService.findTenderByNoticeId(i + "");
				if (tender != null) {
					//System.out.println(tender.getNoticeId() + " " + tender.getBda());
					tender.setLanguage("NL");
					tender.setCpvAI("");
					tender.setCpvNoParentAI("");
					tender.setCpvPureAI("");
					tender.setDisciplinesCpvAI("");

					String titel = tender.getTitle1();
					if (titel != null && titel.length() > 2) {			
            MLearningModels.updateTenderAI(tender, filter, dataStructure, classifiers, knowledgeService.getKnowledgeBase());
					  if (tender.getCpvNoParentAI() != null && tender.getCpvNoParentAI().trim().length() > tender.getCpvNoParent().trim().length()) {
					  	logger.info("CPV ADDED "+tender.getNoticeId()+ " "+tender.getBda()+tender.getTitle() + " "+tender.getEssence() + " " + tender.getCpvNoParentAI()+ " oc: " + tender.getCpvNoParent());
					  	if (tender.getLot() != null && tender.getLot().length()>2)
					  		logger.info("LOT "+tender.getLot());
					  }
					  MLearningModels.updateDiscipline(tender, cpvDisciplineMap,categoryCpvMap, true);
					}
				}
			}			
		} catch (Exception e) {		
			e.printStackTrace();				
		}
	}
}
