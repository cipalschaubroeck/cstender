/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.service;


import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

/**
 * Deamon for test and maintenance tasks
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Startup
@Singleton
//@DependsOn("AkteToTenderService")
@TransactionManagement(TransactionManagementType.BEAN)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class StartDaemon {
	@Inject
	ImportService importService;
	
	@Inject
	ImportPreService importPreService;
	
	@Inject
	TrainingService trainingService;
	
	@Inject
	UserService userService;
	
	@Inject
	PushService pushService;
	
	private final Logger logger = Logger.getLogger(StartDaemon.class);
	
	@PostConstruct
	void init() {
		logger.info("init create user");
		//userService.createUser("js@sygel.com","test","jurrien","saelens");
  }
}
