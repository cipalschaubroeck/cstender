package com.sygel.tender.report.jasper;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class MasterDataSource {
	private DynamicColumnDataSource listDataSource = null;
	private DynamicColumnDataSource filterDataSource = null;
	
	public DynamicColumnDataSource getListDataSource() {
		return listDataSource;
	}
	public void setListDataSource(DynamicColumnDataSource listDataSource) {
		this.listDataSource = listDataSource;
	}
	public DynamicColumnDataSource getFilterDataSource() {
		return filterDataSource;
	}
	public void setFilterDataSource(DynamicColumnDataSource filterDataSource) {
		this.filterDataSource = filterDataSource;
	}
}
