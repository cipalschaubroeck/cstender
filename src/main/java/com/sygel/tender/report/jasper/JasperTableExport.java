package com.sygel.tender.report.jasper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Table;

/**
 * Export a Vaadin table with optionally a filter (fieldgroup) to Jasper. PDF
 * export and print to the default printer are supported.
 * 
 * @author jurrien
 */
public class JasperTableExport extends TableExport {
	private static final long serialVersionUID = -1683452748731184723L;
	private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JasperTableExport.class);
	private DynamicColumnDataSource listDataSource = null;
	private DynamicColumnDataSource filterDataSource = null;
	private MasterDataSource masterDataSource = null;
	private JasperPrint jasperPrint = null;
	private BeanFieldGroup<?> fieldGroup = null;
	/**
	 * The title of the "report" of the table contents.
	 */
	protected String reportTitle;

	/**
	 * 
	 * @param table
	 */
	public JasperTableExport(Table table) {
		super(table);
	}

	/**
	 * 
	 * @param table
	 * @param fieldGroup
	 */
	public JasperTableExport(Table table, BeanFieldGroup<?> fieldGroup) {
		super(table);
		this.fieldGroup = fieldGroup;
	}

	/**
	 * Create and export the Table contents as a PDF. Only the export() method
	 * needs to be called. If the user wishes to manipulate the converted object
	 * to export, then convertTable() should be called separately, and, after
	 * manipulation, sendConverted().
	 */
	public void export() {
		convertTable();
		sendConverted();
	}

	/**
	 * Create and print the Table contents. Only the print() method needs to be
	 * called. If the user wishes to manipulate the converted object to print,
	 * then convertTable() should be called separately, and, after manipulation,
	 * printConverted().
	 * 
	 * @param printerName
	 *          name of the printer to print on, or null for the system-default
	 *          printer
	 */
	public void print(String printerName) {
		convertTable();
		printConverted(printerName);
	}

	@Override
	public void convertTable() {
		this.listDataSource = new DynamicColumnDataSource(getTableHolder());
		this.filterDataSource = new DynamicColumnDataSource(getFieldGroup());
		this.masterDataSource = new MasterDataSource();
		this.masterDataSource.setListDataSource(listDataSource);
		this.masterDataSource.setFilterDataSource(filterDataSource);
		InputStream is = null;
		try {
			is = getClass().getClassLoader().getResourceAsStream("/com/sygel/tender/report/jasper/DynamicColumns.jrxml");
			JasperDesign jasperReportDesign = JRXmlLoader.load(is);
			logger.trace("Adding the dynamic columns");
			DynamicReportBuilder reportBuilder = new DynamicReportBuilder(jasperReportDesign, listDataSource.size());
			reportBuilder.addDynamicColumns();
			logger.trace("Compiling the report");
			JasperReport jasperDynamicReport = JasperCompileManager.compileReport(jasperReportDesign);

			is = getClass().getClassLoader().getResourceAsStream("/com/sygel/tender/report/jasper/DynamicFilters.jrxml");
			JasperDesign jasperFilterDesign = JRXmlLoader.load(is);
			DynamicReportBuilder filterBuilder = new DynamicReportBuilder(jasperFilterDesign, 2);
			filterBuilder.addDynamicColumns();
			JasperReport jasperFilterReport = JasperCompileManager.compileReport(jasperFilterDesign);

			is = getClass().getClassLoader().getResourceAsStream("/com/sygel/tender/report/jasper/DynamicMaster.jrxml");
			JasperDesign jasperMasterDesign = JRXmlLoader.load(is);
			JasperReport jasperMasterReport = JasperCompileManager.compileReport(jasperMasterDesign);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("REPORT_TITLE", getReportTitle());
			params.put("Subreport_1", jasperFilterReport);
			params.put("Subreport_2", jasperDynamicReport);
			params.put("data", masterDataSource);

			logger.trace("Filling the report");
			this.jasperPrint = JasperFillManager.fillReport(jasperMasterReport, params, new JREmptyDataSource());
		} catch (JRException e) {
			logger.error("JRException error. " + e);
		}
	}

	/**
	 * Export the workbook to the end-user.
	 * <p/>
	 * Code obtained from:
	 * http://vaadin.com/forum/-/message_boards/view_message/159583
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean sendConverted() {
		File tempFile = null;
		FileOutputStream fileOut = null;
		try {
			tempFile = File.createTempFile("tmp", ".pdf");
			fileOut = new FileOutputStream(tempFile);
			logger.trace("Exporting the report to pdf");
			JasperExportManager.exportReportToPdfStream(this.jasperPrint, fileOut);
			if (null == mimeType) {
				setMimeType("application/pdf");
			}
			final boolean success = super.sendConvertedFileToUser(getTableHolder().getUI(), tempFile, "pdfexport.pdf");
			return success;
		} catch (final IOException e) {
			logger.error("Converting to PDF failed with IOException " + e);
			return false;
		} catch (JRException e) {
			logger.error("Converting to PDF failed with JRException " + e);
			return false;
		} finally {
			tempFile.deleteOnExit();
			try {
				fileOut.close();
			} catch (final IOException e) {
			}
		}
	}

	/**
	 * Print report to a printer.
	 * 
	 * @param printerName
	 *          the name of the printer to print on, or null for the system
	 *          default printer
	 */
	public void printConverted(String printerName) {
		long start = System.currentTimeMillis();
		PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
		printRequestAttributeSet.add(MediaSizeName.ISO_A4);

		PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
		if (printerName != null)
			printServiceAttributeSet.add(new PrinterName(printerName, null));

		JRPrintServiceExporter exporter = new JRPrintServiceExporter();
		exporter.setExporterInput(new SimpleExporterInput(this.jasperPrint));
		SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
		configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
		configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
		configuration.setDisplayPageDialog(false);
		configuration.setDisplayPrintDialog(false);
		exporter.setConfiguration(configuration);
		try {
			exporter.exportReport();
		} catch (JRException e) {
			logger.error("JRException error. " + e);
		}
		logger.trace("Printing time : " + (System.currentTimeMillis() - start));
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public BeanFieldGroup<?> getFieldGroup() {
		return fieldGroup;
	}

	/**
	 * 
	 * @param fieldGroup
	 *          the fieldgroup should contain the filter used to generate the
	 *          report.
	 */
	public void setFieldGroup(BeanFieldGroup<?> fieldGroup) {
		this.fieldGroup = fieldGroup;
	}
}
