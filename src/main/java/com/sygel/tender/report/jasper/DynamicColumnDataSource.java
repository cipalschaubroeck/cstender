package com.sygel.tender.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.vaadin.addon.tableexport.TableHolder;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Field;

/**
 * Implementation of a Jasper Reports data source that can contain a dynamic number of columns
 * Based on:
 * @see <a href="http://sdg.repositoryhosting.com/git_public/sdg/sdg-blog.git/tree/HEAD:/JasperDynamicColumns">Source</a>
 */
public class DynamicColumnDataSource extends JRAbstractBeanDataSource {
        private List<String> columnHeaders;
        private List<List<String>> rows;
        private Iterator<List<String>> iterator;
        private List<String> currentRow;
        public DynamicColumnDataSource(BeanFieldGroup<?> fieldGroup)
        {      	
        	super(true);
            this.columnHeaders = new ArrayList<String>(); 
            this.rows = new ArrayList<List<String>>(); 
            // add filter
            this.columnHeaders.add("Field");
            this.columnHeaders.add("Value");
            if (fieldGroup != null) {       
              for (Field<?> filterField: fieldGroup.getFields()) {
            	List<String> row = new ArrayList<String>();
            	row.add(filterField.getCaption());
            	if (filterField.getValue() != null)
            	  row.add(filterField.getValue().toString());
            	else
            	  row.add("-");	
            	rows.add(row);
              }
            }
            this.iterator = this.rows.iterator();    
        }
        public DynamicColumnDataSource(TableHolder tableHolder )
        {      	
            super(true);
            this.rows = new ArrayList<List<String>>(); 

            // add table
            List<Object> propIds = tableHolder.getPropIds();           
            this.columnHeaders = new ArrayList<String>();                       
            for(final Object propId : propIds) {
            	this.columnHeaders.add(tableHolder.getColumnHeader(propId));
            }            
            final Collection<?> itemIds = tableHolder.getContainerDataSource().getItemIds();            
            for (final Object itemId : itemIds) {
            	List<String> row = new ArrayList<String>();
            	for(final Object propId : propIds) {
            	  Property prop = tableHolder.getContainerDataSource().getContainerProperty(itemId, propId);
            	  if (prop != null && prop.getValue() != null) 
            	    row.add(prop.getValue().toString());
            	  else
            		row.add("");
            	}
            	rows.add(row);
            }                       
            this.iterator = this.rows.iterator();            
        }
        
        public int size() {
        	return columnHeaders.size();
        }

        @Override
        public boolean next()
        {
            boolean hasNext = false;
            if (iterator != null)
            {
                hasNext = iterator.hasNext();
                if (hasNext)
                {
                    this.currentRow = iterator.next();
                }
            }
            return hasNext;
        }

        @Override
        public Object getFieldValue(JRField field) throws JRException
        {
            // The name of the field in dynamic columns that were created by DynamicReportBulder is also the index into the list of columns.
            // For example, if the field is named 'col1', this is the second (because it's zero-based) column in the currentRow.
            String fieldName = field.getName();
            if (fieldName.startsWith(DynamicReportBuilder.COL_EXPR_PREFIX)) {
                String indexValue = fieldName.substring(DynamicReportBuilder.COL_EXPR_PREFIX.length());
                String column = currentRow.get(Integer.parseInt(indexValue));
                return column;
            } else if (fieldName.startsWith(DynamicReportBuilder.COL_HEADER_EXPR_PREFIX)) {
                int indexValue = Integer.parseInt(fieldName.substring(DynamicReportBuilder.COL_HEADER_EXPR_PREFIX.length()));
                String columnHeader = columnHeaders.get(indexValue);
                return columnHeader;
            } else {
                throw new RuntimeException("The field name '" + fieldName + "' in the Jasper Report is not valid");
            }
        }


        @Override
        public void moveFirst()
        {
            if (rows != null)
            {
                iterator = rows.iterator();
            }
        }

    }
