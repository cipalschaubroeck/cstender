/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.sygel.tender.domain.util.Util;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Government")

@NamedNativeQueries({
	@NamedNativeQuery(name = "Government.findByQuery1", query = "SELECT * "
			+ " FROM Government WHERE MATCH (reverseIndex) AGAINST (:searchString IN BOOLEAN MODE) order by name asc", resultClass = Government.class)

})

@NamedQueries({
    @NamedQuery(name = "Government.findByNameLocation", query = "SELECT g FROM Government g WHERE UPPER(g.name) = :name AND g.location = :location"),  
    @NamedQuery(name = "Government.findAll", query = "SELECT g FROM Government g")    
})

@NamedEntityGraphs({
	@NamedEntityGraph(name = "Government.tenders", attributeNodes = { 
			@NamedAttributeNode("id"), 
			@NamedAttributeNode("name"), 
			@NamedAttributeNode("type"), 
			@NamedAttributeNode("street"), 
			@NamedAttributeNode("location"), 
			@NamedAttributeNode("disciplines"), 
			@NamedAttributeNode("cpv"), 
			@NamedAttributeNode("tenders"), 
			@NamedAttributeNode("contacts")
			}),
 })


public class Government {
	public static final String[] ACTIVITY = {"GENERAL_PUBLIC_SERVICES",
		"SOCIAL_PROTECTION","EDUCATION","HEALTH","ENVIRONMENT","PUBLIC_ORDER_AND_SAFETY",
		"HOUSING_AND_COMMUNITY_AMENITIES","DEFENCE","ECONOMIC_AND_FINANCIAL_AFFAIRS","RECREATION_CULTURE_AND_RELIGION"};
	
	public static final String[] TYPES = {"city","police","railway","firedep","hospital","school","university","army","ocmw","traffic","building"};
	public static final String[][] TYPES_KEYS = {
		{"gemeente","gemeentebestuur","stad","stadsbestuur","ville d","commune d","administration communale","college van burgemeester"},
		{"politiezone","police","politie"},
		{"infrabel","sncb","nmbs"},
		{"brandweer"},
		{"ziekenhuis","ziekenhuizen","az","hospitalier","hospitalière","neurologique","clinique","cliniques"},
		{"ecole","scolaire","school","scholen","onderwijs","scholengroep","schoolbestuur","basisscholen","basisonderwijs","kleuterschool"},
		{"hogeschool","k.u.leuven","ku","ku leuven","universiteit"},
		{"defence","defensie"},
		{"ocmw","centre public d'action sociale","cpas","o.c.m.w.","c.p.a.s."},
		{"wegen en verkeer","vervoermaatschappij","mobiliteitsbedrijf","direction des routes","direction de la coordination des districts routiers","direction de la gestion du trafic routier"},
		{"regie der gebouwen","woningbeheer","woningfonds","régie des bâtiments","service bâtiments","service technique des bâtiments","sociale woningen","habitations sociales"}
	                                     };
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Size(max = 255)
	private String name;
	
	@Size(max = 11)
	private String type;
	
	@Size(max = 255)
	private String street;
	
	@Size(max = 255)
	private String location;
	
	@Size(max = 255)
	private String disciplines;
	
	@Size(max = 255)
	private String activity;
	
	@Size(max = 255)
	private String cpv;
	
	@Lob
	private String reverseIndex;
	
	@OneToMany(mappedBy = "government")
	private Set<Tender> tenders;
	
	@OneToMany(mappedBy = "government", cascade = {CascadeType.REMOVE,CascadeType.PERSIST}, orphanRemoval=true)
	private Set<Contact> contacts = new HashSet<>();
	
	@Transient
	private String locationString;
	
	@Transient 
	private String typeString;

	@PreUpdate
	@PrePersist
	public void updateReverseIndex() {	
		StringBuilder contactBuilder = new StringBuilder();
		if (getContacts() != null)
		  for (Contact contact : getContacts()) {
		  	contactBuilder.append(contact.toString());
		  }
		reverseIndex = Util.constructDBIndex(name + " " + street + " "+contactBuilder.toString())+" "+location+ (type != null ? " "+type : "");		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {		
		if (name != null && name.length() > 254) name = name.substring(0,254);
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		if (street != null && street.length() > 254) street = street.substring(0,254);
		this.street = street;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if (location != null && location.length() > 254) location = location.substring(0,254);
		this.location = location;
	}

	public Set<Tender> getTenders() {
		return tenders;
	}

	public void setTenders(Set<Tender> tenders) {
		this.tenders = tenders;
	}

	public String getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(String disciplines) {
		if (disciplines != null && disciplines.length() > 254) disciplines = disciplines.substring(0,254);
		this.disciplines = disciplines;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		if (cpv != null && cpv.length() > 254) cpv = cpv.substring(0,254);
		this.cpv = cpv;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getLocationString() {
		return locationString;
	}

	public void setLocationString(String locationString) {
		this.locationString = locationString;
	}

	public String getTypeString() {
		return typeString;
	}

	public void setTypeString(String typeString) {
		this.typeString = typeString;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		if (activity != null && activity.length() > 254) activity = activity.substring(0,254);
		this.activity = activity;
	}	
}
