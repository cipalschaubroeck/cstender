/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Award")
public class Award {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@OneToMany(mappedBy = "award")
	private Collection<NoticeText> awardTexts = new Vector<>();

	@OneToOne(mappedBy = "award", fetch=FetchType.EAGER)
	private Tender tender;

	@OneToMany(mappedBy = "award", fetch=FetchType.EAGER)
	private List<AwardItem> awardItems = new Vector<>();
	
	String form;  
	
	Date publication;
	
	@Size(max = 40)
	String attachmentName1;
	@Size(max = 40)
	String attachmentName2;
	@Size(max = 40)
	String attachmentName3;
	@Size(max = 40)
	String attachmentName4;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<NoticeText> getAwardTexts() {
		return awardTexts;
	}

	public void setAwardTexts(Collection<NoticeText> awardTexts) {
		this.awardTexts = awardTexts;
	}

	public Tender getTender() {
		return tender;
	}

	public void setTender(Tender tender) {
		this.tender = tender;
	}


	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public Date getPublication() {
		return publication;
	}

	public void setPublication(Date publication) {
		this.publication = publication;
	}	

	public List<AwardItem> getAwardItems() {
		return awardItems;
	}

	public void setAwardItems(List<AwardItem> awardItems) {
		this.awardItems = awardItems;
	}

	public NoticeText getNoticeText(String language) {		  
		  Collection<NoticeText> noticeTexts = getAwardTexts();
		  NoticeText text = null;
		  if (noticeTexts != null)
			  for (NoticeText noticeText : noticeTexts) {
				  if (language.equals(noticeText.getLanguage())) {
					  return noticeText;
				  }
			  }
		  return text;
	  } 
	public void setAttachmentName(String language, String attachmentName) {
		for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
			if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName1 = attachmentName;
			else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName2 = attachmentName;
			else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName3 = attachmentName;
			else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName4 = attachmentName;
		}
	}

	public String getAttachmentName(String language) {
		for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
			if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName1;
			else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName2;
			else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName3;
			else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName4;
		}
		return null;
	}

	public String getAttachmentName() {
		String t = getAttachmentName(tender.getLanguage());
		if (t == null) {
			if (attachmentName1 != null && attachmentName1.length() > 0)
				t = attachmentName1;
			else if (attachmentName2 != null && attachmentName2.length() > 0)
				t = attachmentName2;
			else if (attachmentName3 != null && attachmentName3.length() > 0)
				t = attachmentName3;
			else if (attachmentName4 != null && attachmentName4.length() > 0)
				t = attachmentName4;
		}
		return t;
	}
	
	public String getAttachmentPdfName() {
		String n = getAttachmentName();
		if (n != null && n.endsWith(".xml")) n = n.substring(0,n.length()-3) + "pdf";
		return n;
	}
	public String getAttachmentName1() {
		return attachmentName1;
	}

	public void setAttachmentName1(String attachmentName1) {
		this.attachmentName1 = attachmentName1;
	}

	public String getAttachmentName2() {
		return attachmentName2;
	}

	public void setAttachmentName2(String attachmentName2) {
		this.attachmentName2 = attachmentName2;
	}

	public String getAttachmentName3() {
		return attachmentName3;
	}

	public void setAttachmentName3(String attachmentName3) {
		this.attachmentName3 = attachmentName3;
	}

	public String getAttachmentName4() {
		return attachmentName4;
	}

	public void setAttachmentName4(String attachmentName4) {
		this.attachmentName4 = attachmentName4;
	}
}
