/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.sygel.tender.domain.util.Loten;
import com.sygel.tender.domain.util.Util;

/**
 * Tender
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Tender")
@NamedNativeQueries({
	@NamedNativeQuery(name = "Tender.findByQueryNoAI1", query = "SELECT * "
			  + " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplines,location,cpv,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and not publication is null order by publication desc", resultClass = Tender.class),
		@NamedNativeQuery(name = "Tender.findByQuery1", query = "SELECT * "
				+ " FROM Tender WHERE (not title1 is null) AND MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) order by publication desc", resultClass = Tender.class),
		@NamedNativeQuery(name = "Tender.findByQueryChangeDate1", query = "SELECT * "
						+ " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and changeDate > :changeDate order by publication desc", resultClass = Tender.class),
		@NamedNativeQuery(name = "Tender.findByQueryExactCategory1", query = "SELECT * "
						+ " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvNoParent,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) order by publication desc", resultClass = Tender.class),
		@NamedNativeQuery(name = "Tender.findByQueryWithAward", query = "SELECT * "
				+ " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and not award is null order by publication desc", resultClass = Tender.class)
	
})
@NamedQueries({ 
  	@NamedQuery(name = "Tender.findByPublicationDate", query = "SELECT t FROM Tender t WHERE t.publication >= :fromPublication AND t.publication <= :toPublication order by t.publication"),
	  @NamedQuery(name = "Tender.findByNoticeIds", query = "SELECT t FROM Tender t WHERE t.noticeIds LIKE :noticeId"),
	  @NamedQuery(name = "Tender.findByNoticeId", query = "SELECT t FROM Tender t WHERE t.noticeId = :noticeId"),
	  @NamedQuery(name = "Tender.findByEreference", query = "SELECT t FROM Tender t WHERE t.ereference = :ereference"),
		@NamedQuery(name = "Tender.findAll", query = "SELECT t FROM Tender t order by openingDate desc"),
		@NamedQuery(name = "Tender.findAll1", query = "SELECT t FROM Tender t WHERE (not title1 is null) order by openingDate desc"),
		@NamedQuery(name = "Tender.findAllOrderPublication", query = "SELECT t FROM Tender t order by publication desc"),
		@NamedQuery(name = "Tender.findAllOrderPublication1", query = "SELECT t FROM Tender t WHERE (not title1 is null) order by publication desc"),
		})
@NamedEntityGraphs({
		@NamedEntityGraph(name = "Tender.attachments", attributeNodes = { @NamedAttributeNode("reverseIndex1"),@NamedAttributeNode("reverseIndex2"),@NamedAttributeNode("reverseIndex3"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("id"), @NamedAttributeNode("bda"), @NamedAttributeNode("noticeId"), @NamedAttributeNode("noticeIds"),
				@NamedAttributeNode("year"), @NamedAttributeNode("cpv"), @NamedAttributeNode("cpvNoParent"), @NamedAttributeNode("nuts"), @NamedAttributeNode("nutsNoParent"), @NamedAttributeNode("location"),
				@NamedAttributeNode("form"), @NamedAttributeNode("ereference"), @NamedAttributeNode("type"), @NamedAttributeNode("openingDate"), @NamedAttributeNode("publication"),
				@NamedAttributeNode("changeDate"), @NamedAttributeNode("features1"), @NamedAttributeNode("features2"), @NamedAttributeNode("features3"), @NamedAttributeNode("features4"),
				@NamedAttributeNode("essence1"), @NamedAttributeNode("essence2"), @NamedAttributeNode("essence3"), @NamedAttributeNode("essence4"), @NamedAttributeNode("lot1"), @NamedAttributeNode("lot2"),
				@NamedAttributeNode("lot3"), @NamedAttributeNode("lot4"), @NamedAttributeNode("title1"), @NamedAttributeNode("title2"), @NamedAttributeNode("title3"), @NamedAttributeNode("title4"),
				//@NamedAttributeNode("value1"), @NamedAttributeNode("value2"), @NamedAttributeNode("value3"), @NamedAttributeNode("value4"),
				//@NamedAttributeNode("specification1"),@NamedAttributeNode("specification2"), @NamedAttributeNode("specification3"), @NamedAttributeNode("specification4"), 
				@NamedAttributeNode("category"), @NamedAttributeNode("categoryAI"),
				@NamedAttributeNode("disciplines"),

				@NamedAttributeNode("preInformation"),@NamedAttributeNode("rectifications"),@NamedAttributeNode("attachments"),@NamedAttributeNode("award") }),
		@NamedEntityGraph(name = "Tender.award", attributeNodes = { @NamedAttributeNode("reverseIndex1"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("id"), @NamedAttributeNode("bda"), @NamedAttributeNode("noticeId"), @NamedAttributeNode("noticeIds"),
				@NamedAttributeNode("year"), @NamedAttributeNode("cpv"), @NamedAttributeNode("cpvNoParent"), @NamedAttributeNode("nuts"), @NamedAttributeNode("nutsNoParent"), @NamedAttributeNode("location"),
				@NamedAttributeNode("form"), @NamedAttributeNode("ereference"), @NamedAttributeNode("type"), @NamedAttributeNode("openingDate"), @NamedAttributeNode("publication"),
				@NamedAttributeNode("changeDate"), @NamedAttributeNode("features1"), @NamedAttributeNode("features2"), @NamedAttributeNode("features3"), @NamedAttributeNode("features4"),
				@NamedAttributeNode("essence1"), @NamedAttributeNode("essence2"), @NamedAttributeNode("essence3"), @NamedAttributeNode("essence4"), @NamedAttributeNode("lot1"), @NamedAttributeNode("lot2"),
				@NamedAttributeNode("lot3"), @NamedAttributeNode("lot4"), @NamedAttributeNode("title1"), @NamedAttributeNode("title2"), @NamedAttributeNode("title3"), @NamedAttributeNode("title4"),
				//@NamedAttributeNode("value1"), @NamedAttributeNode("value2"), @NamedAttributeNode("value3"), @NamedAttributeNode("value4"), 
				//@NamedAttributeNode("specification1"), @NamedAttributeNode("specification2"), @NamedAttributeNode("specification3"), @NamedAttributeNode("specification4"), 
				@NamedAttributeNode("category"), @NamedAttributeNode("categoryAI"),
				@NamedAttributeNode("disciplines"),

				@NamedAttributeNode("award") }) })
public class Tender {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@Lob
	String bda;
	@Size(max = 10)
	String noticeId;
	@Lob
	String noticeIds;
	Integer year;
	@Lob
	String cpv;

	@Lob
	String cpvNoParent;
	@Lob
	String cpvAI;
	@Lob
	String cpvNoParentAI;
	@Lob
	String cpvPureAI;
	@Lob
	String nuts;
	@Lob
	String nutsNoParent;
	@Lob
	String location;
	@Size(max = 4)
	String form;
	@Size(max = 11)
	String governmentType;
	@Lob
	String ereference;
	@Size(max = 20)
	String type;
	

	Date openingDate;
	Date publication;
	Date changeDate;
	
	@Lob
	String features1;
	@Lob
	String features2;
	@Lob
	String features3;
	@Lob
	String features4;

	@Lob
	String essence1;
	@Lob
	String essence2;
	@Lob
	String essence3;
	@Lob
	String essence4;

	@Lob
	String lot1;
	@Lob
	String lot2;
	@Lob
	String lot3;
	@Lob
	String lot4;

	@Lob
	String title1;
	@Lob
	String title2;
	@Lob
	String title3;
	@Lob
	String title4;

	@Size(max = 5)
	String country;
	

	@Lob
	String category;
	@Lob
	String categoryAI;
	@Lob
	String disciplines;
	@Lob
	String disciplinesCpvAI;

	@Lob
	private String reverseIndex1;
	@Lob
	private String reverseIndex2;	
	@Lob
	private String reverseIndex3;	
	@Lob
	private String reverseIndex4;	
	
	@OneToOne
	@JoinColumn(name = "award", referencedColumnName = "id")
	private Award award;

	@OneToOne
	@JoinColumn(name = "information", referencedColumnName = "id")
	private Information information;

	@OneToOne
	@JoinColumn(name = "preInformation", referencedColumnName = "id")
	private PreInformation preInformation;

	@OneToMany(mappedBy = "tender")
	private Set<Rectification> rectifications = new HashSet<>();

	@OneToMany(mappedBy = "tender")
	private Set<Attachment> attachments = new HashSet<>();

	@ManyToOne
	@JoinColumn(name = "city", referencedColumnName = "id")
	private City city;

	@ManyToOne
	@JoinColumn(name = "government", referencedColumnName = "id")
	private Government government;

	@Transient
	public String language = "NL";
	
	@Transient
	public String locationTranslation  = "";
	
	@Transient
	public String disciplineTranslation  = "";
	
	@Transient
	public String categoryTranslation  = "";
	
	@Transient
	public String description  = "";

	public static final String[] LANGUAGE_BE = { "NL", "FR", "DE", "EN" };

	public Long getId() {
		return id;
	}
	
	@PreUpdate
	@PrePersist
	public void updateReverseIndex() {		
		String bdaIndex = "";
		if (bda != null) {
			String[] bdaArray = bda.trim().split(" ");
			for (String b : bdaArray)
			  bdaIndex += " " + Util.BDA_PREFIX+b.replace("-", Util.HYPHEN_ESCAPE);	
		}
		reverseIndex1 = Util.constructDBIndex(title1 + " " +essence1)+bdaIndex;
		reverseIndex2 = Util.constructDBIndex(title2 + " " +essence2)+bdaIndex;
	}
	
	public void setTitle(String language, String title) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				title1 = title;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				title2 = title;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				title3 = title;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				title4 = title;
		}
	}

	public String getTitle(String language) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				return title1;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				return title2;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				return title3;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				return title4;
		}
		return null;
	}

	public void setEssence(String language, String essence) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				essence1 = essence;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				essence2 = essence;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				essence3 = essence;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				essence4 = essence;
		}
	}

	public void setLot(String language, String lot) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				lot1 = lot;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				lot2 = lot;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				lot3 = lot;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				lot4 = lot;
		}
	}

	public String getLot(String language) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				return lot1;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				return lot2;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				return lot3;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				return lot4;
		}
		return null;
	}

	public String getLot() {
		return getLot(language);
	}

	public String getEssence(String language) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				return essence1;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				return essence2;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				return essence3;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				return essence4;
		}
		return null;
	}

	public String getEssence() {
		String t =  getEssence(language);
		if (t == null) {
			if (essence1 != null && essence1.length() > 0)
				t = essence1;
			else if (essence2 != null && essence2.length() > 0)
				t = essence2;
			else if (essence3 != null && essence3.length() > 0)
				t = essence3;
			else if (essence4 != null && essence4.length() > 0)
				t = essence4;
		}
		return t;
	}
	
	public void setFeatures(String language, String features) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				features1 = features;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				features2 = features;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				features3 = features;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				features4 = features;
		}
	}

	public String getFeatures(String language) {
		for (int i = 0; i < LANGUAGE_BE.length; i++) {
			if (i == 0 && LANGUAGE_BE[i].equals(language))
				return features1;
			else if (i == 1 && LANGUAGE_BE[i].equals(language))
				return features2;
			else if (i == 2 && LANGUAGE_BE[i].equals(language))
				return features3;
			else if (i == 3 && LANGUAGE_BE[i].equals(language))
				return features4;
		}
		return null;
	}

	public String getFeatures() {
		return getFeatures(language);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBda() {
		return bda;
	}

	public void setBda(String bda) {
		this.bda = bda;
	}
	
	public void addBda(String bdas) {
		String[] bdaArray = bdas.split(" ");
		for (String b:bdaArray) {
			b = b.trim();
			if (this.bda == null) this.bda= b;
			else if (this.bda.indexOf(b) == -1) this.bda = this.bda + " " + b; 
 		}		
	}

	public String getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}

	public String getNoticeIds() {
		return noticeIds;
	}

	public void setNoticeIds(String noticeIds) {
		this.noticeIds = noticeIds;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		this.cpv = cpv;
	}

	public String getNuts() {
		return nuts;
	}

	public void setNuts(String nuts) {
		this.nuts = nuts;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getEreference() {
		return ereference;
	}

	public void setEreference(String ereference) {
		this.ereference = ereference;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public Date getPublication() {
		return publication;
	}

	public void setPublication(Date publication) {
		this.publication = publication;
	}

	public String getEssence1() {
		return essence1;
	}

	public void setEssence1(String essence1) {
		this.essence1 = essence1;
	}

	public String getTitle() {
		String t = getTitle(language);
		if (t == null) {
			if (title1 != null && title1.length() > 0)
				t = title1;
			else if (title2 != null && title2.length() > 0)
				t = title2;
			else if (title3 != null && title3.length() > 0)
				t = title3;
			else if (title4 != null && title4.length() > 0)
				t = title4;
		}
		return t;
	}

	public void setTitle(String title1) {
		this.title1 = title1;
	}
/*
	public String getValue1() {
		return value1;
	}

	public void setValue1(String value) {
		this.value1 = value;
	}

	public String getSpecification1() {
		return specification1;
	}

	public void setSpecification1(String specification) {
		this.specification1 = specification;
	}
	*/

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryAI() {
		return categoryAI;
	}

	public void setCategoryAI(String categoryAI) {
		this.categoryAI = categoryAI;
	}

	public String getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(String disciplines) {
		this.disciplines = disciplines;
	}

	public Award getAward() {
		return award;
	}

	public void setAward(Award award) {
		this.award = award;
	}

	public Set<Rectification> getRectifications() {
		return rectifications;
	}
	
	public List<Rectification> getRectificationsSorted() {
		List<Rectification> sorted = new ArrayList<>();
		sorted.addAll(rectifications);
	  Collections.sort(sorted);
	  return sorted;
	}

	public void setRectifications(Set<Rectification> rectifications) {
		this.rectifications = rectifications;
	}

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Information getInformation() {
		return information;
	}

	public void setInformation(Information information) {
		this.information = information;
	}

	public PreInformation getPreInformation() {
		return preInformation;
	}

	public void setPreInformation(PreInformation preInformation) {
		this.preInformation = preInformation;
	}

	public String getEssence2() {
		return essence2;
	}

	public void setEssence2(String essence2) {
		this.essence2 = essence2;
	}

	public String getEssence3() {
		return essence3;
	}

	public void setEssence3(String essence3) {
		this.essence3 = essence3;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getTitle3() {
		return title3;
	}

	public void setTitle3(String title3) {
		this.title3 = title3;
	}
/*
	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		this.value3 = value3;
	}

	public String getSpecification2() {
		return specification2;
	}

	public void setSpecification2(String specification2) {
		this.specification2 = specification2;
	}

	public String getSpecification3() {
		return specification3;
	}

	public void setSpecification3(String specification3) {
		this.specification3 = specification3;
	}
	*/

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFeatures1() {
		return features1;
	}

	public void setFeatures1(String features1) {
		this.features1 = features1;
	}

	public String getFeatures2() {
		return features2;
	}

	public void setFeatures2(String features2) {
		this.features2 = features2;
	}

	public String getFeatures3() {
		return features3;
	}

	public void setFeatures3(String features3) {
		this.features3 = features3;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getCpvNoParent() {
		return cpvNoParent;
	}

	public void setCpvNoParent(String cpvNoParent) {
		this.cpvNoParent = cpvNoParent;
	}

	public String getNutsNoParent() {
		return nutsNoParent;
	}

	public void setNutsNoParent(String nutsNoParent) {
		this.nutsNoParent = nutsNoParent;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Transient
	public String getLanguage() {
		return language;
	}

	@Transient
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFeatures4() {
		return features4;
	}

	public void setFeatures4(String features4) {
		this.features4 = features4;
	}

	public String getEssence4() {
		return essence4;
	}

	public void setEssence4(String essence4) {
		this.essence4 = essence4;
	}

	public String getTitle4() {
		return title4;
	}

	public void setTitle4(String title4) {
		this.title4 = title4;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
/*
	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getSpecification4() {
		return specification4;
	}

	public void setSpecification4(String specification4) {
		this.specification4 = specification4;
	}
*/
	public Government getGovernment() {
		return government;
	}

	public void setGovernment(Government government) {
		this.government = government;
	}

	public String getGovernmentType() {
		return governmentType;
	}

	public void setGovernmentType(String governmentType) {
		this.governmentType = governmentType;
	}

	public String getLot1() {
		return lot1;
	}

	public void setLot1(String lot1) {
		this.lot1 = lot1;
	}

	public String getLot2() {
		return lot2;
	}

	public void setLot2(String lot2) {
		this.lot2 = lot2;
	}

	public String getLot3() {
		return lot3;
	}

	public void setLot3(String lot3) {
		this.lot3 = lot3;
	}

	public String getLot4() {
		return lot4;
	}

	public void setLot4(String lot4) {
		this.lot4 = lot4;
	}

	public Loten getLoten() {
		Loten loten = null;
		String l = getLot(language);
		if (l != null && l.length() > 0) {
			StringReader sr = new StringReader(l);
			JAXBContext jaxbContext;
			try {
				jaxbContext = JAXBContext.newInstance(Loten.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				loten = (Loten) jaxbUnmarshaller.unmarshal(sr);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		}
		return loten;
	}

	public String getCpvAI() {
		return cpvAI;
	}

	public void setCpvAI(String cpvAI) {
		this.cpvAI = cpvAI;
	}

	public String getCpvNoParentAI() {
		return cpvNoParentAI;
	}

	public void setCpvNoParentAI(String cpvNoParentAI) {
		this.cpvNoParentAI = cpvNoParentAI;
	}

	public String getDisciplinesCpvAI() {
		return disciplinesCpvAI;
	}

	public void setDisciplinesCpvAI(String disciplinesCpvAI) {
		this.disciplinesCpvAI = disciplinesCpvAI;
	}

	public String getCpvPureAI() {
		return cpvPureAI;
	}		

	@Transient
	public String getCpvOnlyAI() {
		StringBuilder cpvOnlyAI = new StringBuilder();
		if (cpvPureAI == null || cpvPureAI.length() < 2) return "";
		String[] cs = cpvPureAI.split(" ");
		for (String c : cs) {
			if (getCpvNoParent().indexOf(c) == -1) cpvOnlyAI.append(" "+c);
		}
		return cpvOnlyAI.toString().trim();
	}

	public void setCpvPureAI(String cpvPureAI) {
		this.cpvPureAI = cpvPureAI;
	}
	
	@Transient
  public String getDisciplinesString() {
		if (this.getDisciplinesCpvAI() == null || this.getDisciplinesCpvAI().length() < 2) return "";
		StringBuilder db = new StringBuilder();
		String[] ds = this.getDisciplinesCpvAI().split("\\:");
		for (String d : ds) {
			if (d.length()>1)
			  db.append(" "+d.substring(2));
		}
		return db.toString().trim();
	}
	
	@Transient
	public String getCategoryString() {
		if (this.getCategoryAI() == null || this.getCategoryAI().length() < 2) return "";
		StringBuilder db = new StringBuilder();
		String[] ds = this.getCategoryAI().split("\\:");
		for (String d : ds) {
			if (d.startsWith(Util.CATEGORY_PREFIX))
			  db.append(" "+d.substring(3));
			if (d.startsWith(Util.CLASS_PREFIX))
			  db.append(" kl"+d.substring(3));
		}
		return db.toString().trim();
	}
	
	@Transient
	public String getLocationTranslation() {
		return locationTranslation;
	}

	public void setLocationTranslation(String locationTranslation) {
		this.locationTranslation = locationTranslation;
	}

	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Transient
	public String getDisciplineTranslation() {
		return disciplineTranslation;
	}

	public void setDisciplineTranslation(String disciplineTranslation) {
		this.disciplineTranslation = disciplineTranslation;
	}
	
	@Transient
	public String getCategoryTranslation() {
		return categoryTranslation;
	}

	public void setCategoryTranslation(String categoryTranslation) {
		this.categoryTranslation = categoryTranslation;
	}
	
	@Transient
	public String getBdaMaster() {
		String bdaMaster = bda;
		if (bdaMaster != null) {
			int end = bdaMaster.indexOf(" ");
			if (end != -1) bdaMaster = bdaMaster.substring(0,end);
		}
		return bdaMaster;
	}
}
