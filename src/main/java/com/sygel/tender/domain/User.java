/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sygel.tender.domain.util.PasswordHash;
import com.sygel.tender.domain.util.Util;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "User")

@NamedNativeQueries({
	@NamedNativeQuery(name = "User.findByUserFilter", query = "SELECT * "
			+ " FROM User WHERE MATCH (searchIndex) AGAINST (:searchString IN BOOLEAN MODE) order by name asc", resultClass = User.class)

})

@NamedQueries({ 
  @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email LIKE :email"),
	@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
  })

@NamedEntityGraphs({
	@NamedEntityGraph(name = "User.filters", 
			attributeNodes = { 
			@NamedAttributeNode("id"),
			@NamedAttributeNode("email"),
			@NamedAttributeNode("passwordHash"),
			@NamedAttributeNode("name"),
			@NamedAttributeNode("firstname"),
			@NamedAttributeNode("company"),
			@NamedAttributeNode("vat"),
			@NamedAttributeNode("street"),
			@NamedAttributeNode("zipcode"),
			@NamedAttributeNode("city"),
			@NamedAttributeNode("country"),
			@NamedAttributeNode("invoiceStreet"),
			@NamedAttributeNode("invoiceZipcode"),
			@NamedAttributeNode("invoiceCity"),
			@NamedAttributeNode("invoiceCountry"),
			@NamedAttributeNode("subscriptionType"),
			@NamedAttributeNode("subscriptionStatus"),
			@NamedAttributeNode("subscriptionStart"),
			@NamedAttributeNode("subscriptionEnd"),
			@NamedAttributeNode("lastWebLogin"),
			@NamedAttributeNode("lastPushMail"),
			@NamedAttributeNode("filters")		
	})
 })

public class User implements Serializable {
  public static final String ROLE_USER = "user";
  public static final String ROLE_ADMIN = "admin";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	//@Email
	String email;
	String passwordHash;
	
	String name;
	String firstname;
	
	String company;
	String vat;
	String street;
	String zipcode;
	String city;
	String country;
	
	String invoiceStreet;
	String invoiceZipcode;
	String invoiceCity;
	String invoiceCountry;
	
	String roles;
	
	@Lob
	String searchIndex;
	
	@Enumerated(EnumType.ORDINAL)
	SubscriptionType subscriptionType = SubscriptionType.FREE;				
	public enum SubscriptionType {FREE,PAID}

	@Enumerated(EnumType.ORDINAL)
	SubscriptionStatus subscriptionStatus = SubscriptionStatus.OK;				
	public enum SubscriptionStatus {OK,STOP}
	
	Date subscriptionStart;
	Date subscriptionEnd;
	
	Date lastWebLogin;
	Date lastPushMail;
		
	@OneToMany(cascade = {CascadeType.REMOVE},mappedBy = "user")
	private Set<Filter> filters = new HashSet<>();
	
	@Transient
	String newPassword;
	
	@PreUpdate
	@PrePersist
	public void updateSearchIndex() {	
		searchIndex = Util.constructDBIndex(name + " " + firstname+ " "+ street + " " +email+ " "+Util.LOCATION_PREFIX +zipcode+" "+city+" "+country+" "+invoiceStreet+ " "+Util.LOCATION_PREFIX+invoiceZipcode+" "+invoiceCity+ " "+invoiceCountry);		
	}
	
	public void storePassword(String password) {		
		try {
			passwordHash = PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			passwordHash = null;
		}
	}
	
	public boolean checkPassword(String password) {
		
		return PasswordHash.validatePassword(password, passwordHash);

	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getVat() {
		return vat;
	}
	public void setVat(String vat) {
		this.vat = vat;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getInvoiceStreet() {
		return invoiceStreet;
	}
	public void setInvoiceStreet(String invoiceStreet) {
		this.invoiceStreet = invoiceStreet;
	}
	public String getInvoiceZipcode() {
		return invoiceZipcode;
	}
	public void setInvoiceZipcode(String invoiceZipcode) {
		this.invoiceZipcode = invoiceZipcode;
	}
	public String getInvoiceCity() {
		return invoiceCity;
	}
	public void setInvoiceCity(String invoiceCity) {
		this.invoiceCity = invoiceCity;
	}
	public String getInvoiceCountry() {
		return invoiceCountry;
	}
	public void setInvoiceCountry(String invoiceCountry) {
		this.invoiceCountry = invoiceCountry;
	}
	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public SubscriptionStatus getSubscriptionStatus() {
		return subscriptionStatus;
	}
	public void setSubscriptionStatus(SubscriptionStatus subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}
	public Date getSubscriptionStart() {
		return subscriptionStart;
	}
	public void setSubscriptionStart(Date subscriptionStart) {
		this.subscriptionStart = subscriptionStart;
	}
	public Date getSubscriptionEnd() {
		return subscriptionEnd;
	}
	public void setSubscriptionEnd(Date subscriptionEnd) {
		this.subscriptionEnd = subscriptionEnd;
	}
	public Date getLastWebLogin() {
		return lastWebLogin;
	}
	public void setLastWebLogin(Date lastWebLogin) {
		this.lastWebLogin = lastWebLogin;
	}
	public Date getLastPushMail() {
		return lastPushMail;
	}
	public void setLastPushMail(Date lastPushMail) {
		this.lastPushMail = lastPushMail;
	}

	public Set<Filter> getFilters() {
		return filters;
	}

	public void setFilters(Set<Filter> filters) {
		this.filters = filters;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}
  @Transient
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public boolean isUserInRole(String role) {
		if (role.equals(User.ROLE_ADMIN) && roles.indexOf(":a:") != -1) return true;
		if (role.equals(User.ROLE_USER) && roles.indexOf(":u:") != -1) return true;
		return false;
	}
	
}
