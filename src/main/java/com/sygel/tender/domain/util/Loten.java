/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain.util;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@XmlRootElement(name="x")
public class Loten {
	
  private List<Lot> lotList = null;

	public List<Lot> getLotList() {
		return lotList;
	}
	@XmlElement(name="s")
	public void setLotList(List<Lot> lotList) {
		this.lotList = lotList;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Lot lot : lotList) 
			sb.append(lot.toString());
		return sb.toString();
	}
}
