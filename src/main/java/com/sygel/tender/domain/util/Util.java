/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility functions for the DB full test engine.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Util {
	public static final String REVERSE_PREFIX = "rrr";
	static public final String DISCIPLINE_PREFIX = "xx";
	static public final String BDA_PREFIX = "bb";
	static public final String CATEGORY_PREFIX = "yyy";
	static public final String CLASS_PREFIX = "zzz";
	static public final String LOCATION_PREFIX = "l";
	static public final String TYPE_PREFIX = "t";
	static public final String ACTIVITY_PREFIX = "aa";
	
	static public final String AT_ESCAPE = "QQX1A1XQQ";
	static public final String DOT_ESCAPE = "QQX1B1XQQ";
	static public final String UNDERSCORE_ESCAPE = "QQX1C1XQQ";
	static public final String SLASH_ESCAPE = "QQX1D1XQQ";
	static public final String BACKSLASH_ESCAPE = "QQX1E1XQQ";
	static public final String HYPHEN_ESCAPE = "QQX1F1XQQ";
	static public final String SPACE_ESCAPE = "QQX1G1XQQ";
	static public final String PLUS_ESCAPE = "QQX1H1XQQ";

	public static String[] constructWords(String text) {
		if (text == null)
			return null;
		return text.split("[\\s,\\.;]");
	}

	public static String constructDBIndex(String text) {
		if (text == null)
			return null;
		text = text.replace("\u00a0"," "); // remove &nbsp; char
		text = escapeEmails(text);
		text = escapePhoneNumbers(text);
		text = text.replaceAll("   ", " ");
		text = text.replaceAll("  ", " ");
		StringBuilder builder = new StringBuilder();
		String[] words = constructWords(text);
		for (String word : words) {
			if (!"".equals(word)) {
			  word = word.replace("-", HYPHEN_ESCAPE);
			  if (builder.indexOf(word+" ") == -1) builder.append(word+" ");
			}
		}
		for (String word : words) {
			if (!"".equals(word)) {
			  word = word.replace("-", HYPHEN_ESCAPE);
			  if (builder.indexOf(REVERSE_PREFIX+reverseString(word)+" ") == -1) builder.append(REVERSE_PREFIX+reverseString(word)+" ");
			}
		}
		return builder.toString().trim();
	}

	private static String escapeEmails(String text) {
		List<String> emails = getEmails(text);
		for (String email : emails) {
			String escapedEmail = email.replace("@", AT_ESCAPE).replace(".", DOT_ESCAPE).replace("-", HYPHEN_ESCAPE);			
			text = text.replace(email, escapedEmail);
		}
		return text;
	}
	
	private static String escapePhoneNumbers(String text) {
		List<String> phones = getPhoneNumbers(text);
		for (String phone : phones) {
			String escapedPhone = phone.replace(" ", SPACE_ESCAPE).replace("+", PLUS_ESCAPE);
			text = text.replace(phone, escapedPhone);
		}
		return text;
	}

	public static String reverseString(String string) {
		return new StringBuilder(string).reverse().toString();
	}

	public static List<String> getEmails(String line) {
		final String RE_MAIL = "([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})";
		Pattern p = Pattern.compile(RE_MAIL);
		Matcher m = p.matcher(line);
		List<String> emailList = new ArrayList<>();
		while (m.find()) {
			if (!emailList.contains(m.group(1))) {
				emailList.add(m.group(1));
			}
		}
		return emailList;
	}
	
	public static List<String> getPhoneNumbers(String line) {
		// eg +32 476722099
		final String RE_PHONE = "\\+\\d{1,2} \\d{8,9}";
		Pattern p = Pattern.compile(RE_PHONE);
		Matcher m = p.matcher(line);
		List<String> phoneNumberList = new ArrayList<>();
		while (m.find()) {
			if (!phoneNumberList.contains(m.group(0))) {
				phoneNumberList.add(m.group(0));
			}
		}
		return phoneNumberList;
	}
}
