/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain.util;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Category {
  String code;
  String name;
  int occurance = 1;
  
public Category(String code, String name,int occurance) {
	super();
	this.code = code;
	this.name = name;
	this.occurance = occurance;
}
public String getCode() {
	return code;
}
public void setCode(String code) {
	this.code = code;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getOccurance() {
	return occurance;
}
public void addOccurance() {
	occurance++;
}
public void setOccurance(int occurance) {
	this.occurance = occurance;
}
 public int hashCode() {
	 return code.hashCode();
 }
 public boolean equals(Object other) {	 
	 if (other == null) return false;
	 return code.equals(((Category)other).code);
 }
}
