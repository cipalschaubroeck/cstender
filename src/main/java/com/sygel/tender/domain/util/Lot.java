/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain.util;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@XmlRootElement(name="l")
public class Lot {
  String number;
  String title;
  String essence;
  
	public String getNumber() {
		return number;
	}
	@XmlElement(name="n")
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTitle() {
		return title;
	}
	@XmlElement(name="t")
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEssence() {
		return essence;
	}
	@XmlElement(name="e")
	public void setEssence(String essence) {
		this.essence = essence;
	}
  
	public String toString() {
		return number + " " + title + " " + essence;
	}
  
}
