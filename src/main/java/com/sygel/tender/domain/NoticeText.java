/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "NoticeText")
public class NoticeText {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
    @Size(max = 4)
	String language;
    @Lob
	String text;
	Date publication;
	@Size(max = 4)
	String form;	

	@ManyToOne
	@JoinColumn(name="award", referencedColumnName="id")
	private Award award;
	
	@ManyToOne
	@JoinColumn(name="information", referencedColumnName="id")
	private Information information;
	
	@ManyToOne
	@JoinColumn(name="preInformation", referencedColumnName="id")
	private PreInformation preInformation;
	
	@ManyToOne
	@JoinColumn(name="rectification", referencedColumnName="id")
	private Rectification rectification;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getPublication() {
		return publication;
	}
	public void setPublication(Date publication) {
		this.publication = publication;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public Information getInformation() {
		return information;
	}
	public void setInformation(Information information) {
		this.information = information;
	}
	public Award getAward() {
		return award;
	}
	public void setAward(Award award) {
		this.award = award;
	}
	public Rectification getRectification() {
		return rectification;
	}
	public void setRectification(Rectification rectification) {
		this.rectification = rectification;
	}
	public PreInformation getPreInformation() {
		return preInformation;
	}
	public void setPreInformation(PreInformation preInformation) {
		this.preInformation = preInformation;
	}
	
}
