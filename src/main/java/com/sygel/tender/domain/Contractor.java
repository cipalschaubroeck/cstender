/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.sygel.tender.domain.util.Util;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Contractor")
@NamedNativeQueries({
		@NamedNativeQuery(name = "Contractor.findByQuery1", query = "SELECT * "
				+ " FROM Contractor WHERE MATCH (reverseIndex) AGAINST (:searchString IN BOOLEAN MODE) order by name asc", resultClass = Contractor.class)

})
@NamedQueries({
    @NamedQuery(name = "Contractor.findByNameLocation", query = "SELECT c FROM Contractor c WHERE UPPER(c.name) = :name AND c.location = :location"),    
    @NamedQuery(name = "Contractor.findAll", query = "SELECT c FROM Contractor c")    
})

@NamedEntityGraphs({
	@NamedEntityGraph(name = "Contractor.awardItems", attributeNodes = { 
			@NamedAttributeNode("id"), 
			@NamedAttributeNode("name"), 
			@NamedAttributeNode("type"), 
			@NamedAttributeNode("street"), 
			@NamedAttributeNode("location"), 
			@NamedAttributeNode("emails"), 
			@NamedAttributeNode("phones"), 
			@NamedAttributeNode("faxes"),
			@NamedAttributeNode("awardItems")
			}),
 })

public class Contractor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@OneToMany(mappedBy = "contractor")
	private Collection<AwardItem> awardItems;	
	
	@Size(max = 255)
	private String name;
	
	@Size(max = 10)
	private String type;
	
	@Size(max = 255)
	private String street;
	
	@Size(max = 255)
	private String location;
	
	@Size(max = 255)
	private String emails;
	
	@Size(max = 255)
	private String phones;
	
	@Size(max = 255)
	private String faxes;
	
	@Lob
	private String reverseIndex;
	
	@Transient
	private String locationString;

	@PreUpdate
	@PrePersist
	public void updateReverseIndex() {		
		reverseIndex = Util.constructDBIndex(name + " " + street + " "+emails+" "+phones+ " "+faxes)+" "+location;		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<AwardItem> getAwardItems() {
		return awardItems;
	}

	public void setAwardItems(Collection<AwardItem> awardItems) {
		this.awardItems = awardItems;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null && name.length() > 254) name = name.substring(0,254);
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		if (street != null && street.length() > 254) street = street.substring(0,254);
		this.street = street;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if (location != null && location.length() > 254) location = location.substring(0,254);
		this.location = location;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		while (emails.length() > 254) {
			emails = emails.substring(emails.indexOf(";")+1);
		}
		this.emails = emails;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		if (phones != null && phones.length() > 254) phones = phones.substring(0,254);
		this.phones = phones;
	}

	public String getFaxes() {
		return faxes;
	}

	public void setFaxes(String faxes) {
		if (faxes != null && faxes.length() > 254) faxes = faxes.substring(0,254);
		this.faxes = faxes;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocationString() {
		return locationString;
	}

	public void setLocationString(String locationString) {
		this.locationString = locationString;
	}	
	
}
