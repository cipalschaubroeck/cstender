/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.InheritanceType;

import com.sygel.tender.domain.User.SubscriptionType;

@Entity
@Table(name = "Filter")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "FILTER_TYPE")
/**
 * Filter for queries
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public abstract class Filter implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;	

	String name = "";	
	String searchString = "";
	Boolean sendEmail = false;
	Date lastPushMailFrom;
	Date lastPushMailTo;
	
	@Transient
	boolean selected = false;

	@Transient
	String description = null;
		
	@ManyToOne
	@JoinColumn(name="user", referencedColumnName="id")
	private User user;

	public Filter() {
	}
	
	public boolean equals(Object other) {
		boolean equal = false;
		if (other != null && other instanceof Filter) {
			Filter otherFilter = (Filter) other;
			if (otherFilter.getName().equals(getName()) 
					&& otherFilter.getSearchString().equals(getSearchString())
					&& otherFilter.isSendEmail().equals(isSendEmail())
					) equal = true;
		}
		return equal;
	}
	
	public Filter(String name, String searchString) {
		this.name = name;
		this.searchString = searchString;
	}
	
	public void init(Filter filter) {
		id = filter.getId();
		name = filter.getName();
		searchString = filter.getSearchString();
		sendEmail = filter.isSendEmail();
		lastPushMailFrom = filter.getLastPushMailFrom();
		lastPushMailTo = filter.getLastPushMailTo();
		user = filter.getUser();
	}
	
	/**
	 * 
	 * @param searchItem
	 * @return true if the searchString contains the word searchItem (case insensitive) 
	 */
	public boolean containsSearchItem(String searchItem) {
		String tmp = (" "+searchString+" ").toLowerCase();
		if (tmp.indexOf(" "+searchItem.toLowerCase()+" ")!= -1) return true;
		return false;
	}
	
	/**
	 * Removes the searchItem form the searchString
	 * @param searchItem
	 * @return true is something is actually removed from the searchString
	 */
	public boolean removeSearchItem(String searchItem) {
		String tmp = (" "+searchString+" ").toLowerCase();
		String tmp2 = (" "+searchString+" ");
		int pos = tmp.indexOf(" "+searchItem.toLowerCase()+" ");
		if (pos != -1) {
			searchString = (tmp2.substring(0,pos)+tmp2.substring(pos+searchItem.length()+2)).trim();   
			return true;
		}
		return false;
	}
	/**
	 * Add a searchItem to the filter.
	 * @param searchItem
	 */
	public void addSearchItem(String searchItem) {
		searchString += " "+searchItem;
		searchString = searchString.trim();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	
	public Date getLastPushMailFrom() {
		return lastPushMailFrom;
	}

	public void setLastPushMailFrom(Date lastPushMail) {
		this.lastPushMailFrom = lastPushMail;
	}

	public Date getLastPushMailTo() {
		return lastPushMailTo;
	}

	public void setLastPushMailTo(Date lastPushMailTo) {
		this.lastPushMailTo = lastPushMailTo;
	}

	public Boolean isSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}	

	@Transient
	public String getDisplayName() {
		if (getDescription() != null && !selected)
		  return getDescription() + " " + getSearchString();
		else if (getName().length() > 0 && !selected)
		  return getName() + ": " + getSearchString();
		else
			return getSearchString();
	}
	
	@Transient
	public String getShortDisplayName() {
		if (getDescription() != null)
		  return getDescription() + " " + getSearchString();
		else if (getName().length() > 0)
		  return getName() + ": " + getSearchString();
		else
			return getSearchString();
	}
	
	@Transient
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
