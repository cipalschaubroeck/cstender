/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.text.DateFormat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedSubgraph;
import javax.persistence.Transient;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@NamedEntityGraphs({
	@NamedEntityGraph(
			name = "MyTenderFilter.filterTenders", 
			includeAllAttributes=true,
			subgraphs = {
			  @NamedSubgraph(
				   name = "filterTenders",
				   type = TenderFilter.class,
				     attributeNodes = {
				       @NamedAttributeNode(value = "id"),
				       @NamedAttributeNode(value = "tender")
				                        }
				                )
				        }

	)
})


@NamedQueries({
	@NamedQuery(name = "MyTenderFilter.findByUserAndFiltername", query = "SELECT f FROM MyTenderFilter f WHERE f.user.id = :userId AND f.name = :filtername"),
})

@Entity
@DiscriminatorValue(value = "MyTenderFilter")
public class MyTenderFilter extends TenderFilter {
	public final static String NAME = "MYTENDERFILTER";
	public MyTenderFilter(String name, String searchString) {
		super(name, searchString);
	}

	public MyTenderFilter() {
		super();
	}
	
	@Transient
	@Override
	public String getDisplayName() {
		return getDescription();
	}
}
