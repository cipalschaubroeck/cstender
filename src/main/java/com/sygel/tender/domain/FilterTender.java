/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@NamedQueries({ 
  @NamedQuery(name = "FilterTender.delete", query = "DELETE FROM FilterTender e WHERE e.filter = :filter"),
  })
@Entity
@Table(name = "FilterTender")
public class FilterTender {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;	
	
	@ManyToOne
	@JoinColumn(name = "tender", referencedColumnName = "id")
  Tender tender;
	
	@ManyToOne
	@JoinColumn(name="filter", referencedColumnName="id")
	private TenderFilter filter;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Tender getTender() {
		return tender;
	}

	public void setTender(Tender tender) {
		this.tender = tender;
	}

	public TenderFilter getTenderFilter() {
		return filter;
	}

	public void setTenderFilter(TenderFilter filter) {
		this.filter = filter;
	}	
}
