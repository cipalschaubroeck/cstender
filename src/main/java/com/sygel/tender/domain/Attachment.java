/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.sygel.tender.domain.util.Util;

/**
 * Tender Attachments
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Attachment")
@NamedNativeQueries({ @NamedNativeQuery(name = "Attachment.findByQuery1", query = "SELECT * FROM Attachment WHERE MATCH (reverseIndex,features,disciplinesAI,location,cpv,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE)", resultClass = Attachment.class) })
public class Attachment {
	@Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="tender", referencedColumnName="id")
  Tender tender;
	
	@Size(max = 255)
	String url;
	
	@Size(max = 255)
	String name;
	
	@Size(max = 255)
	String fileType;
	
	@Lob
	String features;
	
	@Lob
	String text;

	@Lob
	String reverseIndex;
	
  @Size(max = 255)
  String location;
  
  @Size(max = 20)
  String type;
  
  @Size(max = 255)
  String categoryAI;

  @Size(max = 255)
  String disciplinesAI;
  
  @Lob
  String cpv;
  
  @Size(max = 11)
  String governmentType;
  
  @Transient
  String textSnippet;

	@PreUpdate
	@PrePersist
	public void updateReverseIndex() {	
		reverseIndex = Util.constructDBIndex(text);		
	}
  
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Tender getTender() {
		return tender;
	}

	public void setTender(Tender tender) {
		this.tender = tender;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Check if attachment is an F type document or an extra attachment
	 * 
	 * @return true if this is an extra attachment, false F type document
	 */
	public boolean isExtraAttachment() {
		if (getName() == null) return false;
		if (!getName().toLowerCase().endsWith(".pdf")) return true; 
		String name = getName().substring(0,getName().length()-4);
		if (name.endsWith("fr_FR") || name.endsWith("nl_NL")) name = name.substring(0,getName().length()-5);
		if (containsWord(name)) return true;
		return false;
	}
	
	public static boolean containsWord(String word) {
		return word.matches(".*[A-Za-z]{3}.*");
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategoryAI() {
		return categoryAI;
	}

	public void setCategoryAI(String categoryAI) {
		this.categoryAI = categoryAI;
	}

	public String getDisciplinesAI() {
		return disciplinesAI;
	}

	public void setDisciplinesAI(String disciplinesAI) {
		this.disciplinesAI = disciplinesAI;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		this.cpv = cpv;
	}

	public String getGovernmentType() {
		return governmentType;
	}

	public void setGovernmentType(String governmentType) {
		this.governmentType = governmentType;
	}	
	
	public String getTextSnippet() {
		return textSnippet;
	}
	
	public void setTextSnippet(String textSnippet) {
	  this.textSnippet = textSnippet;
	}	
}
