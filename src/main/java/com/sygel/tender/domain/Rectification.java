/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Rectification")
public class Rectification implements Comparable<Rectification> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;	

	@OneToMany(mappedBy = "rectification", cascade = CascadeType.REMOVE, orphanRemoval=true)
	private Collection<NoticeText> rectificationTexts = new Vector<>();

	@ManyToOne
	@JoinColumn(name="tender", referencedColumnName="id")
	private Tender tender;
	@Size(max = 40)
	String attachmentName1;
	@Size(max = 40)
	String attachmentName2;
	@Size(max = 40)
	String attachmentName3;
	@Size(max = 40)
	String attachmentName4;
	
	@Enumerated(EnumType.ORDINAL)
  private Type type = Type.CORRECTION;
	
	Date publicationDate;
			
	public enum Type {CORRECTION,ADDITIONAL_INFO,INCOMPLETE_PROCEDURE}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<NoticeText> getRectificationTexts() {
		return rectificationTexts;
	}

	public void setRectificationTexts(Collection<NoticeText> rectificationTexts) {
		this.rectificationTexts = rectificationTexts;
	}

	public Tender getTender() {
		return tender;
	}

	public void setTender(Tender tender) {
		this.tender = tender;
	}
	public void setAttachmentName(String language, String attachmentName) {
		for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
			if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName1 = attachmentName;
			else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName2 = attachmentName;
			else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName3 = attachmentName;
			else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
				attachmentName4 = attachmentName;
		}
	}

	public String getAttachmentName(String language) {
		for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
			if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName1;
			else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName2;
			else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName3;
			else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
				return attachmentName4;
		}
		return null;
	}

	public String getAttachmentName() {
		String t = getAttachmentName(tender.getLanguage());
		if (t == null) {
			if (attachmentName1 != null && attachmentName1.length() > 0)
				t = attachmentName1;
			else if (attachmentName2 != null && attachmentName2.length() > 0)
				t = attachmentName2;
			else if (attachmentName3 != null && attachmentName3.length() > 0)
				t = attachmentName3;
			else if (attachmentName4 != null && attachmentName4.length() > 0)
				t = attachmentName4;
		}
		return t;
	}	
	
	public String getAttachmentPdfName() {
		String n = getAttachmentName();
		if (n != null && n.endsWith(".xml")) n = n.substring(0,n.length()-3) + "pdf";
		return n;
	}
	
	public String getAttachmentName1() {
		return attachmentName1;
	}

	public void setAttachmentName1(String attachmentName1) {
		this.attachmentName1 = attachmentName1;
	}

	public String getAttachmentName2() {
		return attachmentName2;
	}

	public void setAttachmentName2(String attachmentName2) {
		this.attachmentName2 = attachmentName2;
	}

	public String getAttachmentName3() {
		return attachmentName3;
	}

	public void setAttachmentName3(String attachmentName3) {
		this.attachmentName3 = attachmentName3;
	}

	public String getAttachmentName4() {
		return attachmentName4;
	}

	public void setAttachmentName4(String attachmentName4) {
		this.attachmentName4 = attachmentName4;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	@Override
	public int compareTo(Rectification o) {
		if (o.getPublicationDate() == null && getPublicationDate() == null) return 0;
		if (o.getPublicationDate() != null && getPublicationDate() == null) return -1;
		if (o.getPublicationDate() == null && getPublicationDate() != null) return 1;
		
		return getPublicationDate().compareTo(o.getPublicationDate());
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}	
	
}
