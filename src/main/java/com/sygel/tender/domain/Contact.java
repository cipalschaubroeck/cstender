/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Entity
@Table(name = "Contact")
public class Contact {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Size(max = 255)
	private String name = "";
	
	@Size(max = 255)
	private String attention = "";
	
	@Size(max = 255)
	private String emails;
	
	@Size(max = 255)
	private String phones;
	
	@Size(max = 255)
	private String faxes;
	
	@Size(max = 255)
	private String disciplines;
	
	@Size(max = 255)
	private String cpv;
	
	@ManyToOne
	@JoinColumn(name="government", referencedColumnName="id")
	private Government government;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null && name.length() > 254) name = name.substring(0,254);
		this.name = name;
	}

	public String getAttention() {
		return attention;
	}

	public void setAttention(String attention) {
		if (attention != null && attention.length() > 254) attention = attention.substring(0,254);
		this.attention = attention;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		while (emails != null && emails.length() > 254) 
			emails = emails.substring(emails.indexOf(";")+1);
		this.emails = emails;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		if (phones != null && phones.length() > 254) phones = phones.substring(0,254);
		this.phones = phones;
	}

	public String getFaxes() {
		return faxes;
	}

	public void setFaxes(String faxes) {
		if (faxes != null && faxes.length() > 254) faxes = faxes.substring(0,254);
		this.faxes = faxes;
	}

	public String getDisciplines() {
		
		return disciplines;
	}

	public void setDisciplines(String disciplines) {
		if (disciplines != null && disciplines.length() > 254) disciplines = disciplines.substring(0,254);
		this.disciplines = disciplines;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		if (cpv != null && cpv.length() > 254) cpv = cpv.substring(0,254);
		this.cpv = cpv;
	}

	public Government getGovernment() {
		return government;
	}

	public void setGovernment(Government government) {
		this.government = government;
	}
	
	public boolean equals(Object other) {
		Contact otherContact = (Contact) other;
		if (other == null) return false;
		return getName().equals(otherContact.getName());
	}
	
	public String toString() {
		return name + " "+ attention + " "+emails+ " "+phones+ " "+faxes;
	}
}
