/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import java.util.HashMap;

/**
 * Feature Map
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class FeatureList extends HashMap<Integer,Feature> {
	HashMap<String,Integer> featureIndex = new HashMap<String, Integer>();
  public Feature getClassFeature() {
  	return get(size()-1);
  }
	public HashMap<String, Integer> getFeatureIndex() {
		return featureIndex;
	}
	public void setFeatureIndex(HashMap<String, Integer> featureIndex) {
		this.featureIndex = featureIndex;
	}
  
}
