/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import libsvm.svm;
import libsvm.svm_model;

/**
 * Wrapper for the LibSVM classifier.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class LibSvmClassifier implements ClassifierInterface {
	protected svm_model smoModel = null;	
	
	public LibSvmClassifier(svm_model smoModel) {
		super();
		this.smoModel = smoModel;
	}

	@Override
	public double classifyInstance(Instance sparseInstance) {
		return svm.svm_predict(smoModel, sparseInstance.getLibSvmInstance());
	}
}
