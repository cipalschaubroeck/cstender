/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import java.util.ArrayList;

/**
 * List of Instances with a feature structure.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class InstanceList extends ArrayList<Instance>{
  public FeatureList structure = null;

	public FeatureList getStructure() {
		return structure;
	}

	public void setStructure(FeatureList structure) {
		this.structure = structure;
	}
}
