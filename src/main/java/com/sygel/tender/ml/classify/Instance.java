/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import libsvm.svm_node;

/**
 * Instance representation of a feature list.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Instance {
  List<Feature> featureList = new ArrayList<>();
  int classFeature = -1;

	public Instance() {
		super();
	}
	
	public void constructInstanceFromFeatures(Set<String> features, FeatureList format) {
	  List<Integer> sparseIndexes = new ArrayList<>();
	  for (String feature : features) {
	  	Integer index = format.getFeatureIndex().get(feature);
	  	if (index != null) 
	  	  sparseIndexes.add(index);
	  }
	  Collections.sort(sparseIndexes);
	  featureList = new ArrayList<>();
	  for (Integer index : sparseIndexes) {
	  	Feature feature = new Feature();
	  	feature.setIndex(index);
	  	feature.setValue(1.0);
	  	featureList.add(feature);
	  }
	}
	
	public void constructInstanceFromARFF(String line) {
		if (line.startsWith("{"))
			constructInstanceSparseFormat(line);
		else
			constructInstanceDenseFormat(line);
	}
	
	protected void constructInstanceDenseFormat(String line) {
		int endIndex = 0;
		int startIndex = 0;		
		int featureIndex = 1;
		do {				     						
			String value = "";
			endIndex = 0;
			if (line.startsWith("'")) { 				
				line = line.substring(endIndex+1);
				endIndex = line.indexOf("'");
				while (endIndex != 0 && line.charAt(endIndex-1) == '\\') endIndex = line.indexOf("'",endIndex+1);
			  value = line.substring(0,endIndex);
				value = value.replaceAll("\\\\'", "'");
			  endIndex++;
			} else {
				endIndex = line.indexOf(",");
				if (endIndex == -1) endIndex = line.length();
				value = line.substring(0,endIndex);
			}						
			Feature feature = new Feature();
			feature.setIndex(featureIndex);
			feature.setName(value);
			getFeatureList().add(feature);
			if (endIndex < line.length() && line.charAt(endIndex) == ',') {
				startIndex = 0;
				line = line.substring(endIndex+1);						
			} else
				startIndex = -1;
		} while (startIndex != -1);
	}
	
	protected void constructInstanceSparseFormat(String line) {
		line = line.substring(1);
		int endIndex = 0;
		int startIndex = 0;		
		do {				     						
			endIndex = line.indexOf(" ");
			int featureIndex = new Integer(line.substring(startIndex,endIndex));
			line = line.substring(endIndex+1);
			String value = "";
			
			if (line.startsWith("'")) { 
				line = line.substring(1);
				endIndex = line.indexOf("'");
				while (endIndex != 0 && line.charAt(endIndex-1) == '\\') endIndex = line.indexOf("'",endIndex+1);
			  value = line.substring(0,endIndex);
				value = value.replaceAll("\\\\'", "'");
			  endIndex++;
			} else {
				endIndex = line.indexOf(",");
				if (endIndex == -1) endIndex = line.indexOf("}");
				value = line.substring(0,endIndex);
			}						
			Feature feature = new Feature();
			feature.setIndex(featureIndex);
		  feature.setName(value);
			try {feature.setValue(new Double(value));} catch (NumberFormatException e) {feature.setName(value);}						
			getFeatureList().add(feature);
			if (line.charAt(endIndex) == ',') {
				startIndex = 0;
				line = line.substring(endIndex+1);						
			} else
				startIndex = -1;
		} while (startIndex != -1);
	}

	public List<Feature> getFeatureList() {
		return featureList;
	}

	public void setFeatureList(List<Feature> featureList) {
		this.featureList = featureList;
	}
	
	public String toLibSVMString() {
		StringBuilder builder = new StringBuilder();
		String classString = "0.0";
		for (Feature feature : featureList) {
			if (feature.getIndex() != classFeature)
			  builder.append(feature.getIndex()+":"+feature.getValue() + " ");
			else
				classString = "1.0";				
		}
		builder.insert(0, classString+" ");
		return builder.toString().trim();
	}
	
	public String toFeatureString(FeatureList format) {
		StringBuilder builder = new StringBuilder("");
		for (Feature feature : featureList) {
			builder.append(format.get(feature.getIndex()).getName()+ " ");
		}
		return builder.toString();
	}
	
	public String toArffString() {
		StringBuilder builder = new StringBuilder("{");
		boolean first = true;
		for (Feature feature : featureList) {
			if (!first) builder.append(",");
			if (feature.getName() == null || feature.getName().length() == 0)
			  builder.append(feature.getIndex()+" "+(feature.getValue() == 1.0 ? "1":"0") + " ");
			else
				if (feature.getName().indexOf(" ") != -1 || feature.getName().indexOf("'") != -1)
				    builder.append(feature.getIndex()+" '"+feature.getName().replaceAll("'", "\\\\'")+"'");		
				  else
				  	builder.append(feature.getIndex()+" "+feature.getName());	
			first = false;
		}
		
		builder.append("}");
		return builder.toString().trim();
	}	
	
	public svm_node[] getLibSvmInstance() {
		svm_node[] nodes = new svm_node[classFeature == -1 ? featureList.size() : featureList.size()-1];
		int i = 0;
		for (Feature feature : featureList) {
			if (feature.getIndex() != classFeature)
			  nodes[i++] = feature.getLibSvmFeature();
		}
		return nodes;
	}
	
	public double getClassValue() {
		return featureList.get(classFeature).getValue();
	}

	public int getClassFeature() {
		return classFeature;
	}

	public void setClassFeature(int classFeature) {
		this.classFeature = classFeature;
	}
	
	public Instance deepCopy() {
		Instance instance = new Instance();
		for (Feature feature : featureList) {
			instance.featureList.add(feature.deepCopy());
		}
		instance.classFeature = classFeature;
		return instance;
	}
}
