/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.sygel.tender.ml.KnowledgeBase;
/**
 * Utility class for format conversions between different machine learning instance representations.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class FormatConversions {
	public static void saveInstanceList(InstanceList instanceList, String fileName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {
			for (Instance instance : instanceList) {
				outLot.write(instance.toLibSVMString() + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void saveFeatureIndexList(FeatureList featureList, String fileName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {		
			List<Integer> indexList = new ArrayList<Integer>(featureList.keySet());
			Collections.sort(indexList);
			for (Integer index : indexList) {
				outLot.write(featureList.get(index).toFeatureIndexString() + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static FeatureList readFeatureIndexList(String fileName) {
		FeatureList featureList = new FeatureList();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8))) {
			String line = br.readLine();
			
			while (line != null) {
				if (!line.startsWith("#")) {
          int endIndex = line.indexOf(" ");
          if (endIndex != -1) {
          	int index = new Integer(line.substring(0,endIndex));
          	String name = line.substring(endIndex+1);
          	Feature feature = new Feature();
          	feature.setIndex(index);
          	feature.setName(name);
          	featureList.put(index, feature);
          	featureList.getFeatureIndex().put(feature.getName(), index);
          }
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return featureList;
	}
	
	public static void saveInstancesInArffFormat(InstanceList instances, String outputFile, String relationName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));) {		
      List<String> features=new ArrayList<>();
      outLot.write("@relation '"+relationName+"'\n");
      for (int index = 0; index < instances.getStructure().size()-1; index++) {
    	  Feature feature = instances.getStructure().get(index);
    	  if (feature.getName().indexOf(" ") != -1 || feature.getName().indexOf("'") != -1)
    	    outLot.write("@attribute '"+feature.getName().replaceAll("'", "\\\\'")+ "' numeric\n");  
    	  else
    	  	outLot.write("@attribute "+feature.getName()+ " numeric\n");
      }
      outLot.write("@attribute "+instances.getStructure().getClassFeature().getName()+"\n");  
      outLot.write("@data\n");
      for (Instance instance :  instances) {
      	outLot.write(instance.toArffString()+"\n");
      }
		} catch (IOException e) {
			e.printStackTrace();
		} 		
	}
	
	/**
	 * Convert ARFF String file to ARFF n-gram feature file.
	 * @param inputFile
	 * @param outputFile
	 * @param minimumFeatureFrequency
	 * @param knowledgeBase
	 */
	public static void convertStringToFeatures(String inputFile, String outputFile, int minimumFeatureFrequency,KnowledgeBase knowledgeBase) {
		InstanceList instanceList = readModelArff(inputFile);
		Map<String,Integer> featureList = new HashMap<>();
		Map<Integer,Set<String>> instanceFeatures = new HashMap<Integer,Set<String>>();
		List<String> classList = new ArrayList<>();
		List<String> classKnowledgeList = new ArrayList<>();
		int index = 0;
    for (Instance instance : instanceList) {
    	String title = instance.getFeatureList().get(1).getName();
    	String essense = instance.getFeatureList().get(2).getName();
    	String cpvKnowledgeBase = instance.getFeatureList().get(5).getName();
    	String cpv = instance.getFeatureList().get(6).getName();
    	if (!classList.contains(cpv)) classList.add(cpv);
    	if (!classKnowledgeList.contains(cpvKnowledgeBase)) classKnowledgeList.add(cpvKnowledgeBase);
    	Set<String> titleFeaturesNoSuffix = knowledgeBase.extractFeaturesAndConcepts(title);
    	Set<String> titleFeatures = new HashSet<String>();
    	for (String feature : titleFeaturesNoSuffix)  titleFeatures.add(feature+"_TITLE");
    	Set<String> essenseFeatures = knowledgeBase.extractFeaturesAndConcepts(essense);
    	knowledgeBase.updateFeatureList(featureList, titleFeatures);
    	knowledgeBase.updateFeatureList(featureList, essenseFeatures);
    	Set<String> features = new HashSet<>(titleFeatures);
    	features.addAll(essenseFeatures);
    	instanceFeatures.put(index++, features);
    }
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));) {		
      List<String> features=new ArrayList<>();
      outLot.write("@relation 'n-gram feature file - multinomial CPV'\n");
      for (String feature : featureList.keySet()) {
    	  if (featureList.get(feature) > minimumFeatureFrequency) {
    	  	features.add(feature);
    	  	outLot.write("@attribute '"+feature.replaceAll("'", "\\\\'")+ "' numeric\n");
    	  }
      }
      outLot.write("@attribute class_disciplines {disciplines");
      for (String c : classList) {
      	outLot.write(",'"+c+"'");
      }      
      outLot.write("}\n"); 
      outLot.write("@attribute class_knowledge {knowledge");
      for (String c : classKnowledgeList) {
      	outLot.write(",'"+c+"'");
      }      
      outLot.write("}\n");
      outLot.write("@data\n");
      for (int i :  instanceFeatures.keySet()) {
    	  Set<String> f = instanceFeatures.get(i);
    	  List<Integer> sparseIndexes = new ArrayList<>();
    	  for (String feature : f) {
    	  	sparseIndexes.add(features.indexOf(feature));
    	  }
    	  if (sparseIndexes.size() > 0) {
    	    Collections.sort(sparseIndexes);
    	    outLot.write("{");
    	    boolean first = true;
    	    for (Integer sparseIndex : sparseIndexes) {
    	  	  outLot.write((first?"":",")+sparseIndex+" 1");
    	  	  first = false;
    	    }
    	    String c = instanceList.get(i).getFeatureList().get(6).getName();
    	    outLot.write(","+(featureList.size())+" '"+c+"'");
    	    c = instanceList.get(i).getFeatureList().get(5).getName();
    	    outLot.write(","+(featureList.size()+1)+" '"+c+"'");
    	    outLot.write("}\n");
    	  }
      }
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Read ARFF model file.
	 * 
	 * @param modelFile
	 * @return
	 */
	public static InstanceList readModelArff(String modelFile) {
  	FeatureList featureList = new FeatureList();
  	InstanceList instanceList = new InstanceList();
  	int index = 0;
  	boolean data = false;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(modelFile), StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (line != null) {
				if (line.startsWith("@attribute")) {
					updateFeatureList(line, featureList, index);
					index++;
				}				
				if (data) {
					Instance sparseInstance = new Instance();
					sparseInstance.constructInstanceFromARFF(line);
					sparseInstance.setClassFeature(index-1);
					instanceList.add(sparseInstance);					
				}
				if (line.startsWith("@data")) data = true;
				line = br.readLine();
			}
    } catch (FileNotFoundException e) {
		  e.printStackTrace();
	  } catch (IOException e) {
		  e.printStackTrace();
	  }
		instanceList.setStructure(featureList);
		
		return instanceList;
	}
	
	/**
	 * Convert ARFF file to FeatureIndex list and LibSVM format.
	 * @param inputFile
	 * @param outputFile
	 * @return
	 */
  public static InstanceList convertArffToFeatureIndexAndLibSVM(String inputFile, String outputFile) {
  	InstanceList instanceList = readModelArff(inputFile);		
		saveInstanceList(instanceList,outputFile+".data");
		saveFeatureIndexList(instanceList.getStructure(),outputFile+".feature");		
		return instanceList;
  }
  
  static private void updateFeatureList(String line,FeatureList featureList,int index) {
		String name = line.substring(11);
		int endFeature = name.lastIndexOf(" numeric");
		if (endFeature != -1) name = name.substring(0,endFeature);
		if (name.startsWith("'")) name = name.substring(1);
		if (name.endsWith("'")) name = name.substring(0,name.length()-1);
		name = name.replaceAll("\\\\'", "'");
		Feature feature = new Feature();		
		feature.setName(name);				
    feature.setIndex(index);
		featureList.put(index, feature);
  }
}
