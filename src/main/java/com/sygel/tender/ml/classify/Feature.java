/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.classify;

import libsvm.svm_node;

/**
 * Feature 
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Feature {
  int index;
  double value;
  String name;
	public Feature() {
		super();
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toFeatureIndexString() {
		return index + " " + name;
	}
	public svm_node getLibSvmFeature() {
		svm_node node = new svm_node();
		node.index = index;
		node.value = value;
		return node;
	}  
	public Feature deepCopy() {
		Feature copy = new Feature();
		copy.setName(name);
		copy.setValue(value);
		copy.setIndex(index);
		return copy;
	}
}
