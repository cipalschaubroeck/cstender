/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml;

import java.util.ArrayList;
import java.util.List;

/**
 * A meaningful entity
 * 
 * @author Jurrien Saelens, Sygel Copyright © Sygel - All Rights Reserved
 *         Unauthorized copying of this file, via any medium is strictly
 *         prohibited Proprietary and confidential
 */
public class KnowledgeEntity {
	private String adjective = null;
	private String entity;
	private String group;
	private String groupBase;
	private String category;
	private List<String> synonymList = null;

	public KnowledgeEntity(String entitieString) {
		String[] info = entitieString.split(";");
		this.entity = info[0].trim();
		if (entity.indexOf(" ") != -1) {
			String[] ae = entity.split(" ");
			if (ae.length == 2) {
				entity = ae[1];
				adjective = ae[0];
			} else {
				entity = null;
			}
		}
		this.group = info[1].trim();
		if (group.indexOf(" ") != -1) {
			String[] ae = group.split(" ");
			if (ae.length == 2) {
				groupBase = ae[1];
			} else {
				groupBase = null;
			}
		} else {
			groupBase = this.group;
		}
		if (info.length == 3)
			this.category = info[2].trim();
	}

	public boolean equals(String adjective, String entity) {
		if (adjective != null) {
			return adjective.equals(this.adjective) && entity.equals(this.entity);
		} else
			return this.adjective == null && entity.equals(this.entity);
	}

	public KnowledgeEntity(String adjective, String entity) {
		if (adjective != null && adjective.length() > 0)
			this.adjective = adjective;
		else
			this.adjective = null;
		this.entity = entity;
		// TODO fix hyphen problems
		if (this.entity != null && this.entity.startsWith("-") && this.entity.length() > 1)
			this.entity = this.entity.substring(1);
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getAdjective() {
		return adjective;
	}

	public void setAdjective(String adjective) {
		this.adjective = adjective;
	}

	public String getGroup() {
		return group;
	}

	public String getGroupBase() {
		return groupBase;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String toString() {
		return (getAdjective() != null ? getAdjective() + " " : "") + getEntity();
	}

	public void addSynonym(String synonym) {
		if (synonymList == null)
			synonymList = new ArrayList<>();
		synonymList.add(synonym);
	}

	public List<String> getSynonymList() {
		return synonymList;
	}

	public void setSynonymList(List<String> synonymList) {
		this.synonymList = synonymList;
	}

}
