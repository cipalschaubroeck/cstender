/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Utility class to create CPV_Upgrade lists for the rule based CPV extractions.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class CpvUpgrade {
	String cpv = null;
	String description = null;
	HashMap<String, Integer> featureCount = new HashMap<>();
	HashMap<String, Integer> featureCountNotInAllCPV = new HashMap<>();

	public CpvUpgrade(String cpv, String description) {
		super();
		this.cpv = cpv;
		this.description = description;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		this.cpv = cpv;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public HashMap<String, Integer> getFeatureCount() {
		return featureCount;
	}

	public void setFeatureCount(HashMap<String, Integer> featureCount) {
		this.featureCount = featureCount;
	}
	
	public HashMap<String, Integer> getFeatureCountNotInAllCPV() {
		return featureCountNotInAllCPV;
	}

	public void setFeatureCountNotInAllCPV(HashMap<String, Integer> featureCountNotInAllCPV) {
		this.featureCountNotInAllCPV = featureCountNotInAllCPV;
	}

	protected List<String> createNeList() {
		List<String> neList = new ArrayList<>();
		for (String rel : featureCount.keySet()) {
			if (rel.startsWith("REL_") && rel.indexOf("_CAT_") == -1 && rel.indexOf("_LOT_") == -1) {
				rel = rel.substring(4);
				try {
					String ne = rel.substring(rel.indexOf("_") + 1);
					if (ne.endsWith("_TITLE"))
						ne = ne.substring(0, ne.length() - 6);
					neList.add(ne);
				} catch (Exception e) {
				}
			}
		}
		return neList;
	}

	public HashMap<String, Integer> constructMergedFeatures() {
		HashMap<String, Integer> mergedFeatureCount = new HashMap<>();
		List<String> neList = createNeList();
		for (String ne : neList) {
			StringBuilder verbList = new StringBuilder();
			int mergedOccurence = 0;
			for (String rel : featureCount.keySet()) {
				if (rel.startsWith("REL_") && rel.indexOf("_CAT_") == -1 && rel.indexOf("_LOT_") == -1) {
					int occurence = featureCount.get(rel);
					rel = rel.substring(4);
					try {
						String cverb = rel.substring(0, rel.indexOf("_"));
						String cne = rel.substring(rel.indexOf("_") + 1);
						if (cne.endsWith("_TITLE"))
							cne = cne.substring(0, ne.length() - 6);
						if (cne.equals(ne)) {
							verbList.append((verbList.length() != 0 ? "|":"") + cverb);
							mergedOccurence += occurence;
						}
					} catch (Exception e) {
					}
				}
			}
			if (verbList.toString().indexOf("|") != -1) {
			  mergedFeatureCount.put("REL_("+verbList.toString()+")_"+ne, mergedOccurence);
			  //mergedFeatureCount.put("REL_("+verbList.toString()+")_"+ne+"_TITLE", mergedOccurence);
			}else{
				String feature = "REL_"+verbList.toString()+"_"+ne;
				mergedFeatureCount.put(feature, mergedOccurence);
				//mergedFeatureCount.put("REL_"+verbList.toString()+"_"+ne+"_TITLE", mergedOccurence);
			}
		}
		
		for (String rel : featureCount.keySet()) {
			if (rel.startsWith("NE_") || rel.indexOf("_CAT_") != -1 || rel.indexOf("_LOT_") != -1) {
				mergedFeatureCount.put(rel, featureCount.get(rel));
			}
		}
		
		return mergedFeatureCount;
	}
	
	protected static List<String> createNeList(Map<String, CpvUpgradeElement> atomicKeywordMap) {
		List<String> neList = new ArrayList<>();
		for (String rel : atomicKeywordMap.keySet()) {
			if (rel.startsWith("REL_") && rel.indexOf("_CAT_") == -1 && rel.indexOf("_LOT_") == -1) {
				rel = rel.substring(4);
				try {
					String ne = rel.substring(rel.indexOf("_") + 1);
					if (ne.endsWith("_TITLE"))
						ne = ne.substring(0, ne.length() - 6);
					neList.add(ne);
				} catch (Exception e) {
				}
			}
		}
		return neList;
	}
	
	
	public static Map<String, CpvUpgradeElement> constructMergedCpvUpgradeElements(Map<String, CpvUpgradeElement> atomicKeywordMap) {
		HashMap<String, CpvUpgradeElement> mergedFeatureCount = new HashMap<>();
		List<String> neList = createNeList(atomicKeywordMap);
		for (String ne : neList) {
			StringBuilder verbList = new StringBuilder();
			StringBuilder cpvList = new StringBuilder();
			int mergedOccurence = 0;
			for (String rel : atomicKeywordMap.keySet()) {
				if (rel.startsWith("REL_") && rel.indexOf("_CAT_") == -1 && rel.indexOf("_LOT_") == -1) {
					int occurence = atomicKeywordMap.get(rel).getOccurence();
					String  cpv = atomicKeywordMap.get(rel).getCpv();
					rel = rel.substring(4);
					try {
						String cverb = rel.substring(0, rel.indexOf("_"));
						String cne = rel.substring(rel.indexOf("_") + 1);
						if (cne.endsWith("_TITLE"))
							cne = cne.substring(0, ne.length() - 6);
						if (cne.equals(ne)) {
							verbList.append((verbList.length() != 0 ? "|":"") + cverb);
							mergedOccurence += occurence;
							if (cpv != null) {
								String[] cpvArray = cpv.trim().split("_");
						  	for (String c : cpvArray) {
						  		if (cpvList.indexOf(c) == -1) cpvList.append("_"+c);
						  	}							  
							}
						}
					} catch (Exception e) {
					}
				}
			}
			if (cpvList.length() == 0) 
				cpvList.append("00000000");
			else
				cpvList = cpvList.deleteCharAt(0);
			if (verbList.toString().indexOf("|") != -1) {
			  mergedFeatureCount.put("REL_("+verbList.toString()+")_"+ne, new CpvUpgradeElement("REL_("+verbList.toString()+")_"+ne,cpvList.toString().trim(),mergedOccurence));
			  //mergedFeatureCount.put("REL_("+verbList.toString()+")_"+ne+"_TITLE", mergedOccurence);
			}else{
				String feature = "REL_"+verbList.toString()+"_"+ne;
				mergedFeatureCount.put(feature, new CpvUpgradeElement(feature,cpvList.toString().trim(),mergedOccurence));
				//mergedFeatureCount.put("REL_"+verbList.toString()+"_"+ne+"_TITLE", mergedOccurence);
			}
		}
		
		for (String rel : atomicKeywordMap.keySet()) {
			if (rel.startsWith("NE_") || rel.indexOf("_CAT_") != -1 || rel.indexOf("_LOT_") != -1) {
				mergedFeatureCount.put(rel, atomicKeywordMap.get(rel));
			}
		}
		
		return mergedFeatureCount;
	}
	
	public static List<String> convertToList(Map<String, CpvUpgradeElement> map) {
		List<String> list = new ArrayList<String>();
		for (CpvUpgradeElement element : map.values()) {
			list.add(element.toString());
		}
		return list;
	}
	
  /**
   * Sort an unsorted map
   * 
   * @param unsortMap
   * @param order
   * @return sorted map
   */
	public static Map<String, CpvUpgradeElement> sortByComparator(Map<String, CpvUpgradeElement> unsortMap, final boolean order) {

		List<Entry<String, CpvUpgradeElement>> list = new LinkedList<Entry<String, CpvUpgradeElement>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<String, CpvUpgradeElement>>() {
			public int compare(Entry<String, CpvUpgradeElement> o1, Entry<String, CpvUpgradeElement> o2) {
				if (order) {
					return o1.getValue().getOccurence().compareTo(o2.getValue().getOccurence());
				} else {
					return o2.getValue().getOccurence().compareTo(o1.getValue().getOccurence());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<String, CpvUpgradeElement> sortedMap = new LinkedHashMap<String, CpvUpgradeElement>();
		for (Entry<String, CpvUpgradeElement> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}
	
}
