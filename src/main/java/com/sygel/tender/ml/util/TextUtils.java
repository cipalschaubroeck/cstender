/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Common text utilities
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class TextUtils {
  public static String[] tokens(String line) {
	  return line.split("( |,|\\.|;|/)");
  }  
    
  public static int indexOf(String text,String pattern) {
	  Pattern p = Pattern.compile(pattern);  // insert your pattern here
	  Matcher m = p.matcher(text);
	  if (m.find()) {
	     //if (m.start() == 0) return -1;
	     return m.start();
	  } else {
		  return -1;
	  }
  }  
  
	public static String[] constructWordBagWithDot(String text) {
		String[] bag = text.split("[\\s,;:‘“”]");
		return bag;
	}
  
	public static String[] constructWordBag(String text) {
		String[] bag = text.split("[\\s,\\.;:‘“”]");
		return bag;
	}
  
  public static int indexOf(String text, String pattern, int startPosition) {
  	String t = text.substring(startPosition);
  	int index = indexOf(t, pattern);
  	if (index != -1)  index += startPosition;
  	return index;
  }
  
  public static int indexOfNotLetter(String text) {
	  return indexOf(text, "[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]");
  }  
  
  public static int indexOfBetweenNotLetter(String text,String pattern) {
	  return indexOf(text, "[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]"+pattern+"[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]");
  }
  
  public static int indexOfBetweenNotLetter(String text,String pattern, int startPosition) {
	  return indexOf(text, "[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]"+pattern+"[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]", startPosition);
  }
}
