/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Common utilities
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Utils {
	/**
	 * Split a list in sublists
	 * 
	 * @param list input list
	 * @param size
	 * @return List of sublists
	 */
  public static List<List<String>> splitList(List list, int size) {
		List<List<String>> partitions = new LinkedList<List<String>>();
		int partitionSize = (int) Math.floor(list.size()/size);
		for (int i = 0; i < size; i++) {
			partitions.add(list.subList((partitionSize*i), i == size -1 ? list.size() : partitionSize*(i+1)));
		}
		return partitions;
  }
  
  /**
   * Sort an unsorted map
   * 
   * @param unsortMap
   * @param order
   * @return sorted map
   */
	public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				if (order) {
					return o1.getValue().compareTo(o2.getValue());
				} else {
					return o2.getValue().compareTo(o1.getValue());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}
	
	public static void printMap(Map<String, Integer> map) {
		int count = 0;
		for (Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
			count++;
		}
	}

	public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2) {
		int count = 0;
		for (Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(count + " Key : " + entry.getKey() + " Value : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()));
			count++;
		}
	}

	public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2, Map<String, Integer> info3, Map<String, Double> info4, Writer writer) throws IOException {
		printMap(map, info, info2, info3, info4, writer, "");
	}

	public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2, Map<String, Integer> info3, Map<String, Double> info4, Writer writer, String prefix)
			throws IOException {
		int count = 0;
		for (Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(prefix + count + " Key : " + entry.getKey() + " Value : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()) + "/"
					+ info3.get(entry.getKey()) + "/" + info4.get(entry.getKey()));
			count++;
			writer.write(prefix + entry.getKey() + " Percent : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()) + "/" + info3.get(entry.getKey()) + "/"
					+ info4.get(entry.getKey()) + "\n");
		}
	}
	public static void writeMap(Map<String, Integer> map, String fileName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {

			for (String key : map.keySet()) {
				if (key != null && !"null".equals(key))
					outLot.write(key + ";" + key + " " + map.get(key) + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
