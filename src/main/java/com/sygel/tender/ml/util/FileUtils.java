/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Common file utilities
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class FileUtils {
	/**
	 * Read a file and store the lines in a Map. Structure of the file: # comment
	 * is ignored key=value pairs are stored in the provided Map
	 * 
	 * @param filename
	 * @param map
	 */
	public static void readFileIntoMap(String filename, Map<String, String> map) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (line != null) {
				if (!line.startsWith("#")) {
					String[] info = line.split("=");
					String key = info[0];
					if (map.get(key) == null)
						map.put(key, info[1]);
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read a file and store the lines in a List. Structure of the file: # comment
	 * is ignored other lines are stored in the List
	 * 
	 * @param filename
	 * @param list
	 */
	public static void readFileIntoList(String filename, List<String> list) {
		 readFileIntoList(filename, list, null);
	}
	
	/**
	 * Read a file and store the lines in a List. Structure of the file: # comment
	 * is ignored other lines are stored in the List
	 * All tokens after ignoreDelimiter are ignored
	 * 
	 * @param filename
	 * @param list
	 * @param ignoreDelimiter All tokens after ignoreDelimiter are ignored
	 */
	public static void readFileIntoList(String filename, List<String> list, String ignoreDelimiter) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (line != null) {
				if (!line.startsWith("#")) {
					int p = 0;
					if (ignoreDelimiter != null && (p =line.indexOf(ignoreDelimiter)) != -1) 
						list.add(line.substring(0,p));
				  else
					  list.add(line);
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
  /**
   * Write the elements of the list separated by \n
   * @param list
   * @param fileName
   */
	public static void writeList(List<String> list, String fileName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {
			for (String element : list) {
				outLot.write(element + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void writeMap(Map<String, Integer> map, String fileName) {
		try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {
			for (Entry<String, Integer> entry : map.entrySet()) {
				outLot.write(entry.getKey() + " " + entry.getValue()+ "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Convert ISO_8859_1 to UTF-8 utility
	 */
	static void convert() {
		String in =  "category_nl_be.txt";
		String outF = "category_nl_be_utf8.txt";

		Writer out;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outF), "UTF-8"));

			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(in), StandardCharsets.ISO_8859_1))) {
				String line = br.readLine();
				while (line != null) {
					out.write(line + "\n");
					line = br.readLine();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	public static void readResourceIntoMap(String filename, Map<String, String> map) {
		InputStream in = new FileUtils().getClass().getClassLoader().getResourceAsStream(filename);
		readFileIntoMap(in,map);
	}
	/**
	 * Read a file and store the lines in a Map. 
	 * Structure of the file: 
	 * # comment is ignored 
	 * key[= ]value pairs are stored in the provided Map
	 * 
	 * 
	 * @param filename
	 * @param map
	 */
	public static void readFileIntoMap(InputStream inputStream, Map<String, String> map) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (line != null) {
				if (!line.startsWith("#")) {
					String[] info = null;
					if (line.indexOf("=") == -1) {
						int end = line.indexOf("\t");
						if (end == -1) end = line.indexOf(" ");
						info = new String[2];
						info[0] = line.substring(0,end);
						info[1] = line.substring(end+1);						
					} else
						info = line.split("=");					
					String key = info[0];
					if (map.get(key) == null)
						map.put(key, info[1]);
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
