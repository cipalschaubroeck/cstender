/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * CpvUpgradeElement representation.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class CpvUpgradeElement {
	String keywords = null;
	String cpv = "";
	Integer occurence = new Integer(0);
	int atomicOccurence = 1;

	public CpvUpgradeElement(String keyword, String cpv, Integer occurence) {
		super();
		this.keywords = keyword;
		this.cpv = cpv;
		this.occurence = occurence;
	}

	public CpvUpgradeElement(String line) {
		super();
		String[] info = line.split(" ");
		this.keywords = info[0];
		//System.out.println(line);
		if (info.length > 2) {
			this.cpv = info[1];		
			this.occurence = new Integer(info[2]);
		} else if (info.length == 2) {
			if (info[1].length() > 7)
				this.cpv = info[1];	
			else
			  this.occurence = new Integer(info[1]);
		}
	}
	
	public String toString() {
		return keywords + " " + (cpv.length() == 0 ? "00000000" : cpv) + " " + occurence;
	}

	public List<String> constructKeywordList() {
		String[] kArray = keywords.split(",");
		return Arrays.asList(kArray);
	}
	
	public int getAtomicOccurence() {
		return atomicOccurence; 
	}

	public List<String> constructAtomicKeywordList() {
		List<String> atomicKeywordList = new ArrayList<String>();
		for (String key : constructKeywordList()) {
			if (!key.startsWith("#")) {
				int pos = key.indexOf("(");
				if (pos != -1) {
					int end = key.indexOf(")", pos);
					if (end != -1) {
						String prefix = key.substring(0, pos);
						String[] keys = key.substring(pos + 1, end).split("\\|");
						String ne = key.substring(end + 2);
						for (String k : keys) {
							atomicKeywordList.add(prefix + k + "_" + ne);
						}
					}
				} else {
					atomicKeywordList.add(key);
				}
			}
		}
		atomicOccurence = occurence / atomicKeywordList.size();
		return atomicKeywordList;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getCpv() {
		return cpv;
	}

	public void setCpv(String cpv) {
		this.cpv = cpv;
	}

	public Integer getOccurence() {
		return occurence;
	}

	public void setOccurence(Integer occurence) {
		this.occurence = occurence;
	}

	public void addCpvString(String cpvNoParent) {
		String[] cpvArray = cpvNoParent.trim().split(" ");
  	for (String c : cpvArray) {
  		if (cpv.indexOf(c) == -1) cpv += "_" + c;
  	}
		
	}

}
