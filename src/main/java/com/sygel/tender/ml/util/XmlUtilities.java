/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Common XML utilities
 * 
 * @author Jurrien Saelens, Sygel Copyright © Sygel - All Rights Reserved
 *         Unauthorized copying of this file, via any medium is strictly
 *         prohibited Proprietary and confidential
 */
public class XmlUtilities {
	public static Document stringToXML(String text) throws IOException, ParserConfigurationException, SAXException {
		StringReader reader = new StringReader(text);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		org.w3c.dom.Document doc = db.parse(new InputSource(reader));
		reader.close();
		return doc;
	}

	public static Document getDocument(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(inputStream);
		inputStream.close();
		return document;
	}

	public static boolean getNodeExists(String xpath, Document document) {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);

			String resultValue = null;

			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeValue() != null)
					resultValue = nodes.item(i).getNodeValue();
			}
			return nodes.getLength() != 0;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean getNodeExists(String xpath, Node node) {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(node, XPathConstants.NODESET);

			String resultValue = null;

			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeValue() != null)
					resultValue = nodes.item(i).getNodeValue();
			}
			return nodes.getLength() != 0;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String getNodeValue(String xpath, Document document) {
		String resultValue = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeValue() != null)
					resultValue = nodes.item(i).getNodeValue();
			}
			return replaceWeirdChars(resultValue);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static NodeList getNodes(String xpath, Document document) {
		String resultValue = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			return (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String replaceWeirdChars(String input) {
		if (input == null)
			return null;
		return input.replace('’', '\'');
	}

	public static String getPlainText(Node node) {
		StringBuffer result = new StringBuffer("");
		if (node.getNodeValue() != null) {
			String text = node.getNodeValue().trim().replace('\t', ' ') + " ";
			if (text.startsWith("-") && text.length() > 2) {
				text = "- " + text.substring(1);
			}
			result.append(text);
		}
		if (node.hasChildNodes()) {
			NodeList children = node.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node n = children.item(j);
				result.append(getPlainText(n));
			}
		}
		return result.toString();
	}

	private static String getPlainTextNoDouble(Node node) {
		StringBuffer result = new StringBuffer("");
		if (node.getNodeValue() != null) {
			result.append(node.getNodeValue().trim().replace('\t', ' ') + " ");
		}
		if (node.hasChildNodes()) {
			NodeList children = node.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node n = children.item(j);
				String pt = getPlainTextNoDouble(n);
				if (result.indexOf(pt.trim()) == -1)
					result.append(pt);
			}
		}
		return result.toString();

	}

	public static String getNodePlainText(String xpath, Document document) {
		String resultValue = "";
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				resultValue += getPlainText(n);
			}
			return replaceWeirdChars(resultValue).trim();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getNodePlainText(String xpath, Node node) {
		String resultValue = "";
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(node, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				resultValue += getPlainText(n);
			}
			return replaceWeirdChars(resultValue).trim();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getNodePlainTextNoDoubles(String xpath, Document document) {
		String resultValue = "";
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				String pt = getPlainTextNoDouble(n);
				if (resultValue.indexOf(pt.trim()) == -1)
					resultValue += pt;
			}
			return replaceWeirdChars(resultValue).trim();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getNodePlainTextNoDoubles(String xpath, Node node) {
		String resultValue = "";
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(node, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				String pt = getPlainTextNoDouble(n);
				if (resultValue.indexOf(pt.trim()) == -1)
					resultValue += pt;
			}
			return replaceWeirdChars(resultValue).trim();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Node getFirstNode(String xpath, Document document) {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);
			if (nodes != null && nodes.getLength() > 0)
				return nodes.item(0);

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean containsNode(String xpath, Document document) {
		return getFirstNode(xpath, document) != null;
	}

	public static String[] getNodePlainTextArray(String xpath, Document document) {
		Vector resultValue = new Vector();
		XPath xPath = XPathFactory.newInstance().newXPath();

		NodeList nodes;
		try {
			nodes = (NodeList) xPath.compile(xpath).evaluate(document.getDocumentElement(), XPathConstants.NODESET);
			// NodeList nodes = result.nodeset();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				resultValue.add(replaceWeirdChars(getPlainText(n)));
			}
			String[] array = null;
			if (resultValue.size() > 0) {
				array = new String[resultValue.size()];
				array = (String[]) resultValue.toArray(array);
			}
			return array;
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Date getDate(String xpath, Document document) {
		String day = getNodeValue(xpath + "/DAY/text()", document);
		String month = getNodeValue(xpath + "/MONTH/text()", document);
		String year = getNodeValue(xpath + "/YEAR/text()", document);
		String time = getNodeValue(xpath + "/TIME/text()", document);
		String d = day + "/" + month + "/" + year + " " + time;
		// System.out.println("parse date " + d);
		if (day != null && month != null && year != null && time != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			try {
				return formatter.parse(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (day != null && month != null && year != null) {
			d = day + "/" + month + "/" + year;
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			try {
				return formatter.parse(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String xmlToString(Document doc) throws ParserConfigurationException, TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(domSource, result);
		return writer.toString();
	}

}
