/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Document;

import com.sygel.tender.ml.classify.Feature;
import com.sygel.tender.ml.classify.FeatureList;
import com.sygel.tender.ml.classify.FormatConversions;
import com.sygel.tender.ml.classify.Instance;
import com.sygel.tender.ml.classify.InstanceList;
import com.sygel.tender.ml.util.AkteUtilities;
import com.sygel.tender.ml.util.FileUtils;
import com.sygel.tender.ml.util.Utils;
import com.sygel.tender.ml.util.XmlUtilities;

import libsvm.*;

/**
 * Things to do for building new models:
 * 
 * 1° create the corpus  data/tender_corpus.txt in StartDeamon trainingService.contructCorpus();
 * 2° create data/newEntities and data/newAdjectives with the KnowledgeBase main function constructNewEntitiesAdjectivesFromCorpus()
 * 3° check if there are new important features in data/newEntities and data/newAdjectives and copy them manually to namedEntities,namedAdjectives and verbphrases in the resource bundle.  Stopwords can be added to the stopword list in the resource bundle.
 * 4° create the training data. data/atender_cpv.arff with trainingService.contructTraningDataCpv(); in StartDeamon
 * 5° copy the atender_cpv.arff to Build.rootExtFolder + "tender/atender_cpv.arff"
 * 6° remove tender_cpv.feature and run this buildModel method (in the main of this class)
 * 7° run importPreService.updateTenderAI(); in StartDeamon to update the CPV AI and Discipline AI
 *
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

public class BuildModels {
	public static final Boolean SYNC = new Boolean(true);
  public static int counter = 0;
  public static String rootDataFolder = constructRootDataFolder();
	public static String rootExtFolder = constructRootExtFolder();

	private final int MAXTHREADS = 8;
	
	public static String constructRootDataFolder() {
		HashMap<String,String> map = new HashMap<String, String>();
		FileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map);
		System.out.println("rootDataFolder " + map.get("rootDataFolder"));
		return map.get("rootDataFolder");
	}
	
	public static String constructRootExtFolder() {
		HashMap<String,String> map = new HashMap<String, String>();
		FileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map);
		System.out.println("rootExtFolder " + map.get("rootExtFolder"));		
		return map.get("rootExtFolder");
	}
	
	public static void main(String[] args) {
    BuildModels build = new BuildModels();
    build.buildModel();
		//System.out.println("rootResourceFolder "+KnowledgeBase.rootResourceFolder);
	}	
	
	public static void createKnowledgeTenderMultinominalClass(KnowledgeBase knowledgeBase) throws Exception {
		FormatConversions.convertStringToFeatures(BuildModels.rootExtFolder + "tender/atender_cpv.arff", BuildModels.rootExtFolder + "tender/atender_cpv_multinominalclass.arff", 0, knowledgeBase);
	}	
	
	/**
	 * Test the accuracy and recall of the classifier
	 * @param prob
	 * @param param
	 * @param nr_fold
	 */
	/*
  static private void crossValidation(Problem prob,Parameter param, int nr_fold) {
    double[] target = new double[prob.l];
    long start, stop;
    start = System.currentTimeMillis();
    Linear.crossValidation(prob, param, nr_fold, target);
    stop = System.currentTimeMillis();
    System.out.println("time: " + (stop - start) + " ms");
    int total_correct = 0;
    int total_true = 0;
    int total_true_correct = 0;
    for (int i = 0; i < prob.l; i++) {
       if (target[i] == prob.y[i]) ++total_correct;
       if (1.0 == prob.y[i]) ++total_true;
       if (1.0 == prob.y[i] && target[i] == 1.0) ++total_true_correct;
    }
    System.out.printf("correct: %d / %d%n", total_correct, prob.l);
    System.out.printf("Cross Validation Accuracy = %g%%%n", 100.0 * total_correct / prob.l); 
    System.out.printf("Cross Validation Recall = %g%% -  %d / %d %n", 100.0 * total_true_correct / total_true, total_true_correct, total_true); 
  }
  */
  
	/**
	 * Test the accuracy and recall of the classifier
	 */
  /*
  static private void accuracyClassifier(Classifier smoModel, Instances data, String cpv) {
    long start, stop;
    start = System.currentTimeMillis();
    int l = data.numInstances();
    int total_correct = 0;
    int total_true = 0;
    int total_true_correct = 0;
    for (int i = 0; i < l; i++) {
    	SparseInstance instance = (SparseInstance)data.get(i);
    	instance.setDataset(data);
    	double y = 0;
			try {
				y = smoModel.classifyInstance(instance);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	double target = instance.classValue();
      if (target == y) ++total_correct;
      if (1.0 == target) ++total_true;
      if (1.0 == y && target == 1.0) ++total_true_correct;
      if (1.0 == y && target != 1.0) {
      	System.out.printf("extra added by SMO %s %s %n",cpv,instanceToString(instance));
      }
      if (1.0 != y && target == 1.0) {
      	System.out.printf("not added by SMO %s %s %n",cpv,instanceToString(instance));
      }
    }

    System.out.printf("correct: %s %d / %d%n",cpv, total_correct, l);
    System.out.printf("Cross Validation Accuracy = %s %g%%%n",cpv, 100.0 * total_correct / l); 
    System.out.printf("Cross Validation Recall = %s %g%% -  %d / %d %n",cpv, 100.0 * total_true_correct / total_true, total_true_correct, total_true); 
    stop = System.currentTimeMillis();
    System.out.println("time: " + (stop - start) + " ms");
  }
  */
  
	/**
	 * Test the accuracy and recall of the classifier
	 */
  static private void accuracyClassifier(svm_model smoModel, svm_problem data, String cpv, FeatureList featureList) {
    long start, stop;
    start = System.currentTimeMillis();
    int l = data.l;
    int total_correct = 0;
    int total_true = 0;
    int total_true_correct = 0;
    for (int i = 0; i < l; i++) {
    	svm_node[] instance = data.x[i];
    	
    	double y = 0;
			try {
				y = svm.svm_predict(smoModel, instance);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	double target = data.y[i];
      if (target == y) ++total_correct;
      if (1.0 == target) ++total_true;
      if (1.0 == y && target == 1.0) ++total_true_correct;
      if (1.0 == y && target != 1.0) {
      	System.out.printf("extra added by SMO %s %s %n",cpv,instanceToString(instance, featureList));
      	counter++;
      }
      if (1.0 != y && target == 1.0) {
      	//System.out.printf("not added by SMO %s %s %n",cpv,instanceToString(instance, featureList));
      }
    }

    System.out.printf("correct: %s %d / %d%n",cpv, total_correct, l);
    System.out.printf("Cross Validation Accuracy = %s %g%%%n",cpv, 100.0 * total_correct / l); 
    System.out.printf("Cross Validation Recall = %s %g%% -  %d / %d %n",cpv, 100.0 * total_true_correct / total_true, total_true_correct, total_true); 
    stop = System.currentTimeMillis();
    //System.out.println("time: " + (stop - start) + " ms");
  }
  
	private static void do_cross_validation(svm_parameter param, svm_problem prob, int nr_fold)
	{
		int i;
		int total_correct = 0;
		int total_true = 0;
		int total_true_correct = 0;
		double total_error = 0;
		double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
		double[] target = new double[prob.l];

		svm.svm_cross_validation(prob,param,nr_fold,target);
		if(param.svm_type == svm_parameter.EPSILON_SVR ||
		   param.svm_type == svm_parameter.NU_SVR)
		{
			for(i=0;i<prob.l;i++)
			{
				double y = prob.y[i];
				double v = target[i];
				total_error += (v-y)*(v-y);
				sumv += v;
				sumy += y;
				sumvv += v*v;
				sumyy += y*y;
				sumvy += v*y;
			}
			System.out.print("Cross Validation Mean squared error = "+total_error/prob.l+"\n");
			System.out.print("Cross Validation Squared correlation coefficient = "+
				((prob.l*sumvy-sumv*sumy)*(prob.l*sumvy-sumv*sumy))/
				((prob.l*sumvv-sumv*sumv)*(prob.l*sumyy-sumy*sumy))+"\n"
				);
		}
		else
		{
			for(i=0;i<prob.l;i++) {
	      if (target[i] == prob.y[i]) ++total_correct;
	      if (1.0 == prob.y[i]) ++total_true;
	      if (1.0 == prob.y[i] && target[i] == 1.0) ++total_true_correct;
			}
	    System.out.printf("correct: %d / %d%n", total_correct, prob.l);
	    System.out.printf("Cross Validation Accuracy = %g%%%n", 100.0 * total_correct / prob.l); 
	    System.out.printf("Cross Validation Recall = %g%% -  %d / %d %n", 100.0 * total_true_correct / total_true, total_true_correct, total_true); 

		}
	}

  static protected String instanceToString(svm_node[] instance, FeatureList featureList) {
  	StringBuilder stringBuilder = new StringBuilder();
		for (svm_node node : instance)
		  stringBuilder.append(featureList.get(node.index).getName() + " ");
    return stringBuilder.toString();
  }
  
	private static double atof(String s)
	{
		double d = Double.valueOf(s).doubleValue();
		if (Double.isNaN(d) || Double.isInfinite(d))
		{
			System.err.print("NaN or Infinity in input\n");
			System.exit(1);
		}
		return(d);
	}

	private static int atoi(String s)
	{
		return Integer.parseInt(s);
	}
  
	static private svm_problem read_problem(File input_file, svm_parameter param) throws IOException
	{
		svm_problem prob;
		BufferedReader fp = new BufferedReader(new FileReader(input_file));
		Vector<Double> vy = new Vector<Double>();
		Vector<svm_node[]> vx = new Vector<svm_node[]>();
		int max_index = 0;

		while(true)
		{
			String line = fp.readLine();
			if(line == null) break;

			StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

			vy.addElement(atof(st.nextToken()));
			int m = st.countTokens()/2;
			svm_node[] x = new svm_node[m];
			for(int j=0;j<m;j++)
			{
				x[j] = new svm_node();
				x[j].index = atoi(st.nextToken());
				x[j].value = atof(st.nextToken());
			}
			if(m>0) max_index = Math.max(max_index, x[m-1].index);
			vx.addElement(x);
		}

		prob = new svm_problem();
		prob.l = vy.size();
		prob.x = new svm_node[prob.l][];
		for(int i=0;i<prob.l;i++)
			prob.x[i] = vx.elementAt(i);
		prob.y = new double[prob.l];
		for(int i=0;i<prob.l;i++)
			prob.y[i] = vy.elementAt(i);

		if(param.gamma == 0 && max_index > 0)
			param.gamma = 1.0/max_index;

		if(param.kernel_type == svm_parameter.PRECOMPUTED)
			for(int i=0;i<prob.l;i++)
			{
				if (prob.x[i][0].index != 0)
				{
					System.err.print("Wrong kernel matrix: first column must be 0:sample_serial_number\n");
					System.exit(1);
				}
				if ((int)prob.x[i][0].value <= 0 || (int)prob.x[i][0].value > max_index)
				{
					System.err.print("Wrong input format: sample_serial_number out of range\n");
					System.exit(1);
				}
			}

		fp.close();
	  return prob;
	}
	
	private static svm_print_interface svm_print_devnull = new svm_print_interface()
	{
		public void print(String s)
		{
		}
	};
	
	public static svm_parameter createSvmParameter() {
		svm_parameter param = new svm_parameter();
		param.svm_type = svm_parameter.C_SVC;
		param.kernel_type = svm_parameter.LINEAR;
		param.degree = 3;
		param.gamma = 0;	// 1/num_features
		param.coef0 = 0;
		param.nu = 0.5;
		param.cache_size = 100;
		param.C = 1;
		param.eps = 1e-3;
		param.p = 0.1;
		param.shrinking = 1;
		param.probability = 0;
		param.nr_weight = 0;
		param.weight_label = new int[0];
		//param.weight_label[0] = 0;
		//param.weight_label[1] = 1;
		param.weight = new double[0];
		//param.weight[0] = 0.1;
		//param.weight[1] = 1;
		return param;
	}

	public static void createTenderCpvModel(String cpv, int core, KnowledgeBase knowledgeBase, FeatureList featureList) {
		File sourceFile = new File(BuildModels.rootExtFolder + "tender/model/tender_"+cpv+".data");
		System.out.println("createTenderCpvModel "+cpv);
		try {			
			/*
			Parameter parameter = new Parameter(SolverType.L2R_L2LOSS_SVC_DUAL, 1, Double.POSITIVE_INFINITY, 0.1);
      double bias = -1;      			
			Problem problem = Train.readProblem(sourceFile,bias);
			crossValidation(problem,parameter,5);
			Model model = Linear.train(problem, parameter);
			synchronized (SYNC) {
        Linear.saveModel(new File(BkCache.rootExtFolder + "tender/model/lin_" + cpv + ".model"), model);
			}
			*/
			
			
			svm.svm_set_print_string_function(svm_print_devnull);
			svm_parameter parameter = createSvmParameter();
			svm_problem problem = read_problem(sourceFile, parameter);
			String error_msg = svm.svm_check_parameter(problem,parameter);
			if(error_msg != null)
			{
				System.err.print("ERROR: "+error_msg+"\n");
				System.exit(1);
			}
			//do_cross_validation(parameter, problem, 20);						
			svm_model model = svm.svm_train(problem,parameter);
			//accuracyClassifier(model,problem,cpv + " " +knowledgeBase.getCpvTranslations().get(cpv), featureList);
			synchronized (SYNC) {
			  svm.svm_save_model(BuildModels.rootExtFolder + "tender/model/smo_" + cpv + ".model",model);
			  sourceFile.delete();
			}	
					
			
			/*
			ArffLoader sourceLoader = new ArffLoader();
			sourceLoader.setFile(sourceFile);
			Instances data = sourceLoader.getDataSet();			
			data.setClassIndex(data.numAttributes() - 1);			
			SMO smoModel = new SMO();
			smoModel.buildClassifier(data);
			accuracyClassifier(smoModel, data, cpv + " " +knowledgeBase.getCpvTranslations().get(cpv));			
			synchronized (SYNC) {
			  weka.core.SerializationHelper.write(BkCache.rootExtFolder + "tender/model/smo_" + cpv + ".model", smoModel);
			}
			*/
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static FeatureList createKnowledgeTenderCpv2(String cpv, int core) throws Exception {
		FeatureList featureList = null;
		InstanceList instanceList = null;
		InstanceList data = FormatConversions.readModelArff(BuildModels.rootExtFolder + "tender/atender_cpv_multinominalclass.arff");
    
		List<Instance> positiveList = new ArrayList<Instance>();
		
		for (Instance inst : data) {
			Feature classFeature = inst.getFeatureList().get(inst.getFeatureList().size()-2);
			Feature cpvKnowledgeFeature = inst.getFeatureList().get(inst.getFeatureList().size()-1);
			inst.getFeatureList().remove(inst.getFeatureList().size()-1);
			String disciplines = classFeature.getName();
			String knowledgeCpv = cpvKnowledgeFeature.getName();
			if (disciplines.indexOf(cpv) != -1) {
				classFeature.setName("true");
				if (knowledgeCpv.indexOf(cpv) != -1) // only add copy if knowledgebase cpv.
				  positiveList.add(inst.deepCopy());
			} else {
				inst.getFeatureList().remove(inst.getFeatureList().size()-1);
			}
			
		}
		// add extra positive copies
		if (positiveList.size() > 0) {
		  int index = (int) Math.floorDiv(data.size()-1,positiveList.size());
		  int counter = 0;
		  for (Instance copy : positiveList) {
			  int i = index * counter;
			  data.add(i,copy);
			  counter++;
	  	}
		}
		data.getStructure().remove(data.getStructure().size()-1);
		data.getStructure().getClassFeature().setName(cpv+"_class {false,true}");		
		
		synchronized (SYNC) {
			FormatConversions.saveInstancesInArffFormat(data,BuildModels.rootExtFolder + "tender/model/tender_"+cpv+".arff","cpv true false");
			instanceList = FormatConversions.convertArffToFeatureIndexAndLibSVM(BuildModels.rootExtFolder + "tender/model/tender_"+cpv+".arff", BuildModels.rootExtFolder + "tender/model/tender_"+cpv);
			featureList = instanceList.getStructure();
			new File(BuildModels.rootExtFolder + "tender/model/tender_"+cpv+".arff").delete();
			File genericFeatureFile = new File(BuildModels.rootExtFolder + "tender/tender_cpv.feature");
			File featureFile = new File(BuildModels.rootExtFolder + "tender/model/tender_"+cpv+".feature");
			if (!genericFeatureFile.exists()) {
				featureFile.renameTo(genericFeatureFile);
			} else {
				featureFile.delete();
			}
		}
		return featureList;		
	}

	public static void updateFeatures(Map<String, Integer> features, String[] cpvArray, Map<String, String> cpvTranslations) {

		for (String cpv : cpvArray) {
			String cpvOnly = cpv.substring(cpv.lastIndexOf(":") + 1);
			Integer occurence = features.get(cpv + " " + cpvTranslations.get(cpvOnly));
			if (occurence == null) {
				features.put(cpv + " " + cpvTranslations.get(cpvOnly), 1);
			} else {
				features.put(cpv + " " + cpvTranslations.get(cpvOnly), occurence + 1);
			}
		}
	}

	public class BuildCore extends Thread {
		int core = 1;
		List<String> cpvList = null;
		KnowledgeBase knowledgeBase;
		public BuildCore(int core, List<String> cpvList, KnowledgeBase knowledgeBase) {
			super();
			this.core = core;
			this.cpvList = cpvList;
			this.knowledgeBase = knowledgeBase;
		}

		public void run() {
			for (String cpv : cpvList) {
				try {
					FeatureList featureList = createKnowledgeTenderCpv2(cpv, core);
					createTenderCpvModel(cpv, core, knowledgeBase, featureList);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("Counter="+counter);
		}
	}
	

	public void buildModel() {
		try {
			KnowledgeBase kb = new KnowledgeBase();

			createKnowledgeTenderMultinominalClass(kb);
		
			List<String> cpvListTemp = new ArrayList<>();
			
			for (String discipline : MLearningModels.DISCIPLINES) {
				cpvListTemp.addAll(KnowledgeBase.constructCpvDisciplineList(discipline));
			}
			List<String> cpvList = new ArrayList<>();
			for (String cpv : cpvListTemp) {
				if (!cpvList.contains(cpv)) cpvList.add(cpv);
			}
			
			System.out.println("size before "+ cpvList.size());
			/*
			List<List<String>> partitions = new LinkedList<List<String>>();
			int partitionSize = (int) Math.floor(cpvList.size()/MAXTHREADS);
			for (int i = 0; i < MAXTHREADS; i++) {
				partitions.add(cpvList.subList((partitionSize*i), i == MAXTHREADS -1 ? cpvList.size() : partitionSize*(i+1)));
			}
			*/
			List<List<String>> partitions = Utils.splitList(cpvList, MAXTHREADS);
			
			List<String> testList = new ArrayList<>();
			List<BuildCore> buildCores = new ArrayList<>();
			for (int i = 0; i < MAXTHREADS; i++) {
				BuildCore buildCore = new BuildCore(i, partitions.get(i), kb);
				buildCores.add(buildCore);
				buildCore.start();
				testList.addAll(partitions.get(i));
			}
			
			System.out.println("size after "+ testList.size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	

}
