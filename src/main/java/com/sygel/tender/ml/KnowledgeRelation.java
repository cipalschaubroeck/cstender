/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;

/**
 * A Relation
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class KnowledgeRelation {
	private List<String> verbs;
	private List<KnowledgeEntity> entities;
	private String entitieString;

	public KnowledgeRelation(List<String> verbs, List<KnowledgeEntity> entities, String entitieString) {
		this.verbs = verbs;
		this.entities = entities;
		this.entitieString = entitieString;
	}

	public List<String> getVerbs() {
		return verbs;
	}

	public void setVerbs(List<String> verbs) {
		this.verbs = verbs;
	}

	public List<KnowledgeEntity> getEntities() {
		return entities;
	}

	public void setEntities(List<KnowledgeEntity> entities) {
		this.entities = entities;
	}

	public String getEntitieString() {
		return entitieString;
	}

	public void setEntitieString(String entitieString) {
		this.entitieString = entitieString;
	}

	public String constructEntities() {
		StringBuilder sb = new StringBuilder();
		for (KnowledgeEntity v : entities)
			sb.append((sb.length() > 0 ? "/" : "") + v);
		return sb.toString();
	}

	public String constructVerbs() {
		StringBuilder sb = new StringBuilder();
		for (String v : verbs)
			sb.append((sb.length() > 0 ? "/" : "") + v);
		return sb.toString();
	}

	public void addFeatures(Map<String, Integer> features, Map<String, KnowledgeEntity> entityMap) {
		for (String verb : getVerbs()) {
			for (KnowledgeEntity entity : getEntities()) {
				String e = entity.getEntity();
				KnowledgeEntity ke = entityMap.get(e);
				String group = null;
				String category = null;
				if (ke != null) {
					group = ke.getGroup();
					category = ke.getCategory();
				}
				if (group != null)
					e = group;
				String feature = "REL_" + verb + "_" + e;
				//System.out.println(feature);
				if (!e.equals("")) KnowledgeBase.updateFeatureList(features, feature);
				if (category != null) {
					feature = "REL_" + verb + "_CAT_" + category;
					KnowledgeBase.updateFeatureList(features, feature);
					if (entity.getAdjective() != null && entity.getAdjective().length() > 0) {
					  feature = "REL_" + verb + "_CAT_" + entity.getAdjective() + "_" + category;
					  KnowledgeBase.updateFeatureList(features, feature);
					}
				}
				if (entity.getAdjective() != null && entity.getAdjective().length() > 0) {
					e = entity.getAdjective() + "_" + entity.getEntity();
					ke = entityMap.get(e);
					group = null;
					category = null;
					if (ke != null) {
						group = ke.getGroup();
						category = ke.getCategory();
					}
					if (group != null)
						e = group;
					feature = "REL_" + verb + "_" + e;
					if (!e.equals("")) KnowledgeBase.updateFeatureList(features, feature);
					if (category != null) {
						feature = "REL_" + verb + "_CAT_" + category;
						KnowledgeBase.updateFeatureList(features, feature);
						feature = "REL_" + verb + "_CAT_" + entity.getAdjective() + "_" + category;
						KnowledgeBase.updateFeatureList(features, feature);
					}
				}
			}
		}
	}

	@Override
	public int hashCode() {
		return (constructVerbs() + "@" + constructEntities()).hashCode();
	}

	public boolean equals(Object other) {
		if (other instanceof KnowledgeRelation) {
			KnowledgeRelation r = (KnowledgeRelation) other;
			return (constructVerbs() + "@" + constructEntities()).equals(r.constructVerbs() + "@" + r.constructEntities());
		}
		return false;
	}

}
