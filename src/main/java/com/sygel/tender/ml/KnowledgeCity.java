/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ml;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
/**
 * A city.
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class KnowledgeCity {
	public String countryCode;
	public String state;
	public String province;
	public String nuts;
	public String zipCode;
	public List<String> names;
	boolean headcity = false;
	public static final String[] PROVINCES_BE = { "BE10", "BE21", "BE22",
			"BE23", "BE25", "BE24", "BE32", "BE33", "BE34", "BE35", "BE31" };
	public static final String[] PROVINCES_LONG_NL_BE = { "Brussel",
			"Antwerpen", "Limburg", "Oost-Vlaanderen", "West-Vlaanderen",
			"Vlaams-Brabant", "Henegouwen", "Luik", "Luxemburg", "Namen",
			"Waals-Brabant" };

	
	public static class LengthComparator implements Comparator<KnowledgeCity> {

	    public int compare(KnowledgeCity city1, KnowledgeCity city2) {
	       int l1 = Integer.MAX_VALUE;
	       int l2 = Integer.MAX_VALUE;
	       for (String c : city1.getNames()) if (l1 > c.length()) l1=c.length();
	       for (String c : city2.getNames()) if (l2 > c.length()) l2=c.length();
	       if(l1 <  l2) return 1;
	       if(l1 == l2) return 0;
	       return -1;
	    }
	}
	
	public KnowledgeCity() {
	}

	public void parseBE(String line) {
		String[] info = line.split(",");
		countryCode = "BE";
		state = null;
		zipCode = info[0];
		nuts = info[3].substring(1, info[3].length() - 1).trim();
		province = info[2].substring(1, info[2].length() - 1).trim();
		info = info[1].substring(1, info[1].length() - 1).split(";");
		for (int i = 0; i < info.length; i++) {
			info[i] = info[i].trim();
			if (info[i].matches("[A-Z\\-\\s'ÉÈÇÀÄËÖÜÔÂÊÏ]+"))
				headcity = true;
		}
		names = Arrays.asList(info);

	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public String getNuts() {
		return nuts;
	}

	public void setNuts(String nuts) {
		this.nuts = nuts;
	}

	public boolean isHeadcity() {
		return headcity;
	}

	public void setHeadcity(boolean headcity) {
		this.headcity = headcity;
	}

	public String constructNames() {
		StringBuilder n = new StringBuilder();
		for (String name : names) {
			n.append((n.length() > 0 ? "," : "") + name);
		}
		return n.toString();
	}

	public String convertProvinceToNuts() {
		for (int i = 0; i < PROVINCES_LONG_NL_BE.length; i++)
			if (PROVINCES_LONG_NL_BE[i].equals(province))
				return PROVINCES_BE[i];
		return province;
	}
	
	public String toZipCodeNameString() {
		return zipCode + " " + names.get(0);
	}

	public String toString() {
		return zipCode + ",\"" + constructNames() + "\",\""
				+ convertProvinceToNuts() + "\",\"" + nuts + "\"";
	}
	
	public boolean isCompatibleWithNuts(String nuts) {
		if (nuts != null &&nuts.length() > 0) {
		  String[] ns = nuts.split(" ");
		  boolean arr = false;
		  boolean prov = false;
		  boolean gem = false;
		  for (String n : ns) {
			  if (n.length() == 5) arr = true;
			  if (n.length() == 4) prov = true;
			  if (n.length() == 3 && !n.equals("BEL")) gem = true;
		  }
		  if (arr && nuts.indexOf(getNuts()) == -1) return false;
		  if (prov && nuts.indexOf(getProvince()) == -1) return false;
		  if (gem && nuts.indexOf(getProvince().substring(0,3)) == -1) return false;
		}
		return true;		
	}
}
