/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.security;

import java.security.Principal;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class RolePrincipal implements Principal {
  
  private String name;
  
  public RolePrincipal(String name) {
    super();
    this.name = name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

}
