/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui;


import javax.inject.Named;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@Theme("tender")
@SuppressWarnings("serial")
@CDIUI("login")
@Named("LoginUI")
public class LoginUI extends UI {
	@Override
	protected void init(VaadinRequest request) {
		Label field = new Label("hello");
		setContent(field);
	}

}
