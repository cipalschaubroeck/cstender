/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.notification;

import java.io.File;
import java.text.DateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.Award;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.PreInformation;
import com.sygel.tender.domain.Rectification;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Lot;
import com.sygel.tender.domain.util.Loten;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.award.ContractorDetailWindow;
import com.sygel.tender.ui.component.Attachments;
import com.sygel.tender.ui.component.NotificationDetailComponent;
import com.sygel.tender.ui.government.GovernmentDetailWindow;
import com.sygel.tender.ui.util.SessionUtil;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ClassResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public class NotificationDetailWindow extends Window {
	@Inject
	SearchService searchService;
	@Inject
	KnowledgeService knowledgeService;	
	@Inject
	UserService userService;	
	@Inject
	NotificationDetailComponent notificationDetailComponent;
	@Inject
	GovernmentDetailWindow governmentDetailWindow;
	@Inject
	ContractorDetailWindow contractorDetailWindow;

	I18Service i18Service;
	Tender tender = null;
	
	@Inject
	public NotificationDetailWindow(I18Service i18Service) {		
		this.i18Service = i18Service;
		setClosable(true);
		// setSizeFull();
		setWidth("90%");
		setHeight("90%");
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
	}
	
	@Override
	public void attach() {
	  super.attach();
		setCaption(i18Service.getMessage("TENDER_DETAIL"));
		setContent(notificationDetailComponent);
	}	

	public void initAttachment(Long attachmentId) {
		Tender attachmentTender = searchService.findTenderByAttachmentId(attachmentId);
		Tender tender = searchService.findTenderById(attachmentTender.getId());
		init(tender);
	}

	public void initTender(Long tenderId) {
		Tender tender = searchService.findTenderById(tenderId);
		init(tender);
	}

	public void init(Tender tender) {
		this.tender = tender;
		tender.setLanguage("NL");
		notificationDetailComponent.getTitle().setValue(tender.getTitle());
		notificationDetailComponent.getEssence().setValue(tender.getEssence());
		
		Loten loten = tender.getLoten();
		if (loten != null) {
			StringBuilder lotenBuilder = new StringBuilder();
			int counter = 1;
			for (Lot lot : loten.getLotList()) {
				if ((lot.getNumber() != null && lot.getNumber().length()>0) || (lot.getTitle() != null && lot.getTitle().length()>0))
				  lotenBuilder.append("<b>"+lot.getNumber()+" "+lot.getTitle()+"</b></br>");
				else
					lotenBuilder.append("<b>"+counter+"</b></br>");
				if (lot.getEssence() != null)
				  lotenBuilder.append(lot.getEssence()+"</br>");
				counter++;
			}
			notificationDetailComponent.getLot().setValue(lotenBuilder.toString());
			notificationDetailComponent.getLot().setVisible(true);
		} else {
			notificationDetailComponent.getLot().setValue(null);
			notificationDetailComponent.getLot().setVisible(false);
		}
		
		PreInformation preInformation = tender.getPreInformation();
		if (preInformation != null) {
			String dateString = "";
			if (preInformation.getPublication() != null)
				dateString = DateFormat.getDateInstance(DateFormat.SHORT).format(preInformation.getPublication());
			notificationDetailComponent.getPreInformation().setValue(dateString + " " + i18Service.getMessage("PREINFORMATION"));
			notificationDetailComponent.getPreInformationLayout().setVisible(true);
			notificationDetailComponent.getPreInformationToPdfButton().addClickListener(new Button.ClickListener() {
	      private static final long serialVersionUID = -73954695086117200L;
	      public void buttonClick(final ClickEvent event) {
	      	downloadPreInformation();
	      }
	    });
		} else {
			notificationDetailComponent.getPreInformation().setValue(null);
			notificationDetailComponent.getPreInformationLayout().setVisible(false);			
		}
		VerticalLayout awardVerticalLayout = notificationDetailComponent.getAwardVerticalLayout();
		awardVerticalLayout.removeAllComponents();
		
		Award award = tender.getAward();
		if (award != null) {
			int counter = 1;
			
			for (AwardItem awardItem : award.getAwardItems()) {
				
				Label awardItemTitle = new Label("",ContentMode.HTML);
				if (award.getAwardItems().size() > 1) {
					
				  if ((awardItem.getNumber() != null && awardItem.getNumber().length()>0) || (awardItem.getTitle() != null && awardItem.getTitle().length()>0))
				  	awardItemTitle.setValue("<b><u>"+i18Service.getMessage("TENDER_AWARD")+"</u></b> "+"<b>"+awardItem.getNumber()+" "+awardItem.getTitle()+"</b></br>");
				  else if (award.getAwardItems().size() > 1) {
				  	awardItemTitle.setValue("<b><u>"+i18Service.getMessage("TENDER_AWARD")+"</u></b> "+"<b>"+counter+"</b></br>");
				  }				  
				} else {
					awardItemTitle.setValue("<b><u>"+i18Service.getMessage("TENDER_AWARD")+"</u></b>");
				}
				awardVerticalLayout.addComponent(awardItemTitle);
				
				Button awardButton = new Button(awardItem.getContractor().getName());
				awardButton.setData(awardItem.getContractor());
				awardButton.addClickListener(new Button.ClickListener() {
		      private static final long serialVersionUID = -73954695086117200L;
		      public void buttonClick(final ClickEvent event) {
		    	  openContractorDetail((Contractor)event.getButton().getData());
		      }
		    });
				awardButton.setHtmlContentAllowed(true);				
				awardButton.setStyleName(ValoTheme.BUTTON_QUIET);
				awardVerticalLayout.addComponent(awardButton);
				Label awardItemLabel = new Label("",ContentMode.HTML);
				awardVerticalLayout.addComponent(awardItemLabel);
				
				StringBuilder awardBuilder = new StringBuilder();
				if (awardItem.getValue() != null && awardItem.getValue().length() > 0) 
					awardBuilder.append(awardItem.getValue()+" "+awardItem.getCurrency()+"</br>");
				
				if (awardItem.getContractor().getStreet() != null && awardItem.getContractor().getStreet().length() >0)
				  awardBuilder.append(awardItem.getContractor().getStreet()+"</br>");
				if (awardItem.getContractor().getLocation() != null && awardItem.getContractor().getLocation().length() > 0)
				  awardBuilder.append(knowledgeService.getKnowledgeBase().translateLocation(awardItem.getContractor().getLocation(),null)+"</br>");
				if (awardItem.getContractor().getEmails() != null && awardItem.getContractor().getEmails().length() > 0)
				  awardBuilder.append(awardItem.getContractor().getEmails()+"</br>");
				
				counter++;
				awardItemLabel.setValue(awardBuilder.toString());
			}
			notificationDetailComponent.getAwardToPdfButton().addClickListener(new Button.ClickListener() {
	      private static final long serialVersionUID = -73954695086117200L;
	      public void buttonClick(final ClickEvent event) {
	      	downloadAward();
	      }
	    });
			notificationDetailComponent.getAwardLayout().setVisible(true);
		} else {
			notificationDetailComponent.getAwardLayout().setVisible(false);
		}
		
		List<Rectification> rectificationList = tender.getRectificationsSorted();
		VerticalLayout rectificationListLayout = notificationDetailComponent.getRectificationLayout();
		rectificationListLayout.removeAllComponents();
		
		if (rectificationList != null && rectificationList.size() > 0) {			
			//System.out.println("Init rec "+rectificationList.size());
			int counter = 1;
			for (Rectification rectification : rectificationList) {
				//System.out.println(counter + " " +rectification.getAttachmentName()+" "+rectification.getId()+ " "+ rectification.getTender().getId());
				HorizontalLayout recificationLayout = new HorizontalLayout();
				recificationLayout.setWidth("100%");
				rectificationListLayout.addComponent(recificationLayout);
				Label r = new Label("",ContentMode.HTML);
				String dateString = "";
				if (tender.getOpeningDate() != null)
					dateString = DateFormat.getDateInstance(DateFormat.SHORT).format(rectification.getPublicationDate());

				r.setValue(i18Service.getMessage(rectification.getType().name())+" "+dateString);
				recificationLayout.addComponent(r);
				recificationLayout.setExpandRatio(r, 1);
				Button rectificationButton = new Button();
				rectificationButton.setData(rectification);
				rectificationButton.setStyleName(ValoTheme.BUTTON_QUIET);
				rectificationButton.setIcon(FontAwesome.FILE_PDF_O /*new ClassResource("/com/sygel/tender/resources/images/pdficon_large.png")*/);    
				recificationLayout.addComponent(rectificationButton);
				rectificationButton.addClickListener(new Button.ClickListener() {
		      private static final long serialVersionUID = -73954695086117200L;
		      public void buttonClick(final ClickEvent event) {
		      	Rectification rectification = (Rectification) event.getButton().getData();
		    		File tempFile = searchService.createAttachmentFile(getTender().getNoticeId(), rectification.getAttachmentPdfName(), "pdf");
		    		Attachments.downloadResource(rectification.getAttachmentPdfName(), "pdf", tempFile);	
		      }
				});
				
				
				Button rectificationButtonXML = new Button();
				rectificationButtonXML.setData(rectification);
				rectificationButtonXML.setStyleName(ValoTheme.BUTTON_QUIET);
				rectificationButtonXML.setIcon(FontAwesome.FILE_TEXT_O /*new ClassResource("/com/sygel/tender/resources/images/xlsicon_large.png")*/);    
				recificationLayout.addComponent(rectificationButtonXML);
				rectificationButtonXML.addClickListener(new Button.ClickListener() {
		      private static final long serialVersionUID = -73954695086117200L;
		      public void buttonClick(final ClickEvent event) {
		      	Rectification rectification = (Rectification) event.getButton().getData();
		    		File tempFile = searchService.createAttachmentFile(getTender().getNoticeId(), rectification.getAttachmentName(), "pdf");
		    		Attachments.downloadResource(rectification.getAttachmentName(), "pdf", tempFile);	
		      }
				});
				
				counter++;
			}
			rectificationListLayout.setVisible(true);
		} else {
			rectificationListLayout.setVisible(false);
		}
		
		
		String cpv = knowledgeService.getKnowledgeBase().translateCPV(tender.getCpvNoParent());
		String cpvAI = knowledgeService.getKnowledgeBase().translateCPV(tender.getCpvOnlyAI());
		if (cpv.length() > 1 && cpvAI.length() > 1) cpv = cpv + "</br><i>"+cpvAI+"</i>";
		if (cpv.length() == 0 && cpvAI.length() > 1) cpv = "<i>"+cpvAI+"</i>";
		
		notificationDetailComponent.getCpv().setValue("<u>"+i18Service.getMessage("CPV")+ "</u> "+cpv);

		String location = knowledgeService.getKnowledgeBase().translateLocation(tender.getLocation(), tender.getNutsNoParent());
		String dateString = "";
		if (tender.getOpeningDate() != null)
			dateString = "<b>"+DateFormat.getDateInstance(DateFormat.SHORT).format(tender.getOpeningDate())+"</b>";
		String discipline = knowledgeService.getKnowledgeBase().translateDiscipline(tender.getDisciplinesCpvAI());
		notificationDetailComponent.getOpeningDate().setValue(dateString + " : " + location + " : " + discipline + " " + tender.getNoticeId() + " "+tender.getBda());

		String cat = knowledgeService.getKnowledgeBase().translateCategory(tender.getCategoryAI(), true).trim();
		if (cat.length() > 0) {
		  notificationDetailComponent.getCategory().setValue("<u>"+i18Service.getMessage("CATEGORY")+ "</u> " +cat);
		  notificationDetailComponent.getCategory().setVisible(true);
		} else {
			notificationDetailComponent.getCategory().setValue(null);
			notificationDetailComponent.getCategory().setVisible(false);
		}

		dateString = "-";
		if (tender.getPublication() != null)
			dateString = DateFormat.getDateInstance(DateFormat.SHORT).format(tender.getPublication());		
		notificationDetailComponent.getGovernment().setValue(i18Service.getMessage("PUBLISHED_BY")+"&nbsp;");		
		notificationDetailComponent.getGovernmentButton().setCaption(tender.getGovernment().getName());
		notificationDetailComponent.getGovernmentPublished().setValue("&nbsp;"+i18Service.getMessage("ON")+" "+dateString);
	  notificationDetailComponent.getGovernmentButton().addClickListener(new Button.ClickListener() {
      private static final long serialVersionUID = -73954695086117200L;
      public void buttonClick(final ClickEvent event) {
    	  openGovernmentDetail();
      }
    });
		
		notificationDetailComponent.getAttachments().fillResults(tender.getAttachments(), searchService, tender);
		
		if (tender.getInformation() != null) {
		  notificationDetailComponent.getTenderToPdfButton().addClickListener(new Button.ClickListener() {
        private static final long serialVersionUID = -73954695086117200L;
        public void buttonClick(final ClickEvent event) {
      	  downloadTender();
        }
      });
		  notificationDetailComponent.getTenderToPdfButton().setVisible(true);
		  
		  notificationDetailComponent.getTenderToXmlButton().addClickListener(new Button.ClickListener() {
        private static final long serialVersionUID = -73954695086117200L;
        public void buttonClick(final ClickEvent event) {
      	  downloadXmlTender();
        }
      });
		  notificationDetailComponent.getTenderToXmlButton().setVisible(true);
		} else {
			notificationDetailComponent.getTenderToPdfButton().setVisible(false);
			notificationDetailComponent.getTenderToXmlButton().setVisible(false);
		}
		String f = tender.getFeatures();
		if (f != null) {
		  // notificationDetailComponent.getFeatures().setValue(f.replaceAll(":", " / "));
		  // notificationDetailComponent.getFeatures().setValue("");
		} else {
		  notificationDetailComponent.getFeatures().setValue("");			
		}
		
		notificationDetailComponent.getRememberTenderCheckBox().setValue(userService.isMyTenderFilter(SessionUtil.getUser().getId(), tender));
		notificationDetailComponent.getRememberTenderCheckBox().setCaption(i18Service.getMessage("REMEMBER"));
		notificationDetailComponent.getRememberTenderCheckBox().addValueChangeListener(event ->
				{
					if (notificationDetailComponent.getRememberTenderCheckBox().getValue()) {
						userService.addTenderToMyTenderFilter(SessionUtil.getUser().getId(),this.tender);
					} else
						userService.removeTenderFromMyTenderFilter(SessionUtil.getUser().getId(),this.tender);
				}
		);
	}
	
	private void downloadTender() {
		File tempFile = searchService.createAttachmentFile(tender.getNoticeId(), tender.getInformation().getAttachmentPdfName(), "pdf");
		Attachments.downloadResource(tender.getInformation().getAttachmentPdfName(), "pdf", tempFile);	
	}
	
	private void downloadXmlTender() {
		File tempFile = searchService.createAttachmentFile(tender.getNoticeId(), tender.getInformation().getAttachmentName(), "xml");
		Attachments.downloadResource(tender.getInformation().getAttachmentName(), "xml", tempFile);	
	}
	
	private void downloadAward() {
		File tempFile = searchService.createAttachmentFile(tender.getNoticeId(), tender.getAward().getAttachmentPdfName(), "pdf");
		Attachments.downloadResource(tender.getAward().getAttachmentPdfName(), "pdf", tempFile);	
	}
	
	private void downloadPreInformation() {
		File tempFile = searchService.createAttachmentFile(tender.getNoticeId(), tender.getPreInformation().getAttachmentPdfName(), "pdf");
		Attachments.downloadResource(tender.getPreInformation().getAttachmentPdfName(), "pdf", tempFile);	
	}
	
	private void openGovernmentDetail() {
		if (!UI.getCurrent().getWindows().contains(governmentDetailWindow))
		  UI.getCurrent().addWindow(governmentDetailWindow);
		governmentDetailWindow.initGovernment(tender.getGovernment().getId(), this);
		governmentDetailWindow.focus();
		
	}
	
	private void openContractorDetail(Contractor contractor) {
		if (!UI.getCurrent().getWindows().contains(contractorDetailWindow))
		  UI.getCurrent().addWindow(contractorDetailWindow);
		contractorDetailWindow.initContractor(contractor.getId(), this);
		contractorDetailWindow.focus();
		
	}

	public Tender getTender() {
		return tender;
	}

	public void setTender(Tender tender) {
		this.tender = tender;
	}

}
