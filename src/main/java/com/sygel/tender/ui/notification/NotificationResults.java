/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.notification;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.report.jasper.JasperTableExport;
import com.sygel.tender.report.jasper.JasperTenderExport;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.ui.component.BasicTable;
import com.sygel.tender.ui.util.HyperlinkExcelExport;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class NotificationResults extends CustomComponent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private VerticalLayout screen = null;
	private BasicTable table = null;
	private Label count = new Label("");
	private BeanItemContainer<Tender> tableDataSource;
	private String name = null;
	
	@Inject
	NotificationDetailWindow notificationDetailWindow;	
	
	I18Service i18Service;
	
	@Inject
	public NotificationResults(I18Service i18Service)
	{
		this.i18Service = i18Service;
		table = new BasicTable();
		table.setSizeFull();
		table.setSelectable(true);
		table.addActionHandler(new TransactionsActionHandler());
		table.addItemClickListener(new ItemClickListener()
		{
			private static final long serialVersionUID = 1L;

			@Override
			public void itemClick(ItemClickEvent event)
			{
			}
		});

		Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
		downloadButton.setStyleName(Reindeer.BUTTON_LINK);
		downloadButton.addClickListener(new ClickListener()
		{
			private static final long serialVersionUID = 1L;
			private HyperlinkExcelExport excelExport;

			public void buttonClick(final ClickEvent event)
			{
				excelExport = new HyperlinkExcelExport(table);				
				excelExport.setBaseUrl("http://localhost:8090/tender/secure/#!MYTENDER/"+getName());
				excelExport.setReportTitle(i18Service.getMessage("TENDERS"));
				excelExport.export();
			}
		});
		
		
		Button pdfButton = new Button(i18Service.getMessage("TO_PDF"));
		pdfButton.setStyleName(Reindeer.BUTTON_LINK);
		pdfButton.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -73954695086117200L;
            private JasperTenderExport jasperPdfExport;
            public void buttonClick(final ClickEvent event) {
              jasperPdfExport = new JasperTenderExport(tableDataSource.getItemIds());
            	jasperPdfExport.setReportTitle(i18Service.getMessage("TENDERS"));
            	jasperPdfExport.setReportFilter(getName());
            	jasperPdfExport.export();
            }
        });

		HorizontalLayout bottom = new HorizontalLayout(count, downloadButton, pdfButton);
		bottom.setSpacing(true);
		screen = new VerticalLayout(table, bottom);
		screen.setMargin(true);
		screen.setSpacing(true);
		setSizeFull();
		screen.setSizeFull();
		screen.setExpandRatio(table, 1);
		setCompositionRoot(screen);
	}

	public void fillResults(Collection<Tender> tendersFound)
	{
		tableDataSource = new BeanItemContainer<Tender>(Tender.class);
		table.setContainerDataSource(tableDataSource);
		table.setColumnCollapsingAllowed(true);
		table.setVisibleColumns("openingDate", "title", "essence", "locationTranslation","disciplinesString", "categoryString", "cpvNoParentAI" , "publication");
		
		table.setColumnHeaders(
				i18Service.getMessage("OPENING"),
				i18Service.getMessage("TITLE"),
				i18Service.getMessage("ESSENCE"),
				i18Service.getMessage("LOCATION"),
				i18Service.getMessage("DISCIPLINE"),
				i18Service.getMessage("CATEGORY"),
				i18Service.getMessage("CPV"),
				i18Service.getMessage("PUBLICATION"));
		Object[] columnIds = table.getVisibleColumns();
		
		for (int counter = 0;counter < columnIds.length;counter++) {
			table.setColumnCollapsingAllowed(true);
			table.setColumnWidth(columnIds[counter], counter == 1 ? 600 : 100);
			table.setColumnCollapsed(columnIds[counter], counter == 2 ? true : false);
		}
		tableDataSource.addAll(tendersFound);
		
		count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + tendersFound.size());
	}
	
	public void selectNotification(Long tenderId)
	{
		for (Tender tender : tableDataSource.getItemIds()) {
			if (tender.getId().equals(tenderId)) {
		    table.select(tender);
		    table.setCurrentPageFirstItemId(tender);
		    break;
			}
		}
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private class TransactionsActionHandler implements Handler {

        private final Action details = new Action(i18Service.getMessage("TENDER_DETAIL"));

        @Override
        public void handleAction(final Action action, final Object sender,
                final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long tenderId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(notificationDetailWindow))
                      UI.getCurrent().addWindow(notificationDetailWindow);
                    notificationDetailWindow.initTender(tenderId);
                    notificationDetailWindow.focus();                    
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
	
}