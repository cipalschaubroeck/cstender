/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui;

import javax.inject.Inject;

import com.google.common.eventbus.Subscribe;
import com.sygel.tender.domain.User;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.ui.event.TenderEvent.PostViewChangeEvent;
import com.sygel.tender.ui.event.TenderEventBus;
import com.sygel.tender.ui.user.UserDetailWindow;
import com.sygel.tender.ui.util.SessionUtil;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public final class TenderMenu extends CustomComponent {
	@Inject
	I18Service i18Service;
	@Inject
	UserDetailWindow userDetailWindow;
	public static final String ID = "tender-menu";
	public static final String REPORTS_BADGE_ID = "tender-menu-reports-badge";
	public static final String NOTIFICATIONS_BADGE_ID = "tender-menu-notifications-badge";
	private static final String STYLE_VISIBLE = "valo-menu-visible";
	private MenuItem settingsItem;

	public TenderMenu() {
		setPrimaryStyleName(ValoTheme.MENU_ROOT);
		setId(ID);
		setSizeUndefined();

		// There's only one DashboardMenu per UI so this doesn't need to be
		// unregistered from the UI-scoped TenderEventBus.
		TenderEventBus.register(this);

	}

	private Component buildContent() {
		final CssLayout menuContent = new CssLayout();
		menuContent.addStyleName("sidebar");
		menuContent.addStyleName(ValoTheme.MENU_PART);
		menuContent.addStyleName("no-vertical-drag-hints");
		menuContent.addStyleName("no-horizontal-drag-hints");
		menuContent.setWidth(null);
		menuContent.setHeight("100%");

		menuContent.addComponent(buildTitle());
		menuContent.addComponent(buildUserMenu());
		menuContent.addComponent(buildToggleButton());
		menuContent.addComponent(buildMenuItems());

		return menuContent;
	}

	private Component buildTitle() {
		Label logo = new Label(i18Service.getMessage("PRODUCT_NAME"), ContentMode.HTML);
		logo.setSizeUndefined();
		HorizontalLayout logoWrapper = new HorizontalLayout(logo);
		logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
		logoWrapper.addStyleName(ValoTheme.MENU_TITLE);
		return logoWrapper;
	}

	private User getCurrentUser() { 
	 return SessionUtil.getUser(); 
	}
	 
	private Component buildUserMenu() {
		final MenuBar settings = new MenuBar();
		settings.addStyleName("user-menu");
		final User user = getCurrentUser();
		settingsItem = settings.addItem(user.getFirstname() + " "+user.getName(), null/*
																										 * new ThemeResource(
																										 * "img/profile-pic-300px.jpg"
																										 * )
																										 */, null);
		settingsItem.addItem(i18Service.getMessage("EDIT_PROFILE"), new Command() {
			@Override
			public void menuSelected(final MenuItem selectedItem) {
				// ProfilePreferencesWindow.open(user, false);
				
        if (!UI.getCurrent().getWindows().contains(userDetailWindow))
          UI.getCurrent().addWindow(userDetailWindow);
        userDetailWindow.initUser(user);
        userDetailWindow.focus();

				
			}
		});
		settingsItem.addSeparator();
		settingsItem.addItem(i18Service.getMessage("SIGN_OUT"), new Command() {
			@Override
			public void menuSelected(final MenuItem selectedItem) {
	        getUI().getSession().close();

	        // Invalidate underlying session instead if login info is stored there
	        VaadinService.getCurrentRequest().getWrappedSession().invalidate();

	        // Redirect to avoid keeping the removed UI open in the browser
	        getUI().getPage().setLocation("/tender");
	    }
		});
		return settings;
	}

	private Component buildToggleButton() {
		Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
					getCompositionRoot().removeStyleName(STYLE_VISIBLE);
				} else {
					getCompositionRoot().addStyleName(STYLE_VISIBLE);
				}
			}
		});
		valoMenuToggleButton.setIcon(FontAwesome.LIST);
		valoMenuToggleButton.addStyleName("valo-menu-toggle");
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
		return valoMenuToggleButton;
	}

	private Component buildMenuItems() {
		CssLayout menuItemsLayout = new CssLayout();
		menuItemsLayout.addStyleName("valo-menuitems");
		User user = getCurrentUser();
		for (final TenderViewType view : TenderViewType.values()) {
			if (user.isUserInRole(view.getRole())) {
			  Component menuItemComponent = new ValoMenuItemButton(view);
			  menuItemsLayout.addComponent(menuItemComponent);
			}
		}
		return menuItemsLayout;

	}

	private Component buildBadgeWrapper(final Component menuItemButton, final Component badgeLabel) {
		CssLayout dashboardWrapper = new CssLayout(menuItemButton);
		dashboardWrapper.addStyleName("badgewrapper");
		dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
		badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
		badgeLabel.setWidthUndefined();
		badgeLabel.setVisible(false);
		dashboardWrapper.addComponent(badgeLabel);
		return dashboardWrapper;
	}

	@Override
	public void attach() {
		super.attach();
		setCompositionRoot(buildContent());
		// updateNotificationsCount(null);
	}

	
	 @Subscribe 
	 public void postViewChange(final PostViewChangeEvent event) {
	   //After a successful view change the menu can be hidden in mobile view.
	   getCompositionRoot().removeStyleName(STYLE_VISIBLE); 
	  }
	
	
	public final class ValoMenuItemButton extends Button {

		private static final String STYLE_SELECTED = "selected";

		private final TenderViewType view;

		public ValoMenuItemButton(final TenderViewType view) {
			this.view = view;
			setPrimaryStyleName(ValoTheme.MENU_ITEM);
			setIcon(view.getIcon());
			
			setCaption(i18Service.getMessage(view.getViewName()));
			
			TenderEventBus.register(this);
			addClickListener(new ClickListener() {
				@Override
				public void buttonClick(final ClickEvent event) {
					UI.getCurrent().getNavigator().navigateTo(view.getViewName());
				}
			});

		}

		@Subscribe
		public void postViewChange(final PostViewChangeEvent event) {
			removeStyleName(STYLE_SELECTED);
			if (event.getView() == view) {
				addStyleName(STYLE_SELECTED);
			}
		}

	}

}
