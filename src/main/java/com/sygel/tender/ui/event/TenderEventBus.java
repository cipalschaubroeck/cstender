/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.sygel.tender.ui.TenderUI;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class TenderEventBus implements SubscriberExceptionHandler {

  private final EventBus eventBus = new EventBus(this);

  public static void post(final Object event) {
    TenderUI.getTenderEventbus().eventBus.post(event);
  }

  public static void register(final Object object) {
  	TenderUI.getTenderEventbus().eventBus.register(object);
  }

  public static void unregister(final Object object) {
  	TenderUI.getTenderEventbus().eventBus.unregister(object);
  }

  @Override
  public final void handleException(final Throwable exception, final SubscriberExceptionContext context) {
      exception.printStackTrace();
  }
}