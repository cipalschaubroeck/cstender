/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.event;

import com.sygel.tender.ui.TenderViewType;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class TenderEvent {
	public static final class PostViewChangeEvent {
    private final TenderViewType view;

    public PostViewChangeEvent(final TenderViewType view) {
        this.view = view;
    }

    public TenderViewType getView() {
        return view;
    }
}
}
