/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;

import com.sygel.tender.ui.build.BuildTenderView;
import com.sygel.tender.ui.notification.NotificationView;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.cdi.NormalUIScoped;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@NormalUIScoped
public class NavigationServiceImpl implements NavigationService {

	private static final long serialVersionUID = 1L;

	@Inject
	private CDIViewProvider viewProvider;
	
	@Inject
	private TenderMenu tenderMenu;

	
	private CssLayout mainPanel = new CssLayout();
	
	@Inject
	private @Named("TenderUI") UI ui;

	@PostConstruct
	public void initialize() {
		//UI ui = UI.getCurrent();
		if (ui.getNavigator() == null) {

			mainPanel.setStyleName("mainPanel");
			mainPanel.setSizeFull();

			final HorizontalLayout layout = new HorizontalLayout();
			ui.setContent(layout);
			layout.setSizeFull();

			ui.getPage().setTitle("Sygel");

			layout.addComponent(tenderMenu);
			layout.addComponent(mainPanel);
			layout.setExpandRatio(mainPanel, 1);

			Navigator navigator = new TenderNavigator(ui, mainPanel);
			navigator.addProvider(viewProvider);
		}
	}

	@Override
	public void onNavigationEvent(@Observes NavigationEvent event) {
		try {
			//UI ui = UI.getCurrent();
			ui.getNavigator().navigateTo(event.getNavigateTo());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}