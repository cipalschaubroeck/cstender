/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui;

import org.apache.log4j.Logger;

import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.sygel.tender.domain.User;
import com.sygel.tender.ui.admin.TaskAdminView;
import com.sygel.tender.ui.admin.UserAdminView;
import com.sygel.tender.ui.attachment.AttachmentView;
import com.sygel.tender.ui.award.AwardView;
import com.sygel.tender.ui.contractor.ContractorView;
import com.sygel.tender.ui.build.BuildTenderView;
import com.sygel.tender.ui.notification.MyNotificationView;
import com.sygel.tender.ui.notification.NotificationView;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public enum TenderViewType {
    TENDER("TENDER", NotificationView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    MYTENDER("MYTENDER", MyNotificationView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    AWARD("AWARD", AwardView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    CONTRACTOR("CONTRACTOR", ContractorView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    GOVERNMENT("GOVERNMENT", AwardView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    ATTACHMENT("ATTACHMENT", AttachmentView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    //BUILD_TENDER("BUILD_TENDER", BuildTenderView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    USER_ADMIN("USER_ADMIN", UserAdminView.class, FontAwesome.USER, true, User.ROLE_ADMIN),
    TASK_ADMIN("TASK_ADMIN", TaskAdminView.class, FontAwesome.TASKS, true, User.ROLE_ADMIN)
    ;
    private final Logger logger = Logger.getLogger(TenderViewType.class);
    private final String viewName;
    private final Class<? extends View> viewClass;
    private final Resource icon;
    private final boolean stateful;
    private final String role;
    private TenderViewType(final String viewName,
            final Class<? extends View> viewClass, final Resource icon,
            final boolean stateful, String role) {
        this.viewName = viewName;
        this.viewClass = viewClass;
        this.icon = icon;
        this.stateful = stateful;
        this.role = role;
    }

    public boolean isStateful() {
        return stateful;
    }

    public String getViewName() {
        return viewName;
    }

    public Class<? extends View> getViewClass() {
        return viewClass;
    }

    public Resource getIcon() {
        return icon;
    }

    public String getRole() {
			return role;
		}

		public static TenderViewType getByViewName(final String viewName) {
    	TenderViewType result = null;
      for (TenderViewType viewType : values()) {
        if (viewType.getViewName().equals(viewName)) {
          result = viewType;
          break;
        }
      }
      return result;
    }
}