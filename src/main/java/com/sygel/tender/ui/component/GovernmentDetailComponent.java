/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.component;

import javax.inject.Inject;

import com.sygel.tender.ui.build.TitleResults;
import com.vaadin.server.ClassResource;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class GovernmentDetailComponent extends VerticalLayout {
	Label name = null;
	Label details = null;
	
	Panel contentPanel = null;
	Panel contactPanel = null;
	VerticalLayout left = null;
	
	private TitleResults titleResults;

	@Inject
	public GovernmentDetailComponent(TitleResults titleResults) {
		this.titleResults = titleResults;
		setMargin(true);
		setWidth("100%");
		setHeight("100%");
		contentPanel = new Panel();

		contentPanel.setWidth("100%");
		contentPanel.setHeight("100%");
	  left = new VerticalLayout();
		left.setMargin(new MarginInfo(true, true, true, true));
		left.setHeight("100%");
		contentPanel.setContent(left);
		
		addComponent(contentPanel);

		HorizontalLayout titleLayout = new HorizontalLayout();
		titleLayout.setWidth("100%");
		left.addComponent(titleLayout);		
		
		name = new Label("",ContentMode.HTML);
		name.addStyleName(ValoTheme.LABEL_LARGE);
		titleLayout.addComponent(name);
		titleLayout.setExpandRatio(name, 1.0f);
		
		contactPanel = new Panel();
		
		contactPanel.setHeight(2,Unit.INCH);
		details = new Label("",ContentMode.HTML);
		contactPanel.setContent(details);
		left.addComponent(contactPanel);
		
		Label gap = new Label();
		gap.setHeight(".5em");
		left.addComponent(gap);
		
		titleResults.setSideMargins(false);
	}
	
	@Override
	public void attach() {
	  super.attach();
	  left.addComponent(titleResults);	  
	  left.setExpandRatio(titleResults, 1.0f);
	}

	public Label getName() {
		return name;
	}

	public void setName(Label name) {
		this.name = name;
	}



	public Label getDetails() {
		return details;
	}

	public void setDetails(Label details) {
		this.details = details;
	}

	public TitleResults getTitleResults() {
		return titleResults;
	}

	public void setTitleResults(TitleResults titleResults) {
		this.titleResults = titleResults;
	}
}
