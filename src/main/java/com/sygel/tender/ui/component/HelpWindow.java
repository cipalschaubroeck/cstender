/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.component;

import javax.inject.Inject;

import com.sygel.tender.domain.Filter;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.service.SearchService;
import com.vaadin.ui.Window;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public class HelpWindow extends Window {
	@Inject
	I18Service i18Service;
		
	@Inject
	HelpComponent helpComponent;
	
	public HelpWindow() {		
		setClosable(true);
		// setSizeFull();
		setWidth("90%");
		setHeight("90%");
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);

	}
	
	@Override
	public void attach() {
	  super.attach();
		setCaption(i18Service.getMessage("HELP"));
		setContent(helpComponent);
	}	
	
	public void setHelpText(String helpText) {
		helpComponent.setHelpText(helpText);
	}

}
