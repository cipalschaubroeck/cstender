/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.component;

import javax.inject.Inject;

import com.sygel.tender.domain.Filter;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.service.SearchService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Window;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public class BuildSearchWindow extends Window {
	@Inject
	SearchService searchService;
	@Inject
	KnowledgeService knowledgeService;
	@Inject
	I18Service i18Service;
		
	@Inject
	BuildSearchComponent buildSearchComponent;
	
	public BuildSearchWindow() {		
		setClosable(true);
		// setSizeFull();
		setWidth("90%");
		setHeight("90%");
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);

	}
	
	@Override
	public void attach() {
	  super.attach();
		setCaption(i18Service.getMessage("BUILD_QUERY"));
		setContent(buildSearchComponent);
	}	

	public void initSearchFilter(Filter searchFilter) {
		buildSearchComponent.setSearchFilter(searchFilter);	
	}
	
	public Filter getSearchFilter() {
		return buildSearchComponent.getSearchFilter();
	}
}
