/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.component;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class HelpComponent extends VerticalLayout {
	
	Panel contentPanel = null;
	VerticalLayout  contentLayout = null;
	Label htmlText = null;
	
	public HelpComponent() {
		super();
		setMargin(true);
		setWidth("100%");
		setHeight("100%");
		
		contentPanel = new Panel();

		contentPanel.setWidth("100%");
		contentPanel.setHeight("100%");
	  contentLayout = new VerticalLayout();
	  contentLayout.setMargin(new MarginInfo(true, true, true, true));
		contentPanel.setContent(contentLayout);
		addComponent(contentPanel);
    htmlText = new Label("",ContentMode.HTML);
    contentLayout.addComponent(htmlText);
	}

	public VerticalLayout getContentLayout() {
		return contentLayout;
	}
	
	public void setHelpText(String helpText) {
		htmlText.setValue(helpText);
	}
}
