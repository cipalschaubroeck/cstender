/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.user;

import javax.inject.Inject;

import com.sygel.tender.domain.User;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.component.UserComponent;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.Window;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public class UserDetailWindow extends Window {
	UserService userService;
	
	I18Service i18Service;
	
	User user = null;
	
	final BeanFieldGroup<User> binder = new BeanFieldGroup<User>(User.class);
		
	UserComponent userComponent;

	@Inject
	public UserDetailWindow(I18Service i18Service, UserComponent userComponent, UserService userService) {		
		this.i18Service = i18Service;
		this.userComponent = userComponent;
		this.userService = userService;
		setClosable(true);
		// setSizeFull();
		setWidth("70%");
		setHeight("70%");
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
		
	  // Form for editing the bean
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_FIRSTNAME"), "firstname"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_NAME"), "name"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_COMPANY"), "company"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_EMAIL"), "email"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_STREET"), "street"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_ZIPCODE"), "zipcode"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_CITY"), "city"));
		userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_COUNTRY"), "country"));

		// Buffer the form content
		binder.setBuffered(true);
		
		Button okButton = new Button("OK");
		okButton.addClickListener(
				event -> {
					try {
			      binder.commit();
			      userService.saveUser(binder.getItemDataSource().getBean());
			      close();
		      } catch (Exception e) {
			    e.printStackTrace();
		}});
		userComponent.getContentLayout().addComponent(okButton);
	}
	
	@Override
	public void attach() {
	  super.attach();
		setCaption(i18Service.getMessage("USER_EDIT"));		
		setContent(userComponent);
	}	

	public void initUser(User user) {
	  user = userService.findUser(user.getEmail(), false);
		binder.setItemDataSource(user);
	}
	public void initUser(Long userId) {
	  user = userService.findUser(userId);
		binder.setItemDataSource(user);
	}
}
