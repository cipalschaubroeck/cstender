/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui;

import java.security.Principal;
import java.util.Locale;

import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.sygel.tender.domain.User;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.event.TenderEventBus;
import com.sygel.tender.ui.util.SessionUtil;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.UI;

/**
 * Main UI
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@Theme("tender")
@SuppressWarnings("serial")
@CDIUI("secure")
@Named("TenderUI")
@DeclareRoles({ User.ROLE_ADMIN, User.ROLE_ADMIN })
//@Push(PushMode.AUTOMATIC)
// @JavaScript({"vaadin://js/jquery-1.7.1.min.js", "vaadin://js/tender.js"})
public class TenderUI extends UI {
	private final Logger logger = Logger.getLogger(TenderUI.class);

	@Inject
	private javax.enterprise.event.Event<NavigationEvent> navigationEvent;
	
	@Inject
	UserService userService;

	private final TenderEventBus tenderEventbus = new TenderEventBus();

	@Override
	protected void init(VaadinRequest request) {
		// Page.getCurrent().getJavaScript().execute("initTender();");
		logger.info("init TenderUI pathinfo:"+request.getPathInfo());
		logger.info("init TenderUI getQuery:"+getPage().getLocation().getQuery());
		
		java.security.Principal p =request.getUserPrincipal();
		
		if (request.isUserInRole(User.ROLE_ADMIN) )
			logger.info("init TenderUI ADMIN ROLE");
		if (request.isUserInRole(User.ROLE_USER) )
			logger.info("init TenderUI USER ROLE");
		
		User user = userService.findUser( request.getUserPrincipal().getName(), true);
		SessionUtil.storeUser(user);
		Locale.setDefault(new Locale("nl", "BE"));
		Responsive.makeResponsive(this);
		TenderEventBus.register(this);
		String viewName = request.getParameter("v-loc");
		if (viewName == null)
			viewName = TenderViewType.TENDER.getViewName();
		else if (!viewName.contains("#!"))
			viewName = TenderViewType.TENDER.getViewName();
		else
			viewName = viewName.replaceFirst(".*#!", "");
    //if (viewName.contains("/")) viewName = viewName.substring(0,viewName.indexOf("/"));
		logger.info("initially navigating to " + viewName);
		navigationEvent.fire(new NavigationEvent(viewName));
	}

	public static TenderEventBus getTenderEventbus() {
		return ((TenderUI) getCurrent()).tenderEventbus;
	}
}
