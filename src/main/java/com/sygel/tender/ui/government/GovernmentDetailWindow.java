/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.government;

import java.io.File;
import java.text.DateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.Contact;
import com.sygel.tender.domain.Award;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.PreInformation;
import com.sygel.tender.domain.Rectification;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Lot;
import com.sygel.tender.domain.util.Loten;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.ui.component.Attachments;
import com.sygel.tender.ui.component.GovernmentDetailComponent;
import com.sygel.tender.ui.component.NotificationDetailComponent;
import com.sygel.tender.ui.notification.NotificationDetailWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ClassResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@SuppressWarnings({ "serial", "unchecked" })
public class GovernmentDetailWindow extends Window {
	@Inject
	SearchService searchService;
	@Inject
	KnowledgeService knowledgeService;
	@Inject
	I18Service i18Service;
	
	@Inject
	GovernmentDetailComponent governmentDetailComponent;
	
	NotificationDetailWindow notificationDetailWindow;

	public GovernmentDetailWindow() {		
		setClosable(true);
		setWidth("90%");
		setHeight("90%");		
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);	
	}
	
	@Override
	public void attach() {
	  super.attach();
	  setCaption(i18Service.getMessage("GOVERNMENT_DETAIL"));
	  setContent(governmentDetailComponent);
	}

	public void initGovernment(Long governmentId, NotificationDetailWindow notificationDetailWindow) {
		this.notificationDetailWindow =notificationDetailWindow;
		Government government = searchService.findGovernmentById(governmentId);
		init(government);
	}

	public void init(Government government) {		
    if (government !=null) {
    	governmentDetailComponent.getName().setValue(government.getName());
    	governmentDetailComponent.getTitleResults().fillResults(government.getTenders(), notificationDetailWindow);
    	StringBuilder adressBuilder = new StringBuilder();
			if (government.getStreet() != null && government.getStreet().length() >0)
				adressBuilder.append(government.getStreet()+"</br>");
			if (government.getLocation() != null && government.getLocation().length() > 0)
				adressBuilder.append(knowledgeService.getKnowledgeBase().translateLocation(government.getLocation(),null)+"</br>");

    	if (government.getContacts() != null) {
    		for (Contact contact : government.getContacts()) {
    			boolean nameNull = true;
    			if (contact.getName() != null && contact.getName().length() > 0) {
  				  adressBuilder.append("<u>"+contact.getName()+"</u></br>");
  				  nameNull = false;
    			}
  				if (contact.getAttention() != null && contact.getAttention().length() > 0)
  				  adressBuilder.append((nameNull ? "<u>" : "") +contact.getAttention()+(nameNull ? "</u>" : "")+"</br>");
  				if (contact.getPhones()!= null && contact.getPhones().length() > 0)
  				  adressBuilder.append(contact.getPhones()+"</br>");
  				if (contact.getFaxes()!= null && contact.getFaxes().length() > 0)
  				  adressBuilder.append(contact.getFaxes()+"</br>");
  				if (contact.getEmails()!= null && contact.getEmails().length() > 0)
  				  adressBuilder.append(contact.getEmails()+"</br>");  				
    		}
    	}
    	governmentDetailComponent.getDetails().setValue(adressBuilder.toString());
    }
	}
}
