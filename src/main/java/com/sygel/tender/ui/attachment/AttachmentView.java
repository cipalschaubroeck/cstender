/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.attachment;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

//import org.vaadin.maddon.FilterableListContainer;











import javax.inject.Inject;

import com.google.common.eventbus.Subscribe;
import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.ui.component.Attachments;
import com.sygel.tender.ui.component.HelpWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Search text in attachments
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@CDIView(value = "ATTACHMENT")
@SuppressWarnings({ "serial", "unchecked" })
public final class AttachmentView extends VerticalLayout implements View {
	@Inject
	private SearchService searchService;	
	
	private I18Service i18Service;

	@Inject
	private AttachmentResults attachmentResults;

	@Inject
	HelpWindow helpWindow;

	private Button helpButton;

	private boolean first = true;

	@Inject
	public AttachmentView(I18Service i18Service) {
		this.i18Service = i18Service;
		setSizeFull();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
		setWidth("100%");
		addComponent(buildToolbar());
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		Collection<Attachment> attachments = searchService.findAttachments("");
		if (first) {
			first = false;
			addComponent(attachmentResults);
			setExpandRatio(attachmentResults, 2);
		}
		attachmentResults.fillResults(attachments);
	}

	@Override
	public void detach() {
		super.detach();
		// A new instance of TransactionsView is created every time it's
		// navigated to so we'll need to clean up references to it on detach.
		// DashboardEventBus.unregister(this);
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.setMargin(new MarginInfo(true, true, false, true));
		header.setWidth("100%");
		// Responsive.makeResponsive(header);
	
		Component filter = buildFilter();
		header.addComponent(filter);

		header.setExpandRatio(filter, 1.0f);
		
		helpButton = buildHelpButton();
		header.addComponent(helpButton);


		return header;
	}
	
	private Button buildHelpButton() {
		final Button helpQueryButton = new Button(i18Service.getMessage("HELP"));
		helpQueryButton.addStyleName(ValoTheme.BUTTON_QUIET);

		helpQueryButton.setIcon(FontAwesome.QUESTION_CIRCLE);
		helpQueryButton.setDescription(i18Service.getMessage("HELP_ATTACHMENT_QUERY_DESCRIPTION"));

		helpQueryButton.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				helpWindow.setHelpText(i18Service.getMessage("HELP_ATTACHMENT_QUERY_TEXT"));
				if (!UI.getCurrent().getWindows().contains(helpWindow))
					UI.getCurrent().addWindow(helpWindow);				
				helpWindow.focus();
				helpWindow.addCloseListener(new Window.CloseListener() {
					public void windowClose(CloseEvent e) {
					}
				});
			}
		});
		helpQueryButton.setEnabled(true);
		return helpQueryButton;
	}

	private Component buildFilter() {
		final TextField filter = new TextField();
		filter.setWidth("100%");
		filter.addTextChangeListener(new TextChangeListener() {
			@Override
			public void textChange(final TextChangeEvent event) {
				Collection<Attachment> governmentItems = searchService.findAttachments(event.getText());
				attachmentResults.fillResults(governmentItems);
			}
		});

		filter.setInputPrompt(i18Service.getMessage("FIND_ATTACHMENT"));
		filter.setIcon(FontAwesome.SEARCH);
		filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		filter.addShortcutListener(new ShortcutListener("Clear", KeyCode.ESCAPE, null) {
			@Override
			public void handleAction(final Object sender, final Object target) {
				filter.setValue("");
			}
		});
		return filter;
	}
}
