/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.contractor;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;

import com.sygel.tender.domain.Award;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.ui.award.ContractorDetailWindow;
import com.sygel.tender.ui.component.BasicTable;
import com.sygel.tender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class ContractorResults extends CustomComponent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private VerticalLayout screen = null;
	private BasicTable table = null;
	private Label count = new Label("");
	private I18Service i18Service;
	
	@Inject
	ContractorDetailWindow contractorDetailWindow;
	
	@Inject
	NotificationDetailWindow notificationDetailWindow;
	
	@Inject
	KnowledgeService knowledgeService;
	
	@Inject
	public ContractorResults(I18Service i18Service)
	{
		this.i18Service = i18Service;
		table = new BasicTable();
		table.setSizeFull();
		table.setSelectable(true);
		table.addActionHandler(new TransactionsActionHandler());		
		table.addItemClickListener(new ItemClickListener()
		{
			private static final long serialVersionUID = 1L;

			@Override
			public void itemClick(ItemClickEvent event)
			{
			}
		});

		Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
		downloadButton.setStyleName(Reindeer.BUTTON_LINK);
		downloadButton.addClickListener(new ClickListener()
		{
			private static final long serialVersionUID = 1L;
			private ExcelExport excelExport;

			public void buttonClick(final ClickEvent event)
			{
				excelExport = new ExcelExport(table);
				excelExport.excludeCollapsedColumns();
				excelExport.setReportTitle(i18Service.getMessage("CONTRACTORS"));
				excelExport.export();
			}
		});
		
		HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
		bottom.setSpacing(true);
		screen = new VerticalLayout(table, bottom);
		screen.setMargin(true);
		screen.setSpacing(true);
		setSizeFull();
		screen.setSizeFull();
		screen.setExpandRatio(table, 1);
		setCompositionRoot(screen);
	}

	public void fillResults(Collection<Contractor> contractorsFound)
	{
		for (Contractor contractor : contractorsFound)
			contractor.setLocationString(knowledgeService.getKnowledgeBase().translateLocation(contractor.getLocation(), null));
		BeanItemContainer<Contractor> contractorContainer = new BeanItemContainer<Contractor>(Contractor.class);
		table.setContainerDataSource(contractorContainer);
		table.setVisibleColumns("name","street","locationString","emails","phones");		
		table.setColumnHeaders(
				i18Service.getMessage("CONTRACTOR"),
				i18Service.getMessage("USER_STREET"),
				i18Service.getMessage("LOCATION"),
				i18Service.getMessage("USER_EMAIL"),
				i18Service.getMessage("PHONE"));
		Object[] columnIds = table.getVisibleColumns();			
		contractorContainer.addAll(contractorsFound);		
		count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + contractorContainer.size());
	}
	
	private class TransactionsActionHandler implements Handler {

        private final Action details = new Action(i18Service.getMessage("CONTRACTOR_DETAILS"));

        @Override
        public void handleAction(final Action action, final Object sender,
                final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long contractorId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(contractorDetailWindow))
                      UI.getCurrent().addWindow(contractorDetailWindow);
                    contractorDetailWindow.focus();
                    contractorDetailWindow.initContractor(contractorId, notificationDetailWindow);
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
}