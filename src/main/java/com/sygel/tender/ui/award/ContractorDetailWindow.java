/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.award;

import java.io.File;
import java.text.DateFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.Contact;
import com.sygel.tender.domain.Award;
import com.sygel.tender.domain.AwardItem;
import com.sygel.tender.domain.Contractor;
import com.sygel.tender.domain.Government;
import com.sygel.tender.domain.PreInformation;
import com.sygel.tender.domain.Rectification;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.util.Lot;
import com.sygel.tender.domain.util.Loten;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.KnowledgeService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.ui.component.Attachments;
import com.sygel.tender.ui.component.ContractorDetailComponent;
import com.sygel.tender.ui.component.GovernmentDetailComponent;
import com.sygel.tender.ui.component.NotificationDetailComponent;
import com.sygel.tender.ui.notification.NotificationDetailWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ClassResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings({ "serial", "unchecked" })
public class ContractorDetailWindow extends Window {
	@Inject
	SearchService searchService;
	@Inject
	KnowledgeService knowledgeService;
	@Inject
	I18Service i18Service;
	
	@Inject
	ContractorDetailComponent contractorDetailComponent;
	
	NotificationDetailWindow notificationDetailWindow;

	public ContractorDetailWindow() {		
		setClosable(true);
		setWidth("90%");
		setHeight("90%");		
		center();
		addStyleName("transactions");
		// DashboardEventBus.register(this);	
	}
	
	@Override
	public void attach() {
	  super.attach();
	  setCaption(i18Service.getMessage("CONTRACTOR_DETAIL"));
	  setContent(contractorDetailComponent);
	}

	public void initContractor(Long contractorId, NotificationDetailWindow notificationDetailWindow) {
		this.notificationDetailWindow =notificationDetailWindow;
		Contractor contractor = searchService.findContractorById(contractorId);
		init(contractor);
	}

	public void init(Contractor contractor) {		
		//     
    if (contractor !=null) {
    	contractorDetailComponent.getName().setValue(contractor.getName());
    	Set<Tender> tenders = new HashSet<>();
    	for (AwardItem awardItem : contractor.getAwardItems()) {
    		Tender tender =awardItem.getAward().getTender();
    		tender.setLanguage("NL");  
    		tenders.add(tender);
    	}
    	
    	contractorDetailComponent.getTitleResults().fillResults(tenders, notificationDetailWindow);
    	StringBuilder adressBuilder = new StringBuilder();
			if (contractor.getStreet() != null && contractor.getStreet().length() >0)
				adressBuilder.append(contractor.getStreet()+"</br>");
			if (contractor.getLocation() != null && contractor.getLocation().length() > 0)
				adressBuilder.append(knowledgeService.getKnowledgeBase().translateLocation(contractor.getLocation(),null)+"</br>");

  		if (contractor.getPhones()!= null && contractor.getPhones().length() > 0)
  			adressBuilder.append(contractor.getPhones()+"</br>");
  		if (contractor.getFaxes()!= null && contractor.getFaxes().length() > 0)
  			adressBuilder.append(contractor.getFaxes()+"</br>");
  		if (contractor.getEmails()!= null && contractor.getEmails().length() > 0)
  			adressBuilder.append(contractor.getEmails()+"</br>");  				   		
    	
    	contractorDetailComponent.getDetails().setValue(adressBuilder.toString());
    }
	}
}
