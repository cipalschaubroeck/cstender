/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.build;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

//import org.vaadin.maddon.FilterableListContainer;




import javax.inject.Inject;

import com.google.common.eventbus.Subscribe;
import com.sygel.tender.domain.Attachment;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.ui.attachment.AttachmentResults;
import com.sygel.tender.ui.notification.NotificationDetailWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@CDIView(value = "BUILD_TENDER")
@SuppressWarnings({ "serial", "unchecked" })
public final class BuildTenderView extends VerticalLayout implements View {
	@Inject
	private ImportService importService;

	@Inject
	private SearchService searchService;

	private boolean first = true;

	Panel contentPanel = null;
	VerticalLayout main = null;
	
	@Inject
	private TitleResults titleResults = null;
	@Inject
	private CpvResults cpvResults = null;
	@Inject
	private CategoryResults categoryResults = null;
	@Inject 
	NotificationDetailWindow notificationDetailWindow;

	public BuildTenderView() {
		setSizeFull();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
		setWidth("100%");
		setMargin(false);
		
		contentPanel = new Panel();		
		contentPanel.setWidth("100%");
		contentPanel.setHeight("100%");
		main = new VerticalLayout();
		main.setMargin(false);
		//main.setMargin(new MarginInfo(true, true, true, true));
		main.addComponent(buildToolbar());
		contentPanel.setContent(main);
		addComponent(contentPanel);
		
		setExpandRatio(contentPanel, 1);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		List<Tender> tenders = importService.findTenders();
		if (first) {
			first = false;
			main.addComponent(titleResults);
			main.addComponent(cpvResults);
			main.addComponent(categoryResults);
		}
		titleResults.fillResults(tenders, notificationDetailWindow);
		cpvResults.fillResults(searchService.constructCpvList(tenders));
		categoryResults.fillResults(searchService.constructCategoryList(tenders));
	}

	@Override
	public void detach() {
		super.detach();
		// A new instance of TransactionsView is created every time it's
		// navigated to so we'll need to clean up references to it on detach.

		// DashboardEventBus.unregister(this);
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.setMargin(new MarginInfo(true, true, true, true));
		header.setWidth("100%");
		// Responsive.makeResponsive(header);

		Component filter = buildFilter();
		header.addComponent(filter);

		return header;
	}

	private Component buildFilter() {
		final TextField filter = new TextField();
		filter.setWidth("100%");
		filter.addTextChangeListener(new TextChangeListener() {

			@Override
			public void textChange(final TextChangeEvent event) {
				List<Tender> tenders = searchService.findTenders(event.getText(), false);

				titleResults.fillResults(tenders, notificationDetailWindow);
				cpvResults.fillResults(searchService.constructCpvList(tenders));
				categoryResults.fillResults(searchService.constructCategoryList(tenders));
			}
		});

		filter.setInputPrompt("Filter Aanbestedingen");
		filter.setIcon(FontAwesome.SEARCH);
		filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		filter.addShortcutListener(new ShortcutListener("Clear", KeyCode.ESCAPE, null) {
			@Override
			public void handleAction(final Object sender, final Object target) {
				filter.setValue("");
			}
		});
		return filter;
	}
}
