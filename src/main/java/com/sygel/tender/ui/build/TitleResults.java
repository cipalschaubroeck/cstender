/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.build;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.ui.component.BasicTable;
import com.sygel.tender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class TitleResults extends CustomComponent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private VerticalLayout screen = null;
	private BasicTable table = null;
	private Label count = new Label("");
	
	private NotificationDetailWindow notificationDetailWindow;
	
	I18Service i18Service = null;
	
	@Inject
	public TitleResults(I18Service i18Service)
	{
		this.i18Service = i18Service;
		table = new BasicTable();
		table.setHeight(2, Unit.INCH);
		table.setWidth("100%");
		table.setSelectable(true);
		table.addActionHandler(new TransactionsActionHandler());
		table.addItemClickListener(new ItemClickListener()
		{
			private static final long serialVersionUID = 1L;

			@Override
			public void itemClick(ItemClickEvent event)
			{
			}
		});

		Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
		downloadButton.setStyleName(Reindeer.BUTTON_LINK);
		downloadButton.addClickListener(new ClickListener()
		{
			private static final long serialVersionUID = 1L;
			private ExcelExport excelExport;

			public void buttonClick(final ClickEvent event)
			{
				excelExport = new ExcelExport(table);
				excelExport.excludeCollapsedColumns();
				excelExport.setReportTitle("Aanbestedingen");
				excelExport.export();
			}
		});
		

		HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
		bottom.setSpacing(true);
		screen = new VerticalLayout(table, bottom);
		screen.setMargin(new MarginInfo(false, true, false, true));
		setSizeFull();
		screen.setSizeFull();
		screen.setExpandRatio(table, 1);
		setCompositionRoot(screen);
	}

	public void fillResults(Collection<Tender> tendersFound, NotificationDetailWindow notificationDetailWindow)
	{
		this.notificationDetailWindow = notificationDetailWindow;
		BeanItemContainer<Tender> ds = new BeanItemContainer<Tender>(Tender.class);
		table.setContainerDataSource(ds);
		table.setVisibleColumns("title");
		
		table.setColumnHeaders("Titel");
		Object[] columnIds = table.getVisibleColumns();
		

		ds.addAll(tendersFound);
		
		count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + tendersFound.size());
	}
	
	private class TransactionsActionHandler implements Handler {

        private final Action details = new Action("Aanbesteding details");

        @Override
        public void handleAction(final Action action, final Object sender,
                final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long tenderId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(notificationDetailWindow))
                      UI.getCurrent().addWindow(notificationDetailWindow);
                    notificationDetailWindow.initTender(tenderId);
                    notificationDetailWindow.focus();
                    
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
	
	  public void setSideMargins(boolean sideMargin) {
	  	if (sideMargin)
	  		screen.setMargin(new MarginInfo(false, true, false, true));
	  	else {
			  screen.setMargin(new MarginInfo(false, false, false, false));
			  table.setHeight("100%");
			  table.setWidth("100%");
			  screen.setHeight("100%");
			  //screen.setWidth("100%");
			  //screen.setHeightUndefined();
			  setHeight("100%");
	  	}
			

	  }
}