/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.admin;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.AsyncResult;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.google.common.eventbus.Subscribe;
import com.sygel.tender.domain.Filter;
import com.sygel.tender.domain.PushTenderFilter;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.User;
import com.sygel.tender.domain.UserFilter;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportPreService;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.Progress;
import com.sygel.tender.service.PushService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.component.BuildSearchWindow;
import com.sygel.tender.ui.user.UserDetailWindow;
import com.sygel.tender.ui.util.ComboBoxTextChange;
import com.sygel.tender.ui.util.SessionUtil;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ReadOnlyException;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.UIEvents.PollEvent;
import com.vaadin.event.UIEvents.PollListener;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Admin tasks
 * Import notice from zip files.
 * Generate Email users
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@CDIView(value = "TASK_ADMIN")
@SuppressWarnings({ "serial", "unchecked" })
@RolesAllowed({User.ROLE_ADMIN})
public final class TaskAdminView extends VerticalLayout implements View, PollListener {
	private final Logger logger = Logger.getLogger(TaskAdminView.class);
	
	private I18Service i18Service;
	@Inject
	ImportPreService importPreService;
	@Inject
	PushService pushService;

	private Button importTenderButton;
	private Button emailTenderButton;

	Filter searchFilter = new UserFilter("", "");
	Filter currentTextFilter = null;
	boolean lastAdded = false;

	private boolean importBusy = false;
	private boolean emailBusy = false;
	
	private HorizontalLayout importTenderLayout = null;
	private HorizontalLayout emailTenderLayout = null;
	
	private transient Future<String> importResult = null;
	private ProgressBar progressBar = null;
	// Volatile because read in another thread
	private volatile Progress progress = null;
	private Label importResultLabel = new Label();
	private Label emailResultLabel = new Label();

	@Inject
	public TaskAdminView(I18Service i18Service) {
		this.i18Service = i18Service;
		setSizeFull();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
		setWidth("100%");
		// Create the indicator, disabled until progress is started
		progressBar = new ProgressBar();
		progressBar.setEnabled(false);
		importTenderLayout = buildImportToolbar();
		emailTenderLayout = buildEmailToolbar();
		addComponent(importTenderLayout);
		addComponent(emailTenderLayout);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
	}

	@Override
	public void detach() {
		super.detach();
		// A new instance of TransactionsView is created every time it's
		// navigated to so we'll need to clean up references to it on detach.

		// DashboardEventBus.unregister(this);
	}

	private HorizontalLayout buildImportToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.setMargin(new MarginInfo(true, true, false, true));
		header.setWidth("100%");
		// Responsive.makeResponsive(header);
		importTenderButton = buildImportTenderButton();
		header.addComponent(importTenderButton);
    header.addComponent(importResultLabel);
		return header;
	}
	
	private HorizontalLayout buildEmailToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.setMargin(new MarginInfo(true, true, false, true));
		header.setWidth("100%");
		// Responsive.makeResponsive(header);
		emailTenderButton = buildEmailTenderButton();
		header.addComponent(emailTenderButton);
    header.addComponent(emailResultLabel);    
		return header;
	}

	private Button buildImportTenderButton() {
		final Button userButton = new Button(i18Service.getMessage("TASK_IMPORT"));
		userButton.addStyleName(ValoTheme.BUTTON_QUIET);

		userButton.setIcon(FontAwesome.DOWNLOAD);

		userButton.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				progress = new Progress();
				importBusy = true;
				importTenderLayout.addComponent(progressBar);
				importTenderLayout.removeComponent(importResultLabel);
				progressBar.setEnabled(true);
				// Disable the button until the work is done
				importTenderButton.setEnabled(false);
				emailTenderButton.setEnabled(false);
				importResult = importPreService.importTenderFolder(new File(ImportService.DATA_FOLDER+"/enot/"), progress);		
				// start polling to update the UI progressBar
				UI.getCurrent().setPollInterval(1000);
				UI.getCurrent().addPollListener(TaskAdminView.this);
			}
		});
		userButton.setEnabled(true);
		return userButton;
	}
	
	private Button buildEmailTenderButton() {
		final Button emailButton = new Button(i18Service.getMessage("TASK_EMAIL"));
		emailButton.addStyleName(ValoTheme.BUTTON_QUIET);
		emailButton.setIcon(FontAwesome.ENVELOPE);
		emailButton.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				progress = new Progress();
				emailBusy = true;
				importTenderLayout.addComponent(progressBar);
				importTenderLayout.removeComponent(importResultLabel);
				progressBar.setEnabled(true);
				// Disable the buttons until the work is done
				importTenderButton.setEnabled(false);
				emailTenderButton.setEnabled(false);
				importResult = pushService.emailUsers(progress);	
				// start polling to update the UI progressBar
				UI.getCurrent().setPollInterval(1000);
				UI.getCurrent().addPollListener(TaskAdminView.this);
			}
		});
		emailButton.setEnabled(true);
		return emailButton;
	}


	@Override
	public void poll(PollEvent event) {
		if (importResult != null && importResult.isDone())
		{
			// Restore the state to initial
			progressBar.setValue(new Float(0.0));
			progressBar.setEnabled(false);
			// Stop polling
			UI.getCurrent().setPollInterval(-1);
			// Update result
			importTenderButton.setEnabled(true);
			emailTenderButton.setEnabled(true);
			importTenderLayout.removeComponent(progressBar);
			try {
				if (importBusy)
				  importResultLabel.setValue(i18Service.getMessage("IMPORT_NOTICE_COUNT")+ " "+importResult.get());
				if (emailBusy)
				  emailResultLabel.setValue(i18Service.getMessage("EMAIL_COUNT")+ " "+importResult.get());
				importResult = null;
				emailBusy = false;
				importBusy = false;
			} catch (ReadOnlyException e) {
				logger.error(e);
			} catch (InterruptedException e) {
				logger.error(e);
			} catch (ExecutionException e) {
				logger.error(e);
			}
			importTenderLayout.addComponent(importResultLabel);			
		} else {
			progressBar.setValue(new Float(progress.getProgress()));
		}		
	}
}
