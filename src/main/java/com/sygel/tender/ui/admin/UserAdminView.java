/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.admin;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import com.google.common.eventbus.Subscribe;
import com.sygel.tender.domain.Filter;
import com.sygel.tender.domain.PushTenderFilter;
import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.User;
import com.sygel.tender.domain.UserFilter;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.ImportService;
import com.sygel.tender.service.SearchService;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.component.BuildSearchWindow;
import com.sygel.tender.ui.user.UserDetailWindow;
import com.sygel.tender.ui.util.ComboBoxTextChange;
import com.sygel.tender.ui.util.SessionUtil;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.UIEvents.PollEvent;
import com.vaadin.event.UIEvents.PollListener;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
@CDIView(value = "USER_ADMIN")
@SuppressWarnings({ "serial", "unchecked" })
@RolesAllowed({User.ROLE_ADMIN})
public final class UserAdminView extends VerticalLayout implements View {
	@Inject
	private UserService userService;

	@Inject
	private SearchService searchService;

	private I18Service i18Service;

	@Inject
	private UserAdminDetailWindow userAdminDetailWindow;

	@Inject
	private UserAdminResults userAdminResults;

	private Button addUserButton;

	//private ComboBoxTextChange filter;
  private TextField filter;

	Filter searchFilter = new UserFilter("", "");
	Filter currentTextFilter = null;
	boolean lastAdded = false;

	private boolean first = true;

	@Inject
	public UserAdminView(I18Service i18Service) {
		this.i18Service = i18Service;
		setSizeFull();
		addStyleName("transactions");
		// DashboardEventBus.register(this);
		setWidth("100%");

	}

	@Override
	public void enter(final ViewChangeEvent event) {
		List<User> users = userService.findAllUsers();
		if (first) {
			first = false;
			addComponent(buildToolbar());
			addComponent(userAdminResults);
			userAdminResults.setView(this);
			setExpandRatio(userAdminResults, 2);
		}
		userAdminResults.fillResults(users);
	}

	@Override
	public void detach() {
		super.detach();
		// A new instance of TransactionsView is created every time it's
		// navigated to so we'll need to clean up references to it on detach.

		// DashboardEventBus.unregister(this);
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.setMargin(new MarginInfo(true, true, false, true));
		header.setWidth("100%");
		// Responsive.makeResponsive(header);

		Component filter = buildFilter();
		header.addComponent(filter);
		header.setExpandRatio(filter, 1.0f);

		addUserButton = buildUserButton();
		header.addComponent(addUserButton);

		return header;
	}

	private Button buildUserButton() {
		final Button userButton = new Button(i18Service.getMessage("ADD_USER"));
		userButton.addStyleName(ValoTheme.BUTTON_QUIET);

		userButton.setIcon(FontAwesome.USER_PLUS);
		//buildQueryButton.setDescription(i18Service.getMessage("BUILD_QUERY_DESCRIPTION"));

		userButton.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (!UI.getCurrent().getWindows().contains(userAdminDetailWindow))
					UI.getCurrent().addWindow(userAdminDetailWindow);
				userAdminDetailWindow.newUser();
				userAdminDetailWindow.focus();
				
				userAdminDetailWindow.addCloseListener(new Window.CloseListener() {
					public void windowClose(CloseEvent e) {
						updateSearch();
					}
				});
			}
		});
		userButton.setEnabled(true);
		return userButton;
	}

	private void saveFilter() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2015);
		c.set(Calendar.MONTH, Calendar.SEPTEMBER);
		c.set(Calendar.DATE, 1);
		searchFilter.setLastPushMailTo(c.getTime());
		if (searchFilter.getId() != null) {
			if (searchFilter.getName().length() == 0)
				userService.deleteFilter(searchFilter);
			else
				userService.saveFilter(searchFilter);
			//updateFilters();
		} else if (searchFilter.getName().length() > 0) {
			userService.addUserFilter(SessionUtil.getUser().getEmail(), searchFilter);
			//updateFilters();
		}
	}

	public void updateSearch() {
		List<User> users = null;
		if (searchFilter.getSearchString() != null && searchFilter.getSearchString().length() > 0)
	    users = userService.findUsers(searchFilter.getSearchString());
		else
			users = userService.findAllUsers();
		
	  userAdminResults.fillResults(users);
	}


	private Component buildFilter() {
		filter = new TextField();
		currentTextFilter = new UserFilter(searchFilter.getSearchString(), searchFilter.getSearchString());
		filter.setWidth("100%");
		filter.addTextChangeListener(event -> {
			String text = event.getText();
			if (text != null && !"".equals(text) && !searchFilter.getSearchString().equals(text)) {
				currentTextFilter.setSearchString(text);
				searchFilter.setSearchString(text);
				updateSearch();
				saveFilter();
			}
		});

		filter.setInputPrompt(i18Service.getMessage("FIND_USER"));

		filter.setIcon(FontAwesome.SEARCH);
		filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
	
		return filter;
	}
}
