/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.admin;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;

import com.sygel.tender.domain.Tender;
import com.sygel.tender.domain.User;
import com.sygel.tender.report.jasper.JasperTableExport;
import com.sygel.tender.report.jasper.JasperTenderExport;
import com.sygel.tender.service.I18Service;
import com.sygel.tender.service.UserService;
import com.sygel.tender.ui.component.BasicTable;
import com.sygel.tender.ui.user.UserDetailWindow;
import com.sygel.tender.ui.util.HyperlinkExcelExport;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class UserAdminResults extends CustomComponent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private VerticalLayout screen = null;
	private BasicTable table = null;
	private Label count = new Label("");
	private BeanItemContainer<User> tableDataSource;
	private String name = null;
	private UserAdminView userAdminView = null;
	@Inject
	UserAdminDetailWindow userAdminDetailWindow;	
	
	private I18Service i18Service;
	
	UserService userService = null;
	
	@Inject
	public UserAdminResults(I18Service i18Service, UserService userService)
	{
		this.i18Service = i18Service;
		this.userService = userService;
		table = new BasicTable();
		table.setSizeFull();
		table.setSelectable(true);
		table.addActionHandler(new TransactionsActionHandler());		
		
		table.addItemClickListener(new ItemClickListener()
		{
			private static final long serialVersionUID = 1L;
			@Override
			public void itemClick(ItemClickEvent event)
			{
			}
		});

		Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
		downloadButton.setStyleName(Reindeer.BUTTON_LINK);
		downloadButton.addClickListener(new ClickListener()
		{
			private static final long serialVersionUID = 1L;
			private HyperlinkExcelExport excelExport;

			public void buttonClick(final ClickEvent event)
			{
				excelExport = new HyperlinkExcelExport(table);				
				excelExport.setBaseUrl("http://localhost:8090/tender/secure/#!MYTENDER/"+getName());
				//excelExport.excludeCollapsedColumns();
				excelExport.setReportTitle(i18Service.getMessage("USERS"));
				excelExport.export();
			}
		});
		
		HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
		bottom.setSpacing(true);
		screen = new VerticalLayout(table, bottom);
		screen.setMargin(true);
		screen.setSpacing(true);
		setSizeFull();
		screen.setSizeFull();
		screen.setExpandRatio(table, 1);
		setCompositionRoot(screen);
	}

	public void fillResults(Collection<User> users)
	{
		tableDataSource = new BeanItemContainer<User>(User.class);
		table.setContainerDataSource(tableDataSource);
		table.setColumnCollapsingAllowed(true);
		table.setVisibleColumns("name", "firstname", "email", "company","vat", "street", "zipcode" , "city","country","roles");
		
		table.setColumnHeaders(
				i18Service.getMessage("USER_NAME"), 
				i18Service.getMessage("USER_FIRSTNAME"), 
				i18Service.getMessage("USER_EMAIL"),
				i18Service.getMessage("USER_COMPANY"),
				i18Service.getMessage("USER_VAT"),
				i18Service.getMessage("USER_STREET"), 
				i18Service.getMessage("USER_ZIPCODE"),
				i18Service.getMessage("USER_CITY"),
				i18Service.getMessage("USER_COUNTRY"),
				i18Service.getMessage("USER_ROLE"));
		Object[] columnIds = table.getVisibleColumns();
		
		for (int counter = 0;counter < columnIds.length;counter++) {
			table.setColumnCollapsingAllowed(true);
			//table.setColumnWidth(columnIds[counter], counter == 1 ? 600 : 100);
			//table.setColumnCollapsed(columnIds[counter], counter == 2 ? true : false);
		}
		tableDataSource.addAll(users);
		
		count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + users.size());
	}
	
	public void selectUser(Long userId)
	{
		for (User user : tableDataSource.getItemIds()) {
			if (user.getId().equals(userId)) {
		    table.select(user);
		    table.setCurrentPageFirstItemId(user);
		    break;
			}
		}
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private class TransactionsActionHandler implements Handler {

        private final Action details = new Action(i18Service.getMessage("USER_EDIT"));
        private final Action remove = new Action(i18Service.getMessage("USER_REMOVE"));

        @Override
        public void handleAction(final Action action, final Object sender,
                final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long userId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(userAdminDetailWindow))
                      UI.getCurrent().addWindow(userAdminDetailWindow);
                    userAdminDetailWindow.initUser(userId);
                    userAdminDetailWindow.focus();
            				userAdminDetailWindow.addCloseListener(new Window.CloseListener() {
            					public void windowClose(CloseEvent e) {
            						userAdminView.updateSearch();
            					}
            				});

                }
            } else if (action == remove) {
              Item item = ((Table) sender).getItem(target);
              if (item != null) {
                  Long userId = (Long) item.getItemProperty("id").getValue();
                  userService.removeUser(userId);
                  userAdminView.updateSearch();
              }
          }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details, remove };
        }
    }

	public void setView(UserAdminView userAdminView) {
		this.userAdminView =userAdminView;
		
	}
	
}