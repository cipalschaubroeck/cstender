/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.util;

import java.util.Map;

import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class ComboBoxTextChange extends ComboBox {
	WorkThread workTread = null;
	String filter = "";
	class WorkThread extends Thread {
    // Volatile because read in another thread in access()
    double current = 0.0;
    String text = null;
    Component component = null;
    volatile boolean active = true;
    
    public WorkThread(Component component, String text) {
    	this.component = component;
    	this.text = text;
    }

    public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		@Override
    public void run() {
        // Count up until 1.0 is reached
        while (current < 1.0 && active) {
            current += 0.10;
            // Do some "heavy work"
            try {
                sleep(100); // Sleep for 100 milliseconds
            } catch (InterruptedException e) {}
        }
        if (active) {       	
        	UI.getCurrent().access(new Runnable() {
            @Override
            public void run() {
              fireEvent(new TextChangeEvent(component) {
                @Override
                public String getText() {
                   return text;
                }

                @Override
                public int getCursorPosition() {
                  return text.length();
                }
              });
              
            }
        });
        	

        }
    }
	}
	
	
  @Override
  public void changeVariables(Object source, Map<String, Object> variables) {
      if (variables.containsKey("filter")) {
      	  if (workTread != null) {
      	  	workTread.setActive(false);
      	  }
      	  filter = variables.get("filter").toString();
      	  
          fireEvent(new TextChangeEvent(this) {
            @Override
            public String getText() {
               return filter;
            }

            @Override
            public int getCursorPosition() {
              return filter.length();
            }
          });
      	  
      	  //workTread = new WorkThread(this, text);
      	  //workTread.start();
      }
      super.changeVariables(source, variables);
  }

  public void addTextChangeListener(TextChangeListener listener) {
      addListener(TextChangeListener.EVENT_ID, TextChangeEvent.class,
              listener, TextChangeListener.EVENT_METHOD);
  }

  public void removeTextChangeListener(TextChangeListener listener) {
      removeListener(TextChangeListener.EVENT_ID, TextChangeEvent.class,
              listener);
  }
}