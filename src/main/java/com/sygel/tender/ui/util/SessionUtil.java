/*
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Unauthorized use of this source code is strictly prohibited
 * Proprietary and confidential
 */
package com.sygel.tender.ui.util;

import java.security.Principal;

import com.sygel.tender.domain.User;
import com.vaadin.ui.UI;

/**
 * 
 * @author Jurrien Saelens, Sygel
 * Copyright © Sygel - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class SessionUtil {
	
  static public void storeUser(User user) {
  	 UI.getCurrent().getSession().setAttribute("USER", user);
  }
  
  static public User getUser() {
  	return (User) UI.getCurrent().getSession().getAttribute("USER");
  }
}
