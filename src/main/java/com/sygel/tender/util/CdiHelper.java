package com.sygel.tender.util;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sygel.tender.service.I18Service;

/**
 * Cdi Inject helper class
 */
public class CdiHelper {
	/**
	 * Injects CDI objects where injection does not work.
	 * Code from: http://docs.jboss.org/weld/reference/1.1.0.Final/en-US/html_single/#d0e5286
	 * 
	 * @param clazz
	 * @param injectionObject
	 * @throws NamingException
	 */
  public static <T> void programmaticInjection(Class clazz, T injectionObject) throws NamingException {
    InitialContext initialContext = new InitialContext();
    Object lookup = initialContext.lookup("java:comp/BeanManager");
    BeanManager beanManager = (BeanManager) lookup;
    AnnotatedType annotatedType = beanManager.createAnnotatedType(clazz);
    InjectionTarget injectionTarget = beanManager.createInjectionTarget(annotatedType);
    CreationalContext creationalContext = beanManager.createCreationalContext(null);
    injectionTarget.inject(injectionObject, creationalContext);
    creationalContext.release();
  }
  
  /**
   * Injects CDI objects where injection does not work.
   * @return
   */
  public static <T> T lookupInject(Class clazz) {
  	BeanManager bm = null;
  	try {
  	    InitialContext context = new InitialContext();
  	    bm = (BeanManager) context.lookup("java:comp/BeanManager");
  	} catch (Exception e) {
  	    e.printStackTrace();
  	}

  	Bean<T> bean = (Bean<T>) bm.getBeans(clazz).iterator().next();
  	CreationalContext<T> ctx = bm.createCreationalContext(bean);
  	return (T) bm.getReference(bean, clazz, ctx);
  }
}
